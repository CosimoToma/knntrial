/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package insilico.core.test;

import insilico.core.descriptor.DescriptorBlock;
import insilico.core.descriptor.blocks.AtomCenteredFragments;
import insilico.core.descriptor.blocks.AutoCorrelationHFilledWithCorrectIState;
import insilico.core.descriptor.blocks.BurdenEigenvalueHFilled;
import insilico.core.descriptor.blocks.ConnectivityIndices;
import insilico.core.descriptor.blocks.EStates;
import insilico.core.descriptor.blocks.EdgeAdjacencyCorrected;
import insilico.core.descriptor.blocks.FunctionalGroups;
import insilico.core.descriptor.blocks.TopologicalDistances;
import insilico.core.descriptor.blocks.WalkAndPath;
import insilico.core.descriptor.blocks.logp.MLogP;
import insilico.core.molecule.InsilicoMolecule;
import insilico.core.molecule.conversion.SmilesMolecule;
import insilico.core.molecule.conversion.file.MoleculeFileSmiles;
import insilico.model.aromatase2.irfmn.AromataseDescriptors;
import insilico.model.aromatase2.irfmn.AromatasePVSA;
import java.io.File;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 *
 * @author kosmu
 */
public class TestAromataseDesc {
    public static double[] descforAromatase(InsilicoMolecule m) throws Exception {
   
    double[] Descriptors = new double[18];
  
    BurdenEigenvalueHFilled burdenEigenvalueHFilled = new BurdenEigenvalueHFilled();
    burdenEigenvalueHFilled.setBoolProperty("weightm", true);
    burdenEigenvalueHFilled.setBoolProperty("weightp", true);
    burdenEigenvalueHFilled.Calculate(m);
    WalkAndPath walkAndPath = new WalkAndPath();
    walkAndPath.setBoolProperty("path08", true);
    walkAndPath.setBoolProperty("path09", true);
    walkAndPath.Calculate(m);
    MLogP mLogP = new MLogP();
    mLogP.Calculate(m);
    ConnectivityIndices connectivityIndices = new ConnectivityIndices();
    connectivityIndices.setBoolProperty("mp05", true);
    connectivityIndices.Calculate(m);
    FunctionalGroups functionalGroups = new FunctionalGroups();
    functionalGroups.Calculate(m);
    AutoCorrelationHFilledWithCorrectIState autoCorrelationHFilledWithCorrectIState = new AutoCorrelationHFilledWithCorrectIState();
    autoCorrelationHFilledWithCorrectIState.setBoolProperty("lag04", true);
    autoCorrelationHFilledWithCorrectIState.setBoolProperty("lag05", true);
    autoCorrelationHFilledWithCorrectIState.setBoolProperty("weightp", true);
    autoCorrelationHFilledWithCorrectIState.setBoolProperty("weights", true);
    autoCorrelationHFilledWithCorrectIState.Calculate(m);
    EStates eStates = new EStates();
    eStates.Calculate(m);
    AtomCenteredFragments atomCenteredFragments = new AtomCenteredFragments();
    atomCenteredFragments.Calculate(m);
    TopologicalDistances topologicalDistances = new TopologicalDistances();
    topologicalDistances.Calculate(m);
    EdgeAdjacencyCorrected edgeAdjacencyCorrected = new EdgeAdjacencyCorrected();
    edgeAdjacencyCorrected.setBoolProperty("weightB", true);
    edgeAdjacencyCorrected.Calculate(m);
    AromatasePVSA pvsa = new AromatasePVSA();
    pvsa.setBoolProperty("weightmr", true);
    pvsa.Calculate(m);
      
      
      Descriptors[0] = burdenEigenvalueHFilled.GetByName("BEH2m").getValue();
      Descriptors[1] = burdenEigenvalueHFilled.GetByName("BEH2p").getValue();
      Descriptors[2] = AromataseDescriptors.Calculate_spMaxAEAdm(m);
      Descriptors[3] = walkAndPath.GetByName("piPC8").getValue();
      Descriptors[4] = walkAndPath.GetByName("piPC9").getValue();
      Descriptors[5] = mLogP.GetByName("MLogP").getValue();
      Descriptors[6] = pvsa.GetByName("P_VSA_mr_5").getValue();
      Descriptors[7] = connectivityIndices.GetByName("X5sol").getValue();
      Descriptors[8] = autoCorrelationHFilledWithCorrectIState.GetByName("ATSC4p").getValue();
      Descriptors[9] = edgeAdjacencyCorrected.GetByName("EEig3bo").getValue();
      Descriptors[10] = eStates.GetByName("SsssN").getValue();
      Descriptors[11] = AromataseDescriptors.Calculate_SM6_Bm(m);
      Descriptors[12] = functionalGroups.GetByName("nRNR2").getValue();
      Descriptors[13] = autoCorrelationHFilledWithCorrectIState.GetByName("ATS5s").getValue();
      Descriptors[14] = eStates.GetByName("SssNH").getValue();
      Descriptors[15] = atomCenteredFragments.GetByName("H-049").getValue();
      Descriptors[16] = topologicalDistances.GetByName("F2(C..N)").getValue();
      Descriptors[17] = AromataseDescriptors.Calculate_SM15_EAed(m);
           try {
            return Descriptors;

        } catch (Throwable e) {
            return null;
        }

    }
    public static void main(String[] args) throws Exception {
        
//       InsilicoMolecule mm = SmilesMolecule.Convert("n1p(np(np1(N2CC2)N3CC3)(N4CC4)N5CC5)(N6CCOCC6)N7CC7");
//       double[] dddd = descforAromatase(mm);
//            for (double d : dddd) {
//               System.out.print(d + "\t");
//            }
            
            
            
            
                MoleculeFileSmiles ifile = new MoleculeFileSmiles();
        ifile.OpenFile("C:/Users/kosmu/Desktop/Aromatase/ts_aromatase_irfmn smiles.txt");
            PrintStream original = System.out;
            System.setOut(new PrintStream(new OutputStream() {
                public void write(int b) {
                    //DO NOTHING
                }
            }));

        ArrayList<InsilicoMolecule> bees = ifile.ReadAll();
             System.setOut(original);

        PrintWriter w = new PrintWriter(new File("C:\\Users\\kosmu\\Desktop\\Aromatase\\Testout aromatase.txt"));
        String[] Names = {"Molecule", "SpMax2_Bh.m.", "SpMax2_Bh.p.", "SpMaxA_AEA.dm.", "piPC08", "piPC09", "MLOGP", "P_VSA_MR_6", "X5sol", "ATSC4p", "Eig03_EA.bo.", "SsssN", "SM6_B.m.", "nRNR2", "ATS5s", "SssNH", "H.049", "F02.C.N.", "SM15_EA.ed."};
        int i=0;
        for (String a : Names) {
            w.print(a + "\t");
        }
        w.print("\n");
        for (InsilicoMolecule mm : bees) {
                    i++;
        System.out.println(i);
                    w.print(mm.GetSMILES()+"\t");
            double[] dddd = descforAromatase(mm);
            for (double d : dddd) {
                w.print(d + "\t");
            }
            w.print("\n");
        }
        w.close();
    }
}
