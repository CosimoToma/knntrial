package insilico.core.test;

import Jama.EigenvalueDecomposition;
import Jama.Matrix;
import com.sun.org.apache.bcel.internal.generic.AALOAD;
import insilico.core.alerts.Alert;
import insilico.core.alerts.AlertList;
import insilico.core.alerts.builders.SABenigniBossa;
import static insilico.core.alerts.builders.SABenigniBossa.KEY_BBSA_IS_CARCINOGEN;
import insilico.core.alerts.builders.SABenigniBossaAdditional;
import insilico.core.alerts.builders.SACombaseAlgae;
import insilico.core.alerts.builders.SACombaseDaphnia;
import insilico.core.alerts.builders.SACombaseFish;
import insilico.core.alerts.builders.SACombaseMicrobes;
import insilico.core.alerts.builders.SAEstrogenBindCerapp;
import insilico.core.alerts.builders.SAHepatotoxicity;
import insilico.core.alerts.builders.SAMutagenCRS4;
import insilico.core.alerts.builders.SAMutagenIRFMN;
import insilico.core.alerts.builders.SAMutagenSarpy;
import insilico.core.alerts.builders.SAMutagenSarpy18K;
import insilico.core.alerts.builders.SAReprotoxIRFMN;
import insilico.core.constant.InsilicoConstants;
import insilico.core.descriptor.Descriptor;
import insilico.core.descriptor.DescriptorBlock;
import insilico.core.descriptor.blocks.AtomCenteredFragments;
import insilico.core.descriptor.blocks.AutoCorrelation;
import insilico.core.descriptor.blocks.AutoCorrelationHFilled;
import insilico.core.descriptor.blocks.AutoCorrelationHFilledWithCorrectIState;
import insilico.core.descriptor.blocks.BalabanLikeIndex;
import insilico.core.descriptor.blocks.BurdenEigenvalue;
import insilico.core.descriptor.blocks.BurdenEigenvaluesmodified;
import insilico.core.descriptor.blocks.BurdenEigenvaluesmodnoH;
import insilico.core.descriptor.blocks.ConnectivityIndices2;
import insilico.core.descriptor.blocks.Cats2D2;
import insilico.core.descriptor.blocks.Constitutional;
import insilico.core.descriptor.blocks.DistanceDetour;
import insilico.core.descriptor.blocks.EdgeAdjacency;
import insilico.core.descriptor.blocks.EdgeAdjacencyAugmentedCorrected;
import insilico.core.descriptor.blocks.EdgeAdjacencyCorrected;
import insilico.core.descriptor.blocks.FunctionalGroups;
import insilico.core.descriptor.blocks.InformationContent;
import insilico.core.descriptor.blocks.InformationContentWithH;
import insilico.core.descriptor.blocks.KmFactor;
import insilico.core.descriptor.blocks.PVSA;
import insilico.core.descriptor.blocks.TopologicalCharge2;
import insilico.core.descriptor.blocks.TopologicalDistances;
import insilico.core.descriptor.blocks.TopologicalEState;
import insilico.core.descriptor.blocks.WalkAndPath;
import static insilico.core.descriptor.blocks.WalkAndPath.PARAMETER_PATH_02;
import insilico.core.descriptor.blocks.logp.MLogP;
import insilico.core.descriptor.blocks.logp.MeylanLogP;
import insilico.core.descriptor.weight.CovalentRadius;
import insilico.core.descriptor.weight.GhoseCrippenWeights;
import insilico.core.descriptor.weight.Mass;
import insilico.core.exception.GenericFailureException;
import insilico.core.exception.InvalidMoleculeException;
import insilico.core.model.runner.gui.InsilicoModelRunnerGUI;
import insilico.core.molecule.InsilicoMolecule;
import insilico.core.molecule.conversion.SmilesMolecule;
import insilico.core.molecule.conversion.custom.CustomSmilesWriter;
import insilico.core.molecule.conversion.file.MoleculeFileSDF;
import insilico.core.molecule.conversion.file.MoleculeFileSmiles;
import insilico.core.molecule.tools.CustomQueryMatcher;
import insilico.core.molecule.tools.Manipulator;
import insilico.core.padel.descriptors.PadelInterface;
import insilico.core.tools.FileUtilities;
import insilico.core.tools.logger.InsilicoLogger;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.List;
import libpadeldescriptor.BurdenModifiedEigenvaluesDescriptor;
import libpadeldescriptor.EStateAtomTypeDescriptor;
import libpadeldescriptor.PaDELChiPathDescriptor;
import org.openscience.cdk.Atom;
import org.openscience.cdk.Molecule;
import org.openscience.cdk.RingSet;
import org.openscience.cdk.fingerprint.Fingerprinter;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;
import org.openscience.cdk.interfaces.IRingSet;
import org.openscience.cdk.isomorphism.UniversalIsomorphismTester;
import org.openscience.cdk.isomorphism.matchers.IQueryAtomContainer;
import org.openscience.cdk.isomorphism.matchers.QueryAtomContainer;
import org.openscience.cdk1.qsar.IMolecularDescriptor;
import org.openscience.cdk.similarity.Tanimoto;
import org.openscience.cdk.smiles.SmilesGenerator;
import org.openscience.cdk.smiles.smarts.parser.SMARTSParser;

/**
 *
 * @author Alberto Manganaro (a.manganaro@kode-solutions.net)
 */
public class Testcore {
    
      public static double Calculate_EIG2_AEAdm(InsilicoMolecule mol) throws insilico.core.exception.GenericFailureException {
    Molecule m;
    try {
      m = mol.GetStructure();
    } catch (InvalidMoleculeException e) {
      return -999.0D;
    } 
    int nBO = m.getBondCount();
    if (m.getAtomCount() < 2)
      return -999.0D; 
    double[][][] EdgeAdjMat = (double[][][])null;
    try {
      EdgeAdjMat = mol.GetMatrixEdgeAdjacency();
    } catch (GenericFailureException e) {
      InsilicoLogger.getLogger().warn(e);
      return -999.0D;
    } 
    double[][] EdgeDipoleMat = new double[EdgeAdjMat.length][(EdgeAdjMat[0]).length];
    int i;
    for (i = 0; i < EdgeAdjMat.length; i++) {
      for (int j = 0; j < (EdgeAdjMat[0]).length; j++)
        EdgeDipoleMat[i][j] = EdgeAdjMat[i][j][0]; 
    } 
    for (i = 0; i < m.getBondCount(); i++) {
      Atom a = (Atom)m.getBond(i).getAtom(0);
      Atom b = (Atom)m.getBond(i).getAtom(1);
      double CurVal = GetDipoleMoment(m, a, b);
      if (CurVal == 0.0D)
        CurVal = GetDipoleMoment(m, b, a); 
      EdgeDipoleMat[i][i] = CurVal;
    } 
    Matrix DataMatrix = new Matrix(EdgeDipoleMat);
    EigenvalueDecomposition ed = new EigenvalueDecomposition(DataMatrix);
    double[] eigenvalues = ed.getRealEigenvalues();
    Arrays.sort(eigenvalues);
//    double spMax = eigenvalues[eigenvalues.length - 1];
    double spMax = eigenvalues[eigenvalues.length-1];
//    double spMaxA = spMax / nBO;
    return spMax;
  }
      
private static double GetDipoleMoment(Molecule CurMol, Atom at1, Atom at2) {
    String a = at1.getSymbol();
    String b = at2.getSymbol();
    if (a.equalsIgnoreCase("C")) {
      if (b.equalsIgnoreCase("F"))
        return 1.51D; 
      if (b.equalsIgnoreCase("Cl")) {
        int nCl = 0;
        for (IAtom at : CurMol.getConnectedAtomsList((IAtom)at1)) {
          if (at.getSymbol().equalsIgnoreCase("Cl"))
            nCl++; 
        } 
        if (nCl == 1)
          return 1.56D; 
        if (nCl == 2)
          return 1.2D; 
        if (nCl == 3)
          return 0.83D; 
      } 
      if (b.equalsIgnoreCase("Br"))
        return 1.48D; 
      if (b.equalsIgnoreCase("I"))
        return 1.29D; 
      if (b.equalsIgnoreCase("N")) {
        if (CurMol.getBond((IAtom)at1, (IAtom)at2).getFlag(5))
          return 0.0D; 
        IBond.Order ord = CurMol.getBond((IAtom)at1, (IAtom)at2).getOrder();
        if (ord == IBond.Order.SINGLE)
          return 0.4D; 
        if (ord == IBond.Order.DOUBLE)
          return 0.9D; 
        if (ord == IBond.Order.TRIPLE)
          return 3.6D; 
      } 
      if (b.equalsIgnoreCase("O")) {
        if (CurMol.getBond((IAtom)at1, (IAtom)at2).getFlag(5))
          return 0.0D; 
        IBond.Order ord = CurMol.getBond((IAtom)at1, (IAtom)at2).getOrder();
        if (ord == IBond.Order.SINGLE)
          return 0.86D; 
        if (ord == IBond.Order.DOUBLE)
          return 2.4D; 
      } 
      if (b.equalsIgnoreCase("S")) {
        if (CurMol.getBond((IAtom)at1, (IAtom)at2).getFlag(5))
          return 0.0D; 
        IBond.Order ord = CurMol.getBond((IAtom)at1, (IAtom)at2).getOrder();
        if (ord == IBond.Order.SINGLE)
          return 2.95D; 
        if (ord == IBond.Order.DOUBLE)
          return 2.8D; 
      } 
    } 
    if (a.equalsIgnoreCase("N") && b.equalsIgnoreCase("O")) {
      if (CurMol.getBond((IAtom)at1, (IAtom)at2).getFlag(5))
        return 0.0D; 
      IBond.Order ord = CurMol.getBond((IAtom)at1, (IAtom)at2).getOrder();
      int nH = 0;
      try {
        nH = at2.getImplicitHydrogenCount().intValue();
      } catch (Exception exception) {}
      int nConn = CurMol.getConnectedAtomsCount((IAtom)at2) + nH;
      if (ord == IBond.Order.SINGLE && nConn == 2)
        return 3.2D; 
      if (ord == IBond.Order.SINGLE && nConn == 1)
        return 2.0D; 
      if (ord == IBond.Order.DOUBLE && nConn == 1)
        return 2.0D; 
    } 
    if (a.equalsIgnoreCase("S") && b.equalsIgnoreCase("O")) {
      if (CurMol.getBond((IAtom)at1, (IAtom)at2).getFlag(5))
        return 0.0D; 
      IBond.Order ord = CurMol.getBond((IAtom)at1, (IAtom)at2).getOrder();
      int nConn = CurMol.getConnectedAtomsCount((IAtom)at2);
      if (ord == IBond.Order.SINGLE && nConn == 2)
        return 2.9D; 
    } 
    if (a.equalsIgnoreCase("C") && b.equalsIgnoreCase("C")) {
      if (CurMol.getBond((IAtom)at1, (IAtom)at2).getFlag(5))
        return 0.0D; 
      int nH1 = 0, nH2 = 0;
      try {
        nH1 = at1.getImplicitHydrogenCount().intValue();
      } catch (Exception exception) {}
      try {
        nH2 = at2.getImplicitHydrogenCount().intValue();
      } catch (Exception exception) {}
      int nConn1 = CurMol.getConnectedAtomsCount((IAtom)at1) + nH1;
      int nConn2 = CurMol.getConnectedAtomsCount((IAtom)at2) + nH2;
      if (nConn1 == 3 && nConn2 == 4)
        return 0.68D; 
      if (nConn1 == 3 && nConn2 == 2)
        return 1.15D; 
      if (nConn1 == 2 && nConn2 == 4)
        return 1.48D; 
    } 
    return 0.0D;
  }


    public static void main(String[] args) throws Exception {
        
        
        InsilicoMolecule mm = SmilesMolecule.Convert("N=C(Nc2cccc1ccccc12)N(c3cccc(c3)CC)C");
        System.out.println("nCIC= "+mm.GetSSSR().getAtomContainerCount());
                //smallest eigenvalue n. 2 of Burden matrix weighted by I-state  Original: SpMin2_Bh(s)
//        BurdenEigenvaluesmodified Burdenmod = new BurdenEigenvaluesmodified();
                BurdenEigenvaluesmodnoH Burdenmod = new BurdenEigenvaluesmodnoH();
        Burdenmod.setBoolProperty("weights", true);
        Burdenmod.setBoolProperty("weightm", true);
        Burdenmod.Calculate(mm);
//        1.708	1.027	0.866	0.15
        System.out.println(Burdenmod.GetByName("BEL1m").getValue()+"\t"+Burdenmod.GetByName("BEL2m").getValue());
        System.out.println(Burdenmod.GetByName("BEL1s").getValue()+"\t"+Burdenmod.GetByName("BEL2s").getValue());
        System.out.println(Calculate_EIG2_AEAdm(mm));
//        SACombaseFish comb = new SACombaseFish();
//        comb.SaveSmartsPNG();
//        
//        if (1==1) return;
//        MoleculeFileSmiles sfile = new MoleculeFileSmiles();
//        sfile.OpenFile("C:\\Users\\Alberto\\Desktop\\noec.smi");
//        ArrayList<InsilicoMolecule> mm = sfile.ReadAll();
//        
//        PVSA pvsa = new PVSA();
//        pvsa.setBoolProperty(PVSA.PARAMETER_WEIGHT_MR, true);        
//
//        for (InsilicoMolecule cm : mm) {
//            pvsa.Calculate(cm);
//            String v = pvsa.GetByName("P_VSA_mr_5").getFormattedValue();
//            System.out.println(v);
//        }
//
//        if (1==1) return;
//        AtomCenteredFragments acf = new AtomCenteredFragments();
//        acf.Calculate(mom);
//        ArrayList<Descriptor> l = acf.GetAllDescriptors();
//        int ii = 0;
//        for (Descriptor d : l) {
//            if (d.getValue() > 0) {
//                double w = GhoseCrippenWeights.GetHydrophobiticty(ii);
//                System.out.println(d.getName() + "\t" + d.getDescription() + "\t" + d.getFormattedValue() + "\t" + w);
//            }
//            ii++;
//        }
//        if (1==1) return;
//        
//        //// PERSISTENZA SUOLO JANUS
//        
////        // file persistenza
////        MoleculeFileSmiles sfile = new MoleculeFileSmiles();
////        sfile.OpenFile("soil_3.txt");
////        ArrayList<InsilicoMolecule> pers = sfile.ReadAll();
////        
////        // descriptors blocks
////        PVSA pvsa = new PVSA();
////        pvsa.setBoolProperty(PVSA.PARAMETER_WEIGHT_I, true);
////        BurdenEigenvalue burden = new BurdenEigenvalue();
////        burden.setBoolProperty(BurdenEigenvalue.PARAMETER_WEIGHT_P, true);
////        Cats2D2 cats = new Cats2D2();
////        MLogP logp = new MLogP();
////        TopologicalDistances topo = new TopologicalDistances();
////        EdgeAdjacencyAugmentedCorrected eae = new EdgeAdjacencyAugmentedCorrected();
////        eae.setBoolProperty(EdgeAdjacencyAugmentedCorrected.PARAMETER_WEIGHT_R, true);
////                
////        
////        // calcolo
////        int pers_idx = 0;
////        for (InsilicoMolecule mmm : pers) {
////            pers_idx++;
////            pvsa.Calculate(mmm);
////            burden.Calculate(mmm);
////            cats.Calculate(mmm);
////            logp.Calculate(mmm);
////            topo.Calculate(mmm);
////            eae.Calculate(mmm);
////            
////            System.out.print(pers_idx + "\t");
////            System.out.print(burden.GetByName("SpPosA_p").getFormattedValue() + "\t");
////            System.out.print(pvsa.GetByName("P_VSA_i_3").getFormattedValue() + "\t");
////            System.out.print(eae.GetByName("EEig7ri").getFormattedValue() + "\t");
////            System.out.print(cats.GetByName("CATS2D_3_DL").getFormattedValue() + "\t");
////            System.out.print(topo.GetByName("B1(C..O)").getFormattedValue() + "\t");
////            System.out.print(topo.GetByName("B1(C..Cl)").getFormattedValue() + "\t");
////            System.out.print(topo.GetByName("B3(O..O)").getFormattedValue() + "\t");
////            System.out.print(Math.pow(logp.GetByName("MLogP").getValue(),2) + "\t");
////            
////            System.out.println();
////        }
////        
////        if (1==1) return;
//        
//        
//        //// PERSISTENZA SEDIMENT JANUS
//        
//        // file persistenza
//        MoleculeFileSmiles sfile = new MoleculeFileSmiles();
//        sfile.OpenFile("sediment.smi");
//        ArrayList<InsilicoMolecule> pers = sfile.ReadAll();
//        
//        // descriptors blocks
//        PVSA pvsa = new PVSA();
//        pvsa.setBoolProperty(PVSA.PARAMETER_WEIGHT_LOGP, true);
//        BurdenEigenvalue burden = new BurdenEigenvalue();
//        burden.setBoolProperty(BurdenEigenvalue.PARAMETER_WEIGHT_P, true);
//        Cats2D2 cats = new Cats2D2();
//        MLogP logp = new MLogP();
//        TopologicalDistances topo = new TopologicalDistances();
//        EdgeAdjacencyAugmentedCorrected eae = new EdgeAdjacencyAugmentedCorrected();
//        eae.setBoolProperty(EdgeAdjacencyAugmentedCorrected.PARAMETER_WEIGHT_B, true);
//                
//        
//        // calcolo
//        int pers_idx = 0;
//        for (InsilicoMolecule mmm : pers) {
//            pers_idx++;
//            burden.Calculate(mmm);
//            pvsa.Calculate(mmm);
//
//            cats.Calculate(mmm);
//            logp.Calculate(mmm);
//            topo.Calculate(mmm);
//            eae.Calculate(mmm);
//            
//            System.out.print(pers_idx + "\t");
//            System.out.print("-999\t"); // CENT
//            System.out.print(burden.GetByName("SpPosA_p").getFormattedValue() + "\t");
//            System.out.print(pvsa.GetByName("P_VSA_logp_4").getFormattedValue() + "\t");
//            System.out.print("-999\t"); // ETA
//            System.out.print(eae.GetByName("EEig14bo").getFormattedValue() + "\t");
//            
//            
//            
////            System.out.print(cats.GetByName("CATS2D_3_DL").getFormattedValue() + "\t");
////            System.out.print(topo.GetByName("B1(C..O)").getFormattedValue() + "\t");
////            System.out.print(topo.GetByName("B1(C..Cl)").getFormattedValue() + "\t");
////            System.out.print(topo.GetByName("B3(O..O)").getFormattedValue() + "\t");
////            System.out.print(Math.pow(logp.GetByName("MLogP").getValue(),2) + "\t");
//            
//            System.out.println();
//        }
//        
//        if (1==1) return;
//        
//                
//        
//        
//        
//        
//        
//        
//        
//        FileInputStream in;
//        BufferedReader br;
//        in = new FileInputStream("18k.txt");
//        br = new BufferedReader(new InputStreamReader(in));
//
//        ArrayList<String> SMILES = new ArrayList<>();
//        ArrayList<Integer> Exp = new ArrayList<>();
//        
//        SAMutagenSarpy18K sar = new SAMutagenSarpy18K();
//
//        String str;
//        while ( (str=br.readLine())!= null ) {
//            String[] parsedStr = str.split("\t");   
//            SMILES.add((parsedStr[0]));
//            Exp.add(Integer.valueOf(parsedStr[1]));
//        }
//        
//        sar.CalcStats(SMILES, Exp);
//   
//        
//        
//        
//        
//        
//        
//        
//        
//        
//        
//        
//        
//        if (1==1) return;
//        
//        IQueryAtomContainer q = SMARTSParser.parse("*-C[$(CCCCCCCC)]C[Cl,Br,O]");
//        System.out.println(q.getAtomCount());
//        if (1==1) return;
//        
//        InsilicoMolecule moo = SmilesMolecule.Convert("c1ccccc1N");
//        SAMutagenSarpy SA = new SAMutagenSarpy();
//        double[] res = SA.getOverlapsPerc(moo);
//        
//        ArrayList<Alert> alerts = SA.getAlerts().getSAList();
//        for (int i=0; i<res.length; i++)
//            System.out.println(alerts.get(i).getDescription() + "\t" + res[i]);
//
//        if (1==1) return;
//        -0.592492193179937	-2.3219536626386668	0.09375609809979858	-2.353590004100504	-1.851805550057306	-0.804734696455816	-0.3727322855518614	1.5475787073450007	-0.2482024653018208	0.12093055801114928 -1.530988537369356	-0.5226120462534196
//        InsilicoMolecule mm = SmilesMolecule.Convert("OC(=O)CNCP(O)(O)=O");
//         System.out.println("scaled");
//        double[] dddd = descforKKKNN(mm,1);
//        for (double d:dddd) {
//            System.out.println(d);
//        }
//        System.out.println("not scaled");
//                dddd = descforKKKNN(mm,0);
//        for (double d:dddd) {
//            System.out.println(d);
//        }
        
        
//        MoleculeFileSmiles ifile = new MoleculeFileSmiles();
//        ifile.OpenFile("bees smiles.txt");
//
//        ArrayList<InsilicoMolecule> bees = ifile.ReadAll();
//        PrintWriter w = new PrintWriter(new File("Testout bees.txt"));
//        String[] Names = {"Molecule", "X4v", "GGI10", "SpMin2_Bh(s)", "Eig02_AEA(dm)", "SsOH", "NssO", "CATS2D_02_DD", "CATS2D_07_LL", "F01[N-O]", "F05[C-S]", "F06[N-S]", "F07[C-N]"};
//        for (String a : Names) {
//            w.print(a + "\t");
//        }
//        w.print("\n");
//        for (InsilicoMolecule mm : bees) {
//                    w.print(mm.GetSMILES()+"\t");
//            double[] dddd = descforBees(mm);
//            for (double d : dddd) {
//                w.print(d + "\t");
//            }
//            w.print("\n");
//        }
//        w.close();

//            EdgeAdjacencyCorrected pvsa = new EdgeAdjacencyCorrected();
//            pvsa.setBoolProperty(EdgeAdjacencyCorrected.PARAMETER_WEIGHT_B, true);
//            pvsa.Calculate(mm);
//            System.out.println(pvsa.GetByName("ESpm7bo").getValue());
//            System.out.println(pvsa.GetByName("ESpm8bo").getValue());
//            System.out.println(pvsa.GetByName("ESpm9bo").getValue());
//            System.out.println();
//            if (1==1) return;
//        
//        
////        PVSA pvsa = new PVSA();
////        pvsa.setBoolProperty(PVSA.PARAMETER_WEIGHT_M, true);
////        pvsa.Calculate(mm);
////        String[] s = pvsa.GetAllFormattedValues();
////        for (String ss : s)
////            System.out.print(ss + "\t");
////        System.out.println();
////        if(1==1) return;
////        
////        double[] dddd = DescForSFO(mm);
//        
//        MoleculeFileSmiles sfo = new MoleculeFileSmiles();
////        sfo.OpenFile("SFO_class.smi");
//        sfo.OpenFile("SFO_regr.smi");
//        ArrayList<InsilicoMolecule> m_sfo = sfo.ReadAll();
//        
//        int idx = 0;
//        for (InsilicoMolecule mmm : m_sfo) {
//            System.out.print("m_" + idx+"\t");
////            double[] dd = DescForSFO_Class(mmm);
//            double[] dd = DescForSFO_Cosimo(mmm);
//            for (int i=0; i<dd.length; i++)
//                System.out.print(dd[i]+"\t");
//            System.out.println();
//            idx++;
//        }
//        
//        if (1==1) return;
//
////        Cats2D2 c2 = new Cats2D2();
////        c2.Calculate(mm);
////        System.out.println(c2.GetByName("CATS2D_3_AP").getFormattedValue());
//        
//        
////        TopologicalEState es = new TopologicalEState();
////        es.Calculate(mm);
////        System.out.println(es.GetByName("MAXDN").getFormattedValue());
////        System.out.println(es.GetByName("MAXDP").getFormattedValue());
//        
//        InformationContent ic = new InformationContent();
//        ic.setBoolProperty(InformationContent.PARAMETER_MAX_LAG_05, true);
//        ic.Calculate(mm);
//        System.out.println(ic.GetByName("IVDE").getFormattedValue());
////        System.out.println(ic.GetByName("BIC2").getFormattedValue());
//        
////        double j_dz = BalabanLikeIndex.Calculate(mm);
////        System.out.println(j_dz);
//        
//        if (1==1) return;
//        
//        double[][][] mat = mm.GetMatrixBarysz();
//        
//        for (int i=0; i<mat.length; i++) {
//            for (int j=0; j<mat.length; j++)
//                System.out.print(mat[i][j][0] + "\t");
//            System.out.println();
//        }
//        
//        if (1==1) return;
//        
//        
//        
//        SAEstrogenBindCerapp cerap = new SAEstrogenBindCerapp();
//        cerap.SaveSmartsPNG();
//        
//        if (1==1) return;
//        
//        InsilicoMolecule m = SmilesMolecule.Convert("n(c(c(c1cccc2)cc(=O)c3)c3)c12 ");
//        
//        String out_SMILESVega;
//        String out_Remarks;
//        
//        if (m.IsValid())
//            out_SMILESVega = m.GetSMILES();
//        else
//            out_SMILESVega = "";
//        
//        out_Remarks = "";
//        for (int j=0; j<m.GetWarnings().GetSize(); j++)
//            out_Remarks += "[Molecule warning] " + m.GetWarnings().GetMessage(j) + ". ";
//        for (int j=0; j<m.GetErrors().GetSize(); j++)
//            out_Remarks += "[Molecule error] " + m.GetErrors().GetMessage(j) + ". ";
//        
//        System.out.println(out_SMILESVega);
//        System.out.println(out_Remarks);
//        
//        if (1==1) return;
//        
        ////////////////////////////////////
//        KmFactor KM = new KmFactor();
//        
////        InsilicoMolecule mm = SmilesMolecule.Convert("c1ccc(cc1)c3ccccc3(c2ccccc2)");
////        KM.Calculate(mm);
//
//        System.out.println();
//        
//        MoleculeFileSmiles sm = new MoleculeFileSmiles();
//        sm.OpenFile("chronic_fish.txt");
//        ArrayList<InsilicoMolecule> mols = sm.ReadAll();
//        System.out.println(mols.size());
//        System.out.println();
//
//        boolean isFirst = true;
//        
//        for (InsilicoMolecule m : mols) {
//            
//            if (!m.IsValid()) {
//                System.out.println(m.GetSMILES() + "\tERROR");
//                continue;
//            }
//
//            KM.Calculate(m);
//
//            if (isFirst) {
//                isFirst = false;
//                String ss = "Mol";
//                for (Descriptor d : KM.GetAllDescriptors()){
//                    ss+= "\t" + d.getDescription();
//                }
////                for (String n : KM.GetAllNames()) {
////                    ss+= "\t" + n;
////                }
//                System.out.println(ss);
//            }
//            
//            String s = "";
//            for (Descriptor d: KM.GetAllDescriptors()) {
//                s+= "\t" + d.getFormattedValue();
//            }
//
//            System.out.println(m.GetSMILES()+s);
//        }
//        
//        if(1==1) return;
        ////////////////////////////////////
//        MoleculeFileSmiles sm = new MoleculeFileSmiles();
//        sm.OpenFile("prova_km.smi");
//        ArrayList<InsilicoMolecule> mols = sm.ReadAll();
//        System.out.println(mols.size());
//        System.out.println();
//        
//        for (InsilicoMolecule m : mols) {
//            KM.Calculate(m);
//            System.out.println(m.GetSMILES()+"\t"+KM.GetByName("kM").getValue());
//        }
//        
//        if(1==1) return;
//        InsilicoMolecule m = SmilesMolecule.Convert("n(c(c(c1cccc2)ccc3)c3)c12 ");
//        KM.Calculate(m);
//        for (Descriptor d : KM.GetAllDescriptors()) {
//            double v = d.getValue();
//            if (v!=0)
//                System.out.println(d.getDescription() + ": " + d.getValue());
//        }
//        SAHepatotoxicity hepa = new SAHepatotoxicity();
//        hepa.SaveSmartsPNG();
//        
//        if (1==1) return;
//        InsilicoMolecule m = SmilesMolecule.Convert("CCCCCCCCC(=O)O");
//        
//        InformationContent ic = new InformationContent();
//        ic.setBoolProperty(InformationContent.PARAMETER_MAX_LAG_01, true);
//        ic.setBoolProperty(InformationContent.PARAMETER_MAX_LAG_02, true);
//        ic.Calculate(m);
//        System.out.println("IC1: " + ic.GetByName("IC1").getValue());
//        System.out.println("CIC1: " + ic.GetByName("CIC1").getValue());
//        System.out.println("IC2: " + ic.GetByName("IC2").getValue());
//        System.out.println("CIC2: " + ic.GetByName("CIC2").getValue());
//        
//        if (1==1) return;
//        SAEstrogenBindCerapp cerapp = new SAEstrogenBindCerapp();
//        ArrayList<Alert> a = cerapp.getAlerts().getSAList();
//        for (Alert aa : a) {
//            System.out.println(aa.getName() + ": " + aa.getDescription());
//        }
//        if (1==1) return;
//        
//        
//        cerapp.SaveSmartsPNG();
//        if (1==1) return;
//        
//        MoleculeFileSmiles SMI = new MoleculeFileSmiles();
//        SMI.OpenFile("ds_SFO_neutralizzati.smi");
//        ArrayList<InsilicoMolecule> Mols = SMI.ReadAll();
//        SMI.CloseFile();
//
//        Cats2D2 cats = new Cats2D2();
//        for (int i=0; i<Mols.size(); i++) {
//            cats.Calculate(Mols.get(i));
//            System.out.println(cats.GetByName("CATS2D_2_PN").getFormattedValue());
//        }
//        
//        if (1==1) return;
//        
//        
//        ArrayList<InsilicoMolecule> Mols2 = new ArrayList<>();
////        Mols2.add(SmilesMolecule.Convert("C(CCCC1)(C1)C"));
//        Mols2.add(SmilesMolecule.Convert("ClCC(Cl)C")); // 9
//        
//
//        
//        EdgeAdjacency ea = new EdgeAdjacency();
//        ea.setBoolProperty(EdgeAdjacency.PARAMETER_WEIGHT_D, true);
//        for (int i=0; i<Mols2.size(); i++) {
//            ea.Calculate(Mols2.get(i));
////            System.out.println(ea.GetByName("ESpm1dmD5").getFormattedValue() + "\t" + ea.GetByName("ESpm1dm").getFormattedValue());
//            System.out.println(ea.GetByName("ESpm11dm").getFormattedValue());
//        }
/*        
         System.out.println("nCIR");
         int[] nCIR = new int[Mols.size()];
         for (int i=0; i<Mols.size(); i++) {
         InsilicoMolecule m = Mols.get(i);
         nCIR[i] = m.GetAllRings().getAtomContainerCount();
         }

         System.out.println("TI1");
         double[] TI1 = new double[Mols.size()];
         for (int i=0; i<Mols.size(); i++) {
         InsilicoMolecule m = Mols.get(i);
         int nSK = m.GetStructure().getAtomCount();
         int nBO = m.GetStructure().getBondCount();
         int[][] mat = m.GetMatrixLaplace();
            
         double[][] eig_mat = new double[mat.length][mat[0].length];
         for (int k=0; k<mat.length; k++)
         for (int z=0; z<mat[0].length; z++)
         eig_mat[k][z] = mat[k][z];
            
         // Calculates eigenvalues
         Matrix DataMatrix = new Matrix(eig_mat);
         double[] eigenvalues;
         EigenvalueDecomposition ed = new EigenvalueDecomposition(DataMatrix);
         eigenvalues = ed.getRealEigenvalues();
         //Arrays.sort(eigenvalues);
            
         double eig_sum = 0;
         for (double eig : eigenvalues)
         if (eig>0.000001)
         eig_sum += 1.0/eig;
            
         double QW = eig_sum * (double)nSK;
         TI1[i] = 2 * QW * Math.log10((double)nBO/(double)nSK);
         //System.out.println(TI1[i]);
         }

        
         System.out.println("S2K");
         double[] S2K = new double[Mols.size()];
         for (int i=0; i<Mols.size(); i++) {
         InsilicoMolecule m = Mols.get(i);
         int nSK = m.GetStructure().getAtomCount();
            
         // TotPC2
         WalkAndPath WPDesc = new WalkAndPath();
         WPDesc.setBoolProperty(PARAMETER_PATH_02, true);
         WPDesc.Calculate(m);
         double TotPC2 = WPDesc.GetByName("MPC2").getValue();
            
         // Alpha
         double[] KierCR = getKierCovRadius(m.GetStructure());
         boolean SomeMissing = false;
         for (double d : KierCR)
         if (d==Descriptor.MISSING_VALUE)
         SomeMissing = true;
            
         if (!SomeMissing) {
         double Csp3_radius =  0.77;
         double alpha = 0;
         for (double d : KierCR)
         alpha += (d / Csp3_radius) - 1;

         // S2K
         if (nSK == 1)
         S2K[i] = 0;
         else if (nSK == 2)
         S2K[i] = 1 +alpha;
         else {
         if (TotPC2 == 0)
         S2K[i] = 0;
         else
         S2K[i] = (nSK + alpha - 1) * Math.pow((nSK + alpha - 2),2) / 
         Math.pow((TotPC2 + alpha),2);
         }
         } else {
         S2K[i] = Descriptor.MISSING_VALUE;
         }
            
         //System.out.println(S2K[i]);
         }
        
        
         System.out.println("piPC09");
         double[] piPC09 = new double[Mols.size()];
         for (int i=0; i<Mols.size(); i++) {
         InsilicoMolecule m = Mols.get(i);
         int nSK = m.GetStructure().getAtomCount();

         WalkAndPath WPDesc = new WalkAndPath();
         WPDesc.setBoolProperty(WalkAndPath.PARAMETER_PATH_09, true);
         WPDesc.Calculate(m);
            
         piPC09[i] = WPDesc.GetByName("piPC9").getValue();
         //System.out.println(piPC09[i]);
         }        


         System.out.println("PCR");
         double[] PCR = new double[Mols.size()];
         for (int i=0; i<Mols.size(); i++) {
         InsilicoMolecule m = Mols.get(i);
         int nSK = m.GetStructure().getAtomCount();

         WalkAndPath WPDesc = new WalkAndPath();
         WPDesc.setBoolProperty(WalkAndPath.PARAMETER_INCLUDE_INDICES, true);
         WPDesc.Calculate(m);
            
         PCR[i] = WPDesc.GetByName("PCR").getValue();
         //            System.out.println(m.GetSMILES() + "\t" + PCR[i]);
         //            System.out.print(m.GetSMILES() + "\t");
         //            for (int z=1;z<11; z++)
         //                System.out.print( Math.log(1+WPDesc.GetByName("MPC"+z).getValue())+"\t");
         //            System.out.println();
         }        

     
        
         System.out.println("Xindex");
         double[] Xindex = new double[Mols.size()];
         for (int i=0; i<Mols.size(); i++) {
         InsilicoMolecule m = Mols.get(i);
         int nSK = m.GetStructure().getAtomCount();
         int nEdges = m.GetStructure().getBondCount();
         int nH = 0;
         for (IAtom a : m.GetStructure().atoms()) {
         int H = Manipulator.CountImplicitHydrogens((Atom) a);
         nH += H;
         nEdges += H;
         }
            
         int[][] DistanceT = m.GetMatrixTopologicalDistance();
            
         // Calculate distance degree and distance frequencies for all atoms
         int[] DistDeg = new int[nSK];
         int [][] AtomDistanceFreq = new int[nSK][nSK+1];

         for (int at=0; at<nSK; at++) {
         DistDeg[at] = 0;
         for (int adf : AtomDistanceFreq[at]) adf = 0;
                
         for (int z=0; z<nSK; z++) {
         if (at==z) continue;
         DistDeg[at] += DistanceT[at][z];
         AtomDistanceFreq[at][DistanceT[at][z]]++;
         }
         }
            
         // Calculate Xlocal (weight for Xindex)
         double[] Xlocal = new double[nSK];
         double[] Ylocal = new double[nSK];
            
         for (int at=0; at<nSK; at++) {
         Ylocal[at] = 0;
         int idx=0;
         while ( (idx<nSK) && (AtomDistanceFreq[at][idx+1]>0) ) {
         double val = AtomDistanceFreq[at][idx+1] * (idx+1);
         Ylocal[at] += val * Math.log(idx+1) * (1.0 / Math.log(2));
         idx++;
         }
         }            

         for (int at=0; at<nSK; at++) {
         double Sig = DistDeg[at] * Math.log(DistDeg[at]) * (1.0 / Math.log(2));
         Xlocal[at] = Sig - Ylocal[at];
         }

         // Calculate Xindex
         double curXindex = 0;
         for (int at=0; at<nSK; at++) 
         for (int at2=0; at2<nSK; at2++)
         if (DistanceT[at][at2] == 1) 
         if (Xlocal[at] > 0)
         curXindex += 1.0 / Math.sqrt(Xlocal[at] * Xlocal[at2]);
         curXindex = curXindex / 2.0 * (nEdges - (nSK+nH) + nSK ) / (nEdges - (nSK+nH) + 2);
         Xindex[i] = curXindex;
         System.out.println(Xindex[i]);
            
         }        
         */
    }

    public static double[] descforBees(InsilicoMolecule m) throws Exception {
//  X4v	valence connectivity index of order 4	Connectivity indices	Kier-Hall molecular connectivity indices
//GGI10	topological charge index of order 10	2D autocorrelations	Topological charge autocorrelations
//SpMin2_Bh(s)	smallest eigenvalue n. 2 of Burden matrix weighted by I-state	Burden eigenvalues	Smallest eigenvalues
//Eig02_AEA(dm)	eigenvalue n. 2 from augmented edge adjacency mat. weighted by dipole moment	Edge adjacency indices	Eigenvalues
//SsOH	Sum of sOH E-states	Atom-type E-state indices	E-State sums
//NssO	Number of atoms of type ssO	Atom-type E-state indices	Atom-type counts
//CATS2D_02_DD	CATS2D Donor-Donor at lag 02	CATS 2D	Basic descriptors
//CATS2D_07_LL	CATS2D Lipophilic-Lipophilic at lag 07	CATS 2D	Basic descriptors
//F1(N..O)	Frequency of N - O at topological distance 1	2D Atom Pairs	Frequency Atom Pairs of order 1
//F5(C..S)	Frequency of C - S at topological distance 5	2D Atom Pairs	Frequency Atom Pairs of order 5
//F6(N..S)	Frequency of N - S at topological distance 6	2D Atom Pairs	Frequency Atom Pairs of order 6
//F7(C..N)	Frequency of C - N at topological distance 7	2D Atom Pairs	Frequency Atom Pairs of order 7

        double[] Descriptors = new double[12];
        TopologicalCharge2 tpch = new TopologicalCharge2();
        tpch.Calculate(m);

        //Eig02_AEA(dm)	eigenvalue n. 2 from augmented edge adjacency mat. weighted by dipole moment
        EdgeAdjacencyAugmentedCorrected EigAEA = new EdgeAdjacencyAugmentedCorrected();
        EigAEA.setBoolProperty("weightD", true);

        EigAEA.Calculate(m);

//        ConnectivityIndices2 ConnI = new ConnectivityIndices2();
//        ConnI.Calculate(m);
        Cats2D2 cats2d = new Cats2D2();
        cats2d.Calculate(m);

        TopologicalDistances TopD = new TopologicalDistances();
        TopD.Calculate(m);

        //prova con Padel
        PadelInterface Padel = new PadelInterface();
        Padel.SetSMILES(m.GetSMILES());
        //smallest eigenvalue n. 2 of Burden matrix weighted by I-state  Original: SpMin2_Bh(s)
        BurdenEigenvaluesmodified Burdenmod = new BurdenEigenvaluesmodified();
        Burdenmod.setBoolProperty("weights", true);
        Burdenmod.Calculate(m);
 
        PaDELChiPathDescriptor chipath = new PaDELChiPathDescriptor();
        ArrayList<String> DescNames = new ArrayList<>();
        DescNames.add("VP-4");
        double[] reschipath = Padel.CalculateDescriptors((IMolecularDescriptor) chipath, DescNames);

        EStateAtomTypeDescriptor estate = new EStateAtomTypeDescriptor();
        DescNames = new ArrayList<>();
        DescNames.add("SsOH");
        DescNames.add("nssO");
        double[] resestate = Padel.CalculateDescriptors((IMolecularDescriptor) estate, DescNames);

//        Descriptors[0] = ConnI.GetByName("X4v").getValue();
        Descriptors[0] = reschipath[0];
        Descriptors[1] = tpch.GetByName("GGI10").getValue();
        Descriptors[2] = Burdenmod.GetByName("BEL2s").getValue();
        Descriptors[3] = EigAEA.GetByName("EEig2dm").getValue();
        Descriptors[4] = resestate[0];
        Descriptors[5] = resestate[1];
        Descriptors[6] = cats2d.GetByName("CATS2D_2_DD").getValue();
        Descriptors[7] = cats2d.GetByName("CATS2D_7_LL").getValue();
        Descriptors[8] = TopD.GetByName("F1(N..O)").getValue();
        Descriptors[9] = TopD.GetByName("F5(C..S)").getValue();
        Descriptors[10] = TopD.GetByName("F6(N..S)").getValue();
        Descriptors[11] = TopD.GetByName("F7(C..N)").getValue();

        try {
            return Descriptors;

        } catch (Throwable e) {
            return null;
        }

    }

//    public static double[] descforKKKNN(InsilicoMolecule m, int scaled) throws Exception {
//        double[] Scaling_a = new double[] { 3.5370843989769822D, 3.856560783960346D, -0.03387469721111648D, -0.10508931674940925D, 0.6170238277597968D, 2.657289002557545D, 0.22506393861892582D, 0.21994884910485935D, 0.14066496163682865D, 78.51069875307833D, 87.46735867136897D, 39.22878914743523D };
//  
//  double[] Scaling_s = new double[] { 5.96984135772872D, 0.659460276333299D, 0.3613066018922694D, 0.11762769273291303D, 0.038593831950676234D, 2.0594228257542744D, 0.6038219583948833D, 0.5040461898273225D, 0.5667347480444093D, 66.91139520241951D, 51.531270372701464D, 40.59997969146719D };
//
//        
//        try{
//
//    AtomCenteredFragments atomCenteredFragments = new AtomCenteredFragments();
//    atomCenteredFragments.Calculate(m);
//    AutoCorrelation autoCorrelation = new AutoCorrelation();
//    autoCorrelation.setBoolProperty("lag01", true);
//    autoCorrelation.setBoolProperty("lag04", true);
//    autoCorrelation.setBoolProperty("lag07", true);
//    autoCorrelation.setBoolProperty("weightm", true);
//    autoCorrelation.setBoolProperty("weightp", true);
//    autoCorrelation.setBoolProperty("weighte", true);
//    autoCorrelation.Calculate(m);
//    Constitutional constitutional= new  Constitutional();
//    constitutional.Calculate(m);
//    FunctionalGroups functionalGroups= new  FunctionalGroups();
//    functionalGroups.Calculate(m);
//        PVSA pvsa= new  PVSA();
//        pvsa.setBoolProperty("weightm", true);
//        pvsa.setBoolProperty("weighti", true);
//    pvsa.Calculate(m);
//    
//      double[] Descriptors = new double[12];
//      Descriptors[0] = atomCenteredFragments.GetByName("H-046").getValue();
//      Descriptors[1] = autoCorrelation.GetByName("ATS4m").getValue();
//      Descriptors[2] = autoCorrelation.GetByName("MATS7p").getValue();
//      Descriptors[3] = autoCorrelation.GetByName("MATS1e").getValue();
//      Descriptors[4] = constitutional.GetByName("Mv").getValue();
//      Descriptors[5] = constitutional.GetByName("nN").getValue();
//      Descriptors[6] = functionalGroups.GetByName("nR=Ct").getValue();
//      Descriptors[7] = functionalGroups.GetByName("nRCOOH").getValue();
//      Descriptors[8] = functionalGroups.GetByName("nRNH2").getValue();
//      Descriptors[9] = pvsa.GetByName("P_VSA_m_3").getValue();
//      Descriptors[10] = pvsa.GetByName("P_VSA_i_2").getValue();
//      Descriptors[11] = pvsa.GetByName("P_VSA_i_4").getValue();
//      if (scaled==1){
//            for (int i = 0; i < 12; i++)
//        Descriptors[i] = (Descriptors[i] - Scaling_a[i]) / Scaling_s[i]; 
//      }
//        return Descriptors;
//            
//        } catch (Throwable e) {
//            return null;
//        }
//    
//    }
    private static double[] getKierCovRadius(Molecule mol) {

        int nSK = mol.getAtomCount();
        double[] covrad = CovalentRadius.getWeights(mol);

        // Calculate Kier radius for specific atom types, if not found normal
        // covalent radius is used
        for (int i = 0; i < nSK; i++) {
            Atom at = (Atom) mol.getAtom(i);
            double rad = Descriptor.MISSING_VALUE;
//            int vd = mol.getConnectedAtomsCount(at) + Manipulator.CountImplicitHydrogens(at);
            int vd = mol.getConnectedAtomsCount(at);

            if (at.getSymbol().compareTo("C") == 0) {
                if (vd == 4) {
                    rad = 0.77; // C sp3 
                }
                if (vd == 3) {
                    rad = 0.67; // C sp2 
                }
                if (vd == 2) {
                    rad = 0.60; // C sp 
                }
                if (vd == 1) {
                    rad = 0.0; // 
                }
            }
            if (at.getSymbol().compareTo("N") == 0) {
                if (vd > 2) {
                    rad = 0.74; // N sp3
                }
                if (vd == 2) {
                    rad = 0.62; // N sp2
                }
                if (vd == 1) {
                    rad = 0.55; // N sp
                }
            }
            if (at.getSymbol().compareTo("O") == 0) {
                if (vd > 1) {
                    rad = 0.74; // O sp3
                }
                if (vd == 1) {
                    rad = 0.62; // O sp2
                }
            }
            if (at.getSymbol().compareTo("S") == 0) {
                if (vd > 1) {
                    rad = 1.04; // O sp3
                }
                if (vd == 1) {
                    rad = 0.94; // O sp2
                }
            }
            if (at.getSymbol().compareTo("P") == 0) {
                if (vd > 2) {
                    rad = 1.10; // P sp3
                }
                if (vd == 2) {
                    rad = 1.00; // P sp2
                }
            }
            if (at.getSymbol().compareTo("F") == 0) {
                rad = 0.72;
            }
            if (at.getSymbol().compareTo("Cl") == 0) {
                rad = 0.99;
            }
            if (at.getSymbol().compareTo("Br") == 0) {
                rad = 1.14;
            }
            if (at.getSymbol().compareTo("I") == 0) {
                rad = 1.33;
            }

            if (rad != Descriptor.MISSING_VALUE) {
                covrad[i] = rad;
            }
        }

        return covrad;
    }

    public static double[] DescForSFO(InsilicoMolecule m) throws Exception {

        try {

            // Frequency and binary atom pairs
            int nSK = m.GetStructure().getAtomCount();
            int[][] TopoMat = m.GetMatrixTopologicalDistance();

            int F04_O_Cl = 0;
            int B08_Cl_Cl = 0;
            int B08_N_O = 0;
            int B10_C_S = 0;

            for (int i = 0; i < (nSK + 1); i++) {
                for (int j = i + 1; j < nSK; j++) {
                    String a = m.GetStructure().getAtom(i).getSymbol();
                    String b = m.GetStructure().getAtom(j).getSymbol();

                    if (TopoMat[i][j] == 4) {
                        if (((a.equalsIgnoreCase("O")) && (b.equalsIgnoreCase("Cl"))) || ((b.equalsIgnoreCase("O")) && (a.equalsIgnoreCase("Cl")))) {
                            F04_O_Cl++;
                        }
                    }
                    if (TopoMat[i][j] == 8) {
                        if (((a.equalsIgnoreCase("Cl")) && (b.equalsIgnoreCase("Cl")))) {
                            B08_Cl_Cl = 1;
                        }
                        if (((a.equalsIgnoreCase("N")) && (b.equalsIgnoreCase("O"))) || ((b.equalsIgnoreCase("N")) && (a.equalsIgnoreCase("O")))) {
                            B08_N_O = 1;
                        }
                    }
                    if (TopoMat[i][j] == 10) {
                        if (((a.equalsIgnoreCase("C")) && (b.equalsIgnoreCase("S"))) || ((b.equalsIgnoreCase("C")) && (a.equalsIgnoreCase("S")))) {
                            B10_C_S = 1;
                        }
                    }
                }
            }

            // J_Dz(v)
            double J_Dz_v = BalabanLikeIndex.Calculate(m);

            // nCIC
            int nCIC = m.GetSSSR().getAtomContainerCount();

            // N-67 corrected
            QueryAtomContainer q1 = SMARTSParser.parse("[N;D2;H;!a;!$(NC=[N,P,S,O])]([!a])[!a]");
            CustomQueryMatcher Matcher = new CustomQueryMatcher(m);
            boolean status = Matcher.matches(q1);
            int nmatch = 0;
            List mappings = null;
            if (status) {
                mappings = Matcher.getUniqueMatchingAtoms();
                nmatch = mappings.size();
            }
            int N67 = nmatch;

            // Blocks
            DistanceDetour desc_DD = new DistanceDetour();
            desc_DD.Calculate(m);

            FunctionalGroups desc_FG = new FunctionalGroups();
            desc_FG.Calculate(m);

            InformationContent desc_IC = new InformationContent();
            desc_IC.setBoolProperty(InformationContent.PARAMETER_MAX_LAG_03, true);
            desc_IC.Calculate(m);

            InformationContentWithH desc_ICH = new InformationContentWithH();
            desc_ICH.setBoolProperty(InformationContent.PARAMETER_MAX_LAG_03, true);
            desc_ICH.Calculate(m);

            AutoCorrelation desc_AC = new AutoCorrelation();
            desc_AC.setBoolProperty(AutoCorrelation.PARAMETER_LAG_04, true);
            desc_AC.setBoolProperty(AutoCorrelation.PARAMETER_WEIGHT_S, true);
            desc_AC.Calculate(m);

            TopologicalEState desc_TE = new TopologicalEState();
            desc_TE.Calculate(m);

            Cats2D2 desc_CA = new Cats2D2();
            desc_CA.Calculate(m);

            double[] Descriptors = new double[14];
            Descriptors[0] = F04_O_Cl;
            Descriptors[1] = J_Dz_v;
            Descriptors[2] = B08_Cl_Cl;
            Descriptors[3] = desc_DD.GetByName("D/Dr10").getValue();
            Descriptors[4] = desc_FG.GetByName("nRNNOx").getValue();
            Descriptors[5] = B10_C_S;
            Descriptors[6] = desc_ICH.GetByName("BIC3").getValue();
            Descriptors[7] = desc_AC.GetByName("GATS4s").getValue(); // lag diverso
            Descriptors[8] = desc_TE.GetByName("MAXDP").getValue();
            Descriptors[9] = N67;
            Descriptors[10] = B08_N_O;
            Descriptors[11] = desc_CA.GetByName("CATS2D_7_AP").getValue();
            Descriptors[12] = nCIC;
            Descriptors[13] = desc_IC.GetByName("IVDE").getValue();

            return Descriptors;

        } catch (Throwable e) {
            return null;
        }
    }

    public static double[] DescForSFO_Cosimo(InsilicoMolecule m) throws Exception {

        try {

            // nCIC
            int nCIC = m.GetSSSR().getAtomContainerCount();

            // Frequency and binary atom pairs
            int nSK = m.GetStructure().getAtomCount();
            int[][] TopoMat = m.GetMatrixTopologicalDistance();

            int B05_O_O = 0;
            int B08_Cl_Cl = 0;
            int F04_O_Cl = 0;

            for (int i = 0; i < (nSK - 1); i++) {
                for (int j = i + 1; j < nSK; j++) {
                    String a = m.GetStructure().getAtom(i).getSymbol();
                    String b = m.GetStructure().getAtom(j).getSymbol();

                    if (TopoMat[i][j] == 4) {
                        if (((a.equalsIgnoreCase("O")) && (b.equalsIgnoreCase("Cl"))) || ((b.equalsIgnoreCase("O")) && (a.equalsIgnoreCase("Cl")))) {
                            F04_O_Cl++;
                        }
                    }
                    if (TopoMat[i][j] == 5) {
                        if (((a.equalsIgnoreCase("O")) && (b.equalsIgnoreCase("O")))) {
                            B05_O_O = 1;
                        }
                    }
                    if (TopoMat[i][j] == 8) {
                        if (((a.equalsIgnoreCase("Cl")) && (b.equalsIgnoreCase("Cl")))) {
                            B08_Cl_Cl = 1;
                        }
                    }
                }
            }

            // N-67 corrected
            QueryAtomContainer q1 = SMARTSParser.parse("[N;D2;H;!a;!$(NC=[N,P,S,O])]([!a])[!a]");
            CustomQueryMatcher Matcher = new CustomQueryMatcher(m);
            boolean status = Matcher.matches(q1);
            int nmatch = 0;
            List mappings = null;
            if (status) {
                mappings = Matcher.getUniqueMatchingAtoms();
                nmatch = mappings.size();
            }
            int N67 = nmatch;

            // Blocks
            AutoCorrelationHFilled desc_AC = new AutoCorrelationHFilled();
            desc_AC.setBoolProperty(AutoCorrelation.PARAMETER_LAG_02, true);
            desc_AC.setBoolProperty(AutoCorrelation.PARAMETER_WEIGHT_S, true);
            desc_AC.Calculate(m);

            PVSA desc_PV = new PVSA();
            desc_PV.setBoolProperty(PVSA.PARAMETER_WEIGHT_M, true);
            desc_PV.Calculate(m);

//            EdgeAdjacency desc_EA = new EdgeAdjacency();
            EdgeAdjacencyCorrected desc_EA = new EdgeAdjacencyCorrected();
            desc_EA.setBoolProperty(EdgeAdjacencyCorrected.PARAMETER_WEIGHT_B, true);
            desc_EA.Calculate(m);

            FunctionalGroups desc_FG = new FunctionalGroups();
            desc_FG.Calculate(m);

            Cats2D2 desc_CA = new Cats2D2();
            desc_CA.Calculate(m);

            double[] Descriptors = new double[12];
            Descriptors[0] = nCIC;
            Descriptors[1] = desc_AC.GetByName("GATS2s").getValue();
            Descriptors[2] = desc_PV.GetByName("P_VSA_m_5").getValue();
            Descriptors[3] = desc_EA.GetByName("ESpm8bo").getValue(); // corrisponde a SM08_EA(bo)
            Descriptors[4] = desc_FG.GetByName("nRNNOx").getValue();
            Descriptors[5] = desc_FG.GetByName("nFuranes").getValue();
            Descriptors[6] = N67;
            Descriptors[7] = desc_CA.GetByName("CATS2D_3_DA").getValue();; // CATS2D_03_DA
            Descriptors[8] = desc_CA.GetByName("CATS2D_4_AL").getValue();; // CATS2D_04_AL
            Descriptors[9] = B05_O_O;
            Descriptors[10] = B08_Cl_Cl;
            Descriptors[11] = F04_O_Cl;

            return Descriptors;

        } catch (Throwable e) {
            return null;
        }
    }

    public static double[] DescForSFO_Class(InsilicoMolecule m) throws Exception {

        try {

            // nCIC
            int nCIC = m.GetSSSR().getAtomContainerCount();

            // Frequency and binary atom pairs
            int nSK = m.GetStructure().getAtomCount();
            int[][] TopoMat = m.GetMatrixTopologicalDistance();

            int B02_C_N = 0;
            int B09_C_F = 0;
            int nS = 0;

            for (int i = 0; i < (nSK); i++) {
                if ((m.GetStructure().getAtom(i).getSymbol().equalsIgnoreCase("S"))) {
                    nS++;
                }
            }

            for (int i = 0; i < (nSK - 1); i++) {

                for (int j = i + 1; j < nSK; j++) {
                    String a = m.GetStructure().getAtom(i).getSymbol();
                    String b = m.GetStructure().getAtom(j).getSymbol();

                    if (TopoMat[i][j] == 2) {
                        if (((a.equalsIgnoreCase("C")) && (b.equalsIgnoreCase("N"))) || ((b.equalsIgnoreCase("C")) && (a.equalsIgnoreCase("N")))) {
                            B02_C_N = 1;
                        }
                    }
                    if (TopoMat[i][j] == 9) {
                        if (((a.equalsIgnoreCase("C")) && (b.equalsIgnoreCase("F"))) || ((b.equalsIgnoreCase("C")) && (a.equalsIgnoreCase("F")))) {
                            B09_C_F = 1;
                        }
                    }
                }
            }

            // Blocks
            AutoCorrelationHFilled desc_AC = new AutoCorrelationHFilled();
            desc_AC.setBoolProperty(AutoCorrelation.PARAMETER_LAG_06, true);
            desc_AC.setBoolProperty(AutoCorrelation.PARAMETER_WEIGHT_S, true);
            desc_AC.Calculate(m);

            PVSA desc_PV = new PVSA();
            desc_PV.setBoolProperty(PVSA.PARAMETER_WEIGHT_LOGP, true);
            desc_PV.Calculate(m);

            EdgeAdjacencyCorrected desc_EA = new EdgeAdjacencyCorrected();
            desc_EA.setBoolProperty(EdgeAdjacency.PARAMETER_WEIGHT_D, true);
            desc_EA.Calculate(m);

            double[] Descriptors = new double[7];
            Descriptors[0] = nS;
            Descriptors[1] = nCIC;
            Descriptors[2] = desc_AC.GetByName("ATSC6s").getValue();
            Descriptors[3] = desc_PV.GetByName("P_VSA_logp_6").getValue();
            Descriptors[4] = desc_EA.GetByName("SpMaxdm").getValue(); // corrisponde a SpMax_EA(dm)
            Descriptors[5] = B02_C_N;
            Descriptors[6] = B09_C_F;

            return Descriptors;

        } catch (Throwable e) {
            return null;
        }
    }

}
