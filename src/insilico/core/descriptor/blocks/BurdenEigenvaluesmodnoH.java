/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package insilico.core.descriptor.blocks;

import Jama.EigenvalueDecomposition;
import Jama.Matrix;
import insilico.core.descriptor.Descriptor;
import insilico.core.descriptor.DescriptorBlock;
import insilico.core.descriptor.weight.EStateCorrectForH;
import insilico.core.descriptor.weight.Electronegativity;
import insilico.core.descriptor.weight.Mass;
import insilico.core.descriptor.weight.Polarizability;
import insilico.core.descriptor.weight.VanDerWaals;
import insilico.core.exception.GenericFailureException;
import insilico.core.exception.InvalidMoleculeException;
import insilico.core.molecule.InsilicoMolecule;
import insilico.core.tools.logger.InsilicoLogger;
import java.util.ArrayList;
import java.util.Arrays;
import org.openscience.cdk.Molecule;


/**
 * Burden eigenvalue (on H-Filled molecule) molecular descriptors.
 *
 * This version has the same implementation of BE descriptors, it just works
 * on a H-filled molecule. Several models need this block to be consistent
 * with original BE descriptors calculated by Dragon.
 * 
 * @author Alberto Manganaro (a.manganaro@kode-solutions.net)
 */
public class BurdenEigenvaluesmodnoH extends DescriptorBlock {

    private static final long serialVersionUID = 1L;
    private final static String BlockName = "Burden Eigenvalue Descriptors";
    
    public final static String PARAMETER_WEIGHT_M = "weightm";
    public final static String PARAMETER_WEIGHT_P = "weightp";
    public final static String PARAMETER_WEIGHT_E = "weighte";
    public final static String PARAMETER_WEIGHT_V = "weightv";
    public final static String PARAMETER_WEIGHT_S = "weights";
    
    private final static short WEIGHT_M_IDX = 0;
    private final static short WEIGHT_P_IDX = 1;
    private final static short WEIGHT_E_IDX = 2;
    private final static short WEIGHT_V_IDX = 3;
    private final static short WEIGHT_S_IDX = 4;
    private final static String[] WEIGHT_SYMBOL = {"m", "p", "e", "v","s"};
    private final static int MaxEig = 8;

    
    /**
     * Constructor. This should not be used, no weight is specified. The 
     * overloaded constructors should be used instead.
     */    
    public BurdenEigenvaluesmodnoH() {
        super();
        this.Name = BurdenEigenvaluesmodnoH.BlockName;
    }


    

    @Override
    protected final void GenerateDescriptors() {
        DescList.clear();
        ArrayList<Integer> weightList = BuildWeightList();
        for (Integer curWeight : weightList) {
            for (int i=0; i<MaxEig; i++) {
                Add("BEH" + (i+1) + WEIGHT_SYMBOL[curWeight], "");
                Add("BEL" + (i+1) + WEIGHT_SYMBOL[curWeight], "");
            }
        } 
        SetAllValues(Descriptor.MISSING_VALUE);
    }
    
    
    private ArrayList<Integer> BuildWeightList() {
        ArrayList<Integer> w = new ArrayList<>();
        if (getBoolProperty(PARAMETER_WEIGHT_M))
            w.add(new Integer(WEIGHT_M_IDX));
        if (getBoolProperty(PARAMETER_WEIGHT_P))
            w.add(new Integer(WEIGHT_P_IDX));
        if (getBoolProperty(PARAMETER_WEIGHT_E))
            w.add(new Integer(WEIGHT_E_IDX));
        if (getBoolProperty(PARAMETER_WEIGHT_V))
            w.add(new Integer(WEIGHT_V_IDX));
                if (getBoolProperty(PARAMETER_WEIGHT_S))
            w.add(new Integer(WEIGHT_S_IDX));
        return w;
    }


    /**
     * Calculate descriptors for the given molecule.
     * 
     * @param mol molecule to be calculated
     */
    @Override
    public void Calculate(InsilicoMolecule mol) {
      
    Molecule m;
    double[][] BurdenMat;
    GenerateDescriptors();
    try {
      m = mol.GetStructure();
    } catch (InvalidMoleculeException e) {
      SetAllValues(-999.0D);
      return;
    } 
    try {
      BurdenMat = mol.GetMatrixBurden();
    } catch (GenericFailureException e) {
      InsilicoLogger.getLogger().warn(e);
      SetAllValues(-999.0D);
      return;
    } 
        int nSK = m.getAtomCount();
        
        // Cycle for all found weighting schemes
        ArrayList<Integer> weightList = BuildWeightList();
        for (Integer curWeight : weightList) {
        
            // Sets needed weights
            double[] w = null;

            if (curWeight == WEIGHT_M_IDX) 
                w = Mass.getWeights(m);
            if (curWeight == WEIGHT_P_IDX) 
                w = Polarizability.getWeights(m);
            if (curWeight == WEIGHT_E_IDX) 
                w = Electronegativity.getWeights(m);
            if (curWeight == WEIGHT_V_IDX) 
                w = VanDerWaals.getWeights(m);
             if (curWeight == WEIGHT_S_IDX) {
                try {
                    EStateCorrectForH ES = new EStateCorrectForH(m);
                    w = ES.getIS();
                    
                    // correction for compatibility with D7
                    // H I-state is always 1
                    for (int i=0; i<nSK; i++) {
                        if (m.getAtom(i).getSymbol().equalsIgnoreCase("H"))
                            w[i] = 1;
                    }
                } catch (Exception e) {
                    w = new double[nSK];
                    for (int i=0; i<nSK; i++) w[i]=Descriptor.MISSING_VALUE;
                }
            }

            // If one or more weights are not available, sets all to missing value
            boolean MissingWeight = false;
            for (int i=0; i<nSK; i++) 
                if (w[i] == Descriptor.MISSING_VALUE)
                    MissingWeight = true;
            if (MissingWeight)        
                continue;

            // Builds the weighted matrix
            for (int i=0; i<nSK; i++) {
                BurdenMat[i][i] = w[i];
            }

            // Calculates eigenvalues
            Matrix DataMatrix = new Matrix(BurdenMat);
            
            EigenvalueDecomposition ed = new EigenvalueDecomposition(DataMatrix);
            double[] eigenvalues = ed.getRealEigenvalues();

            Arrays.sort(eigenvalues);

            for (int i=0; i<MaxEig; i++) {
                double valH, valL;
                if (i>(eigenvalues.length-1)) {
                    valH = 0;
                    valL = 0;
                } else {
                    valH = Math.abs(eigenvalues[(eigenvalues.length-1-i)]);
                    valL = Math.abs(eigenvalues[i]);
                }
                System.out.println("BEL" + (i+1) + WEIGHT_SYMBOL[curWeight]+ "\t"+ valL);
                SetByName("BEH" + (i+1) + WEIGHT_SYMBOL[curWeight], valH);
                SetByName("BEL" + (i+1) + WEIGHT_SYMBOL[curWeight], valL);
            }
        }
    }


    /**
     * Clones the actual descriptor block
     * @return a cloned copy of the actual object
     * @throws CloneNotSupportedException 
     */
    @Override
    public DescriptorBlock CreateClone() throws CloneNotSupportedException {
        BurdenEigenvaluesmodnoH block = new BurdenEigenvaluesmodnoH();
        block.CloneDetailsFrom(this);
        return block;
    }

}

