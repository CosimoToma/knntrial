/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package insilico.core.kknn;

/**
 *
 * @author kosmu
 */
public class KKNNVega {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double[] test = OptimalWeights(5,12);
              for (int i = 0; i < test.length; i++) {
                  System.out.println(test[i]);
              } 
    }
    
public static double[] OptimalWeights(int K, int nDescriptors){
      
    double[] Weights = new double[K];
    double sumWeights= 0.0D;
    //cast as double K ans ndesc
   double nDesc= (double)nDescriptors;
    double Kd= (double)K;
      for (double Kcount = 0; Kcount < K; Kcount++) {
      // 1/k*(1 + d/2 - d/(2*k^(2/d)) * ( (1:k)^(1+2/d) - (0:(k-1))^(1+2/d)  ))
      Weights[(int)Kcount] = 1/Kd*(1 + nDesc/2 - nDesc/(2*Math.pow(Kd,2/nDesc)) * 
              ( Math.pow(Kcount+1, 1+2/nDesc) - Math.pow(Kcount, 1+2/nDesc) ));
    
      sumWeights += Weights[(int)Kcount];
    } 
      
     //the sum of the weights could be from 0.8 to 1.2 this operation let the 
      //normalization to 1
    for (int Kcount = 0; Kcount < K; Kcount++)
      Weights[Kcount] = Weights[Kcount] / sumWeights;     
    return Weights;
}

    
}
