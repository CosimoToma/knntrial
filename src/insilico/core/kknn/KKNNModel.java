package insilico.core.kknn;
 /**
 *
 * @author kosmu
 */
 import insilico.core.model.trainingset.iTrainingSet;
 import java.util.ArrayList;
 import insilico.core.opera.OperaDistance;
 
 public class KKNNModel {
     
   protected final iTrainingSet TS;
   
   protected final int nDescriptors;
   
   protected final int K;
   
   protected final short DistType;
   
   protected boolean SkipExactMatch;
   
   public KKNNModel(iTrainingSet ModelTS, int nDescriptors, int K, short DistanceType) {
     this.TS = ModelTS;
     this.nDescriptors = nDescriptors;
     this.K = K;
     this.DistType = DistanceType;
     this.SkipExactMatch = false;
   }
   
   public void SetSkipExactMatch(boolean status) {
     this.SkipExactMatch = status;
   }
   
   public double Calculate(double[] Descriptors) throws Exception {
     if (Descriptors.length != this.nDescriptors)
       throw new Exception("Wrong number of descriptors"); 
     double[] Distances = new double[this.TS.getMoleculesSize()];
     for (int molIdx = 0; molIdx < this.TS.getMoleculesSize(); molIdx++) {
       double[] curDesc = new double[this.nDescriptors];
       for (int k = 0; k < this.nDescriptors; k++)
         curDesc[k] = this.TS.getDescriptor(molIdx, k); 
       Distances[molIdx] = OperaDistance.CalculateDistance(this.DistType, Descriptors, curDesc);
       //add these line to use only ts it works if all molecules of ts come first, otherwise change evaluator
//       if(molIdx>390){Distances[molIdx]= 1000000.0D; }
     } 
     
     ArrayList<Integer> MostSim = new ArrayList<>();
     boolean[] Used = new boolean[this.TS.getMoleculesSize()];
     int i;
     for (i = 0; i < this.TS.getMoleculesSize(); i++)
       Used[i] = false; 
     for (i = 0; i < this.K; i++) {
       double LowestDistance = 1000000.0D;
       int LowestDistIndex = 0;
       for (int k = 0; k < this.TS.getMoleculesSize(); k++) {
         if (!this.SkipExactMatch || 
           Distances[k] >= 0.001D)
           if (!Used[k] && 
             Distances[k] < LowestDistance) {
             LowestDistIndex = k;
             LowestDistance = Distances[k];
           }  
       } 
       MostSim.add(Integer.valueOf(LowestDistIndex));
       Used[LowestDistIndex] = true;
     } 
     double[] Weights = OptimalWeights(K, nDescriptors);
       double Prediction = 0.0D;
     for (int j = 0; j < this.K; j++)
       Prediction += Weights[j] * this.TS.getExperimentalValue(((Integer)MostSim.get(j)).intValue()); 
     return Prediction;
   }
   
   
   public static double[] OptimalWeights(int K, int nDescriptors){
      
    double[] Weights = new double[K];
    double sumWeights= 0.0D;
    //cast as double K ans ndesc
   double nDesc= (double)nDescriptors;
    double Kd= (double)K;
      for (double Kcount = 0; Kcount < K; Kcount++) {
      // 1/k*(1 + d/2 - d/(2*k^(2/d)) * ( (1:k)^(1+2/d) - (0:(k-1))^(1+2/d)  ))
      Weights[(int)Kcount] = 1/Kd*(1 + nDesc/2 - nDesc/(2*Math.pow(Kd,2/nDesc)) * 
              ( Math.pow(Kcount+1, 1+2/nDesc) - Math.pow(Kcount, 1+2/nDesc) ));
    
      sumWeights += Weights[(int)Kcount];
    } 
      
     //the sum of the weights could be from 0.8 to 1.2 this operation let the 
      //normalization to 1
    for (int Kcount = 0; Kcount < K; Kcount++)
      Weights[Kcount] = Weights[Kcount] / sumWeights;     
    return Weights;
}
 }

