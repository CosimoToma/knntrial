/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package insilico.core.descriptorsprinter;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;


import insilico.core.descriptor.DescriptorBlock;
import insilico.core.descriptor.blocks.AtomCenteredFragments;
import insilico.core.descriptor.blocks.AutoCorrelation;
import insilico.core.descriptor.blocks.BurdenEigenvalue;
import insilico.core.descriptor.blocks.BurdenEigenvalueHFilled;
import insilico.core.descriptor.blocks.Cats2D2;
import insilico.core.descriptor.blocks.ConnectivityIndices2;
import insilico.core.descriptor.blocks.Constitutional;
import insilico.core.descriptor.blocks.DistanceDetour;
import insilico.core.descriptor.blocks.DistanceEdge;
import insilico.core.descriptor.blocks.EStates;
import insilico.core.descriptor.blocks.EdgeAdjacency;
import insilico.core.descriptor.blocks.EdgeAdjacencyAugmentedCorrected;
import insilico.core.descriptor.blocks.EdgeAdjacencyCorrected;
import insilico.core.descriptor.blocks.EigenvalueBased2;
import insilico.core.descriptor.blocks.FunctionalGroups;
import insilico.core.descriptor.blocks.InformationContent;
import insilico.core.descriptor.blocks.InformationContentWithH;
import insilico.core.descriptor.blocks.PVSA;
import insilico.core.descriptor.blocks.Rings;
import insilico.core.descriptor.blocks.Topological;
import insilico.core.descriptor.blocks.TopologicalCharge2;
import insilico.core.descriptor.blocks.TopologicalDistances;
import insilico.core.descriptor.blocks.TopologicalEState;
import insilico.core.descriptor.blocks.WalkAndPath;
import insilico.core.descriptor.blocks.logp.ALogP;
import insilico.core.descriptor.blocks.logp.CHLogP;
import insilico.core.descriptor.blocks.logp.MLogP;
import insilico.core.descriptor.blocks.logp.MeylanLogP;
import insilico.core.molecule.InsilicoMolecule;
import insilico.core.molecule.conversion.SmilesMolecule;
import insilico.core.molecule.conversion.file.MoleculeFileSmiles;


/**
 *
 * @author kosmu
 */
public class DescriptorsPrinter {
    
	public static void main(String[] args)
			throws Exception {

			boolean hfilled = false;
			boolean corrected = true;
			boolean augcorr = true;
//		boolean hfilled = false;
//		boolean corrected = false;
//		boolean augcorr = true;
		String[] weightburden = { "m", "p", "e", "v" };
		String[] weightsymbol = { "m", "p", "e", "v", "s" };
		String[] atomsymbol = { "C", "N", "O" };
		String[] weightAdj = { "weightX", "weightD", "weightR" };
		String[] weightaug = { "weightX", "weightD", "weightR", "weightB" };
		String[] includesym = { "SRW", "Ind" };


		ArrayList<DescriptorBlock> blocks = new ArrayList<DescriptorBlock>();
		DescriptorBlock desc = new ALogP();
		blocks.add(desc);
		desc = new MLogP();
		blocks.add(desc);
		desc = new CHLogP();
		blocks.add(desc);
		desc = new MeylanLogP();
		blocks.add(desc);
		desc = new AtomCenteredFragments();
		blocks.add(desc);
		desc = new AutoCorrelation();
		for (int nlag = 1; nlag < 9; nlag++) {
			String lag = "lag0" + nlag;
			desc.setBoolProperty(lag, true);
		}
		for (String a : weightsymbol) {
			String weight = "weight" + a;
			desc.setBoolProperty(weight, true);
		}
		blocks.add(desc);
		if (hfilled == false) {
			desc = new BurdenEigenvalue();

			for (String a : weightburden) {
				String weight = "weight" + a;
				desc.setBoolProperty(weight, true);
			}
			blocks.add(desc);
		} else {
			desc = new BurdenEigenvalueHFilled();
			for (String a : weightburden) {
				String weight = "weight" + a;
				desc.setBoolProperty(weight, true);
			}
			blocks.add(desc);
		}
		desc = new Cats2D2();
		blocks.add(desc);
		desc = new ConnectivityIndices2();

		for (int nmp = 0; nmp < 6; nmp++) {
			String mp = "mp0" + nmp;
			desc.setBoolProperty(mp, true);
		}
		blocks.add(desc);
		desc = new Constitutional();
		blocks.add(desc);
		desc = new DistanceDetour();
		blocks.add(desc);
		desc = new DistanceEdge();

		for (int nvd = 1; nvd < 4; nvd++) {
			String vd = "vd0" + nvd;
			desc.setBoolProperty(vd, true);
		}
		for (String a : atomsymbol) {
			String atom = "atom" + a;
			desc.setBoolProperty(atom, true);
		}
		blocks.add(desc);
		desc = new EStates();
		blocks.add(desc);
		if (augcorr == true) {
			desc = new EdgeAdjacencyAugmentedCorrected();
			for (String a : weightaug) {
				desc.setBoolProperty(a, true);
			}
			blocks.add(desc);
		} else {
			if (corrected == false) {
				desc = new EdgeAdjacency();
				for (String a : weightAdj) {
					desc.setBoolProperty(a, true);
				}
				blocks.add(desc);
			} else {

				desc = new EdgeAdjacencyCorrected();
				for (String a : weightaug) {
					desc.setBoolProperty(a, true);
				}
				blocks.add(desc);
			}
		}
		desc = new EigenvalueBased2();

		for (String a : weightburden) {
			String weight = "weight" + a;
			desc.setBoolProperty(weight, true);
		}
		blocks.add(desc);
		desc = new WalkAndPath();

		for (int npath = 1; npath < 11; npath++) {
			String path = "path0" + npath;
			desc.setBoolProperty(path, true);
		}
		for (String a : includesym) {
			String weight = "calc" + a;
			desc.setBoolProperty(weight, true);
		}
		blocks.add(desc);
		desc = new FunctionalGroups();
		blocks.add(desc);
		if (hfilled == false) {
			desc = new InformationContent();
			for (int nlag = 1; nlag < 6; nlag++) {
				String lag = "ml0" + nlag;
				desc.setBoolProperty(lag, true);
			}
			blocks.add(desc);
		} else {
			desc = new InformationContentWithH();

			for (int nlag = 1; nlag < 6; nlag++) {
				String lag = "ml0" + nlag;
				desc.setBoolProperty(lag, true);
			}
			blocks.add(desc);
		}

		desc = new PVSA();

		String[] weightPVSA = { "m", "logp", "i" };
		for (String a : weightPVSA) {
			String weight = "weight" + a;
			desc.setBoolProperty(weight, true);
		}
		blocks.add(desc);
		desc = new Rings();
		blocks.add(desc);
		desc = new Topological();
		blocks.add(desc);
		desc = new TopologicalDistances();
		blocks.add(desc);
		desc = new TopologicalCharge2();
		blocks.add(desc);
		desc = new TopologicalEState();
		blocks.add(desc);

		
		InsilicoMolecule m = SmilesMolecule.Convert("C1=CC=CC=C1");
                
                PrintWriter w = new PrintWriter(new File("desclist.txt"));
		// try {
		for (DescriptorBlock descblock : blocks) {
                    descblock.Calculate(m);
                    String[] names = descblock.GetAllNames();
                    for (String name : names){
                    w.println(name +"\t"+descblock.GetName()+"\t"+ descblock.GetByName(name).getDescription());
			
	
		}

        }
                		w.close();
        }
        }
