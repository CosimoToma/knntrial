/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package insilico.model.test;

import insilico.core.exception.GenericFailureException;
import insilico.core.model.InsilicoModel;
import insilico.core.model.InsilicoModelOutput;
import insilico.core.model.trainingset.TrainingSet;
import insilico.core.molecule.InsilicoMolecule;
import insilico.core.molecule.conversion.SmilesMolecule;
import insilico.core.molecule.conversion.file.MoleculeFileSmiles;
import insilico.core.molecule.tools.Depiction;
import insilico.core.similarity.SimilarMolecule;
import insilico.model.aromatase2.irfmn.ismAromataseIRFMN;
import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 *
 * @author kosmu
 */
public class ModelAromataseTest {
        public static void BuildDataset() throws Exception {

        TrainingSet TS;
        InsilicoModel m;
        
        m = new ismAromataseIRFMN();
        m.setSkipADandTSLoading(true);
        TS = new TrainingSet();
        TS.Build("/insilico/model/aromatase2/irfmn/data/ts_aromatase_irfmn.txt", m);
        TS.SerializeToFile("ts_aromatase_irfmn.dat");
        
        }
            public static void BuildPNG() throws Exception {

        InsilicoModel m = new ismAromataseIRFMN();
        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "\\png\\aromatase_irfmn");
            }
        
           public static void main(String[] args) throws Exception {
//        
//
        BuildDataset();
        if (1==1) return;
//        
//        BuildPNG();
//        if (1==1) return;  


//      
//        ismAromataseIRFMN ppbmodel = new ismAromataseIRFMN();
//        
//        MoleculeFileSmiles ifile = new MoleculeFileSmiles();
//        ifile.OpenFile("C:/Users/kosmu/Desktop/Aromatase/ts_aromatase_irfmn smiles.txt");
//        
//        ArrayList<InsilicoMolecule> arom = ifile.ReadAll();
//        PrintWriter w = new PrintWriter(new File("C:\\Users\\kosmu\\Desktop\\Aromatase\\Testout_model.txt"));
//        w.println("Prediction");
//               
//               for (InsilicoMolecule mm : arom) {
//                   try{
//            InsilicoModelOutput oo = ppbmodel.Execute(mm);
//            w.println(oo.getResults()[0]);
//                   }catch (Exception e){
//                       w.println("-");
//                   }
//        }
//        w.close();
//        if (1==1) return;
//      
        

               
//               
//        InsilicoMolecule mmmm = SmilesMolecule.Convert("O(c1cc(c(OC)cc1CC)CC(N)C)C");  
//        ismAromataseIRFMN  model = new   ismAromataseIRFMN();     
//       
//        
//        
//        InsilicoModelOutput oo = model.Execute(mmmm);
//
//        String[] ss= model.GetResultsName();
//      for (String s : ss){
//        System.out.print(s+"\t");
//      }
//        System.out.println(oo.getAssessment());
//        System.out.println(oo.getResults()[0]);
//        System.out.println(oo.getResults()[1]);
//        System.out.println(oo.getResults()[2]);
//        System.out.println(oo.getResults()[3]);
//        System.out.println(oo.getErrMessage());
           }
           }
