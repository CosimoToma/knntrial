/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package insilico.model.test;

import insilico.core.molecule.InsilicoMolecule;
import insilico.core.molecule.conversion.SmilesMolecule;
import org.openscience.cdk.CDKConstants;
import org.openscience.cdk.fingerprint.Fingerprinter;
import org.openscience.cdk.fingerprint.FingerprinterTool;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.isomorphism.UniversalIsomorphismTester;
import org.openscience.cdk.isomorphism.matchers.IQueryAtom;
import org.openscience.cdk.isomorphism.matchers.QueryAtomContainer;
import org.openscience.cdk.isomorphism.matchers.QueryAtomContainerCreator;
import org.openscience.cdk.isomorphism.matchers.SymbolQueryAtom;
import org.openscience.cdk.isomorphism.matchers.smarts.AliphaticSymbolAtom;
import org.openscience.cdk.isomorphism.matchers.smarts.LogicalOperatorAtom;
import org.openscience.cdk.isomorphism.matchers.smarts.RingMembershipAtom;
import org.openscience.cdk.smiles.smarts.parser.SMARTSParser;
import org.openscience.cdk.tools.manipulator.AtomContainerManipulator;

/**
 *
 * @author kosmu
 */
public class TestHighlight {
    public static void main(String[] arg){
  try{  
    InsilicoMolecule mmmm = SmilesMolecule.Convert("CS(=O)(=O)NC1=C(OC2=CC=CC=C2)C=C(C=C1)[N+]([O-])=O");  
   IAtomContainer mol = mmmm.GetStructure();
//   AtomContainerManipulator.suppressHydrogens(mol);
//SmartsPattern ptrn = SmartsPattern.create(sma, bldr);
//
//DepictionGenerator dptgen = new DepictionGenerator();
//dptgen.withSize(200, 250)
//      .withHighlight(ptrn.matchAll(mol)
//                         .uniqueAtoms()
//                         .toChemObjects(),
//                     Color.RED)
//      .depict(mol)
//      .writeTo("~/3016.png");

}catch(Exception e)
{ 
    return;
}
    }
    }

