/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package insilico.model.test;

import insilico.core.exception.InitFailureException;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.dmg.pmml.FieldName;
import org.dmg.pmml.PMML;
import org.jpmml.evaluator.Evaluator;
import org.jpmml.evaluator.FieldValue;
import org.jpmml.evaluator.InputField;
import org.jpmml.evaluator.ModelEvaluator;
import org.jpmml.evaluator.ModelEvaluatorFactory;
import org.jpmml.model.PMMLUtil;
/**
 *
 * @author kosmu
 */
public class PMMLTEST {
    private final Evaluator evaluator;
  
  protected boolean verbose;
  
  public PMMLTEST(InputStream PmmlSource) throws InitFailureException {
    try {
      PMML pmml = PMMLUtil.unmarshal(PmmlSource);
      	// Initialize the pmmlManager
	
      ModelEvaluatorFactory modelEvaluatorFactory = ModelEvaluatorFactory.newInstance();
      
      ModelEvaluator<?> modelEvaluator = modelEvaluatorFactory.newModelEvaluator(pmml);
      this.evaluator = (Evaluator)modelEvaluator;
    } catch (Exception e) {
      throw new InitFailureException("Unable to init PMML model - " + e.getMessage());
    } 
    this.verbose = false;
  }
  
  public Map<FieldName, ?> Evaluate(Map<String, Object> Descriptors) throws Exception {
    Map<FieldName, FieldValue> arguments = new LinkedHashMap<>();
    System.out.println(this.evaluator.getSummary());
    System.out.println(this.evaluator.getOutputFields().get(0).getName());
    System.out.println(this.evaluator.getTargetFields().get(0).getDataField().getName().getValue());
    List<InputField> inputFields = this.evaluator.getInputFields();
    for (InputField inputField : inputFields) {
      FieldName inputFieldName = inputField.getName();
      if (!Descriptors.containsKey(inputField.getName().getValue()))
        throw new Exception("Descriptor " + inputField.getName().getValue() + " not found in the parameters"); 
      Object rawValue = Descriptors.get(inputField.getName().getValue());
      if (this.verbose)
        System.out.println(inputField.getName().getValue() + " : " + Descriptors.get(inputField.getName().getValue())); 
      FieldValue inputFieldValue = inputField.prepare(rawValue);
      arguments.put(inputFieldName, inputFieldValue);
    } 
    Map<FieldName, ?> outputs = this.evaluator.evaluate(arguments);
    return outputs;
  }
  
  public boolean isVerbose() {
    return this.verbose;
  }
  
  public void setVerbose(boolean verbose) {
    this.verbose = verbose;
  }
}
