package insilico.model.test;

import insilico.model.bcf.arnotgobas.ArnotGobasBCFBAF;
import insilico.core.alerts.Alert;
import insilico.core.alerts.AlertList;
import insilico.core.alerts.builders.SABenigniBossa;
import insilico.core.alerts.builders.SABenigniBossaAdditional;
import insilico.core.alerts.builders.SAMeylanLogPAdditionalFragments;
import insilico.core.alerts.builders.SAMeylanLogPCorrectionFragments;
import insilico.core.alerts.builders.SAMeylanLogPFragments;
import insilico.core.alerts.builders.SAAndrogenBindComparaIRFMN;
import insilico.core.alerts.builders.SAPersistenceSoil;
import insilico.core.alerts.builders.SAPersistenceWater;
import insilico.core.descriptor.blocks.EdgeAdjacency;
import insilico.core.knn.insilicoKnnPrediction;
import insilico.core.model.InsilicoModel;
import insilico.core.model.InsilicoModelConsensusOutput;
import insilico.core.model.InsilicoModelOutput;
import insilico.core.model.report.pdf.ReportPDF;
import insilico.core.model.report.pdf.ReportPDFSingle;
import insilico.core.model.report.txt.ReportTXTConsensusSingle;
import insilico.core.model.report.txt.ReportTXTSingle;
import insilico.core.model.runner.InsilicoModelConsensusWrapper;
import insilico.core.model.runner.InsilicoModelRunnerByMolecule;
import insilico.core.model.runner.gui.InsilicoModelRunnerGUI;
import insilico.core.model.trainingset.TrainingSet;
import insilico.core.model.trainingset.TrainingSetForKNNModels;
import insilico.core.model.trainingset.iTrainingSet;
import insilico.core.molecule.InsilicoMolecule;
import insilico.core.molecule.conversion.SmilesMolecule;
import insilico.core.molecule.conversion.file.MoleculeFileSDF;
import insilico.core.molecule.conversion.file.MoleculeFileSmiles;
import insilico.core.molecule.tools.CustomQueryMatcher;
import insilico.core.molecule.tools.Depiction;
import insilico.core.similarity.SimilarMolecule;
import insilico.core.tools.logger.InsilicoLogger;
import insilico.model.algae.combaseClass.ismAlgaeCombaseClass;
import insilico.model.algae.combaseEC50.ismAlgaeCombaseEC50;
import insilico.model.algae.ec50.ismAlgaeEC50;
import insilico.model.algae.noec.ismAlgaeNOEC;
import insilico.model.aromatase2.irfmn.ismAromataseIRFMN;
import insilico.model.bcf.arnotgobas.ismBCFArnotGobas;
import insilico.model.bcf.caesar.ismBCFCaesar;
import insilico.model.bcf.knn.ismBCFKnn;
import insilico.model.bcf.meylan.ismBCFMeylan;
import insilico.model.bee.knn.ismBeeKnn;
import insilico.model.carcinogenicity.antares.ismCarcinogenicityAntares;
import insilico.model.carcinogenicity.bb.ismCarcinogenicityBB;
import insilico.model.carcinogenicity.isscancgx.ismCarcinogenicityIsscanCgx;
import insilico.model.carcinogenicity.sficlassification.ismCarcinogenicitySFIClassification;
import insilico.model.carcinogenicity.sfiregression.ismCarcinogenicitySFIRegression;
import insilico.model.carcinogenicity.sfoclassification.ismCarcinogenicitySFOClassification;
import insilico.model.carcinogenicity.sforegression.ismCarcinogenicitySFORegression;
import insilico.model.daphnia.combase.ismDaphniaCombase;
import insilico.model.daphnia.demetra.ismDaphniaDemetra;
import insilico.model.daphnia.ec50.ismDaphniaEC50;
import insilico.model.daphnia.noec.ismDaphniaNOEC;
import insilico.model.devtox.pg.ismDevToxPG;
import insilico.model.devtox.pg.library.CustomSmilesWriterWithRings;
import insilico.model.devtox.pg.library.DARTLibrary;
import insilico.model.devtox.pg.library.PGMoleculeFileSDF;
import insilico.model.devtox.pg.library.VirtualCompound;
import insilico.model.devtox.pg.library.VirtualCompoundLibrary;
import insilico.model.fathead.knn.ismFatheadKnn;
import insilico.model.fish.combase.ismFishCombase;
import insilico.model.fish.noec.ismFishNOEC;
import insilico.model.fish.knn.ismFishKnn;
import insilico.model.fish.lc50.ismFishLC50;
import insilico.model.fish.nic.ismFishNic;
import insilico.model.guppy.knn.ismGuppyKnn;
import insilico.model.hepatotoxicity.irfmn.ismHepatotoxicityIrfmn;
import insilico.model.km.arnot.ismKmArnot;
import insilico.model.kp.istknn.ismKPistKnn;
import insilico.model.kp.kknn.ismkpkknn;
import insilico.model.loel.irfmn.LOELKnn;
import insilico.model.loel.irfmn.TrainingSetForLOEL;
import insilico.model.loel.irfmn.ismLoelIrfmn;
import insilico.model.logp.meylan.ismLogPMeylan;
import insilico.model.mutagenicity.bb.ismMutagenicityBB;
import insilico.model.mutagenicity.caesar.ismMutagenicityCaesar;
import insilico.model.mutagenicity.consensus.ismcMutagenicity;
import insilico.model.mutagenicity.knn.ismMutagenicityKnn;
import insilico.model.mutagenicity.sarpy.ismMutagenicitySarpy;
import insilico.model.persistence.sediment.irfmn.ismPersistenceSedimentIrfmn;
import insilico.model.persistence.sediment.quantitative_irfmn.ismPersistenceSedimentQuantitativeIrfmn;
import insilico.model.persistence.soil.irfmn.ismPersistenceSoilIrfmn;
import insilico.model.persistence.soil.quantitative_irfmn.ismPersistenceSoilQuantitativeIrfmn;
import insilico.model.persistence.water.irfmn.ismPersistenceWaterIrfmn;
import insilico.model.persistence.water.quantitative_irfmn.ismPersistenceWaterQuantitativeIrfmn;
import insilico.model.ppb.kknn.ismPPBkknn;
import insilico.model.rba.cerapp.ismEstrogenBindingCerapp;
import insilico.model.rba.comparairfmn.ismAndrogenBindingComparaIRFMN;
import insilico.model.skin.caesar.ismSkinCaesar;
import insilico.model.skin.irfmn.ismSkinIRFMN;
import insilico.model.sludge.combaseClass.ismSludgeCombaseClass;
import insilico.model.sludge.combaseEC50.ismSludgeCombaseEC50;
import insilico.model.vaporpressure.irfmn.ismVaporPressureIRFMN;
import insilico.model.watersolubility.irfmn.ismWaterSolubilityIRFMN;
import insilico.model.zebrafish.coral.ismZebrafishCoral;
import java.io.File;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import org.dmg.pmml.Value;
import org.openscience.cdk.CDKConstants;
import org.openscience.cdk.fingerprint.Fingerprinter;
import org.openscience.cdk.fingerprint.FingerprinterTool;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.isomorphism.UniversalIsomorphismTester;
import org.openscience.cdk.isomorphism.matchers.IQueryAtom;
import org.openscience.cdk.isomorphism.matchers.QueryAtomContainer;
import org.openscience.cdk.isomorphism.matchers.QueryAtomContainerCreator;
import org.openscience.cdk.isomorphism.matchers.SymbolQueryAtom;
import org.openscience.cdk.isomorphism.matchers.smarts.AliphaticSymbolAtom;
import org.openscience.cdk.isomorphism.matchers.smarts.LogicalOperatorAtom;
import org.openscience.cdk.isomorphism.matchers.smarts.RingMembershipAtom;
import org.openscience.cdk.smiles.smarts.parser.SMARTSParser;

/**
 *
 * @author Alberto Manganaro (a.manganaro@kode-solutions.net)
 */
public class Test {

    public static void BuildPNG() throws Exception {

        InsilicoModel m = new ismAlgaeCombaseClass();
        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "C:\\Users\\Alberto\\Desktop\\png\\algae_class");
        m = new ismAlgaeCombaseEC50();
        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "C:\\Users\\Alberto\\Desktop\\png\\algae_reg");        
        m = new ismSludgeCombaseClass();
        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "C:\\Users\\Alberto\\Desktop\\png\\sludge_class");
        m = new ismSludgeCombaseEC50();
        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "C:\\Users\\Alberto\\Desktop\\png\\sludge_reg");        
        m = new ismFishCombase();
        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "C:\\Users\\Alberto\\Desktop\\png\\fish");        
        m = new ismDaphniaCombase();
        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "C:\\Users\\Alberto\\Desktop\\png\\daphnia");        
        
        
//        InsilicoModel m = new ismSkinIRFMN();
//        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "C:\\Users\\Alberto\\Desktop\\png");
        
//        InsilicoModel m = new ismAndrogenBindingComparaIRFMN();
//        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "C:\\Users\\Alberto\\Desktop\\png");
        
//        InsilicoModel m = new ismFishLC50();
//        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "C:\\Users\\Alberto\\Desktop\\png\\fish_lc50");
//        m = new ismFishNOEC();
//        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "C:\\Users\\Alberto\\Desktop\\png\\fish_noec");
//        m = new ismAlgaeEC50();
//        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "C:\\Users\\Alberto\\Desktop\\png\\alga_ec50");
//        m = new ismAlgaeNOEC();
//        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "C:\\Users\\Alberto\\Desktop\\png\\alga_noec");
        
//        InsilicoModel m = new ismBCFArnotGobas();
//        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "C:\\Users\\Alberto\\Desktop\\png");
        
//        InsilicoModel m = new ismPersistenceWaterQuantitativeIrfmn();
//        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "C:\\Users\\Alberto\\Desktop\\png\\water");
//        
//        m = new ismPersistenceSedimentQuantitativeIrfmn();
//        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "C:\\Users\\Alberto\\Desktop\\png\\sediment");

//        InsilicoModel m = new ismWaterSolubilityIRFMN();
//        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "C:\\Users\\Alberto\\Desktop\\png");
        
//        InsilicoModel m = new ismPersistenceSoilQuantitativeIrfmn();
//        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "C:\\Users\\Alberto\\Desktop\\png");
        
//        InsilicoModel m = new ismGuppyKnn();
//        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "C:\\Users\\Alberto\\Desktop\\png\\guppy");
//        
//        m = new ismFatheadKnn();
//        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "C:\\Users\\Alberto\\Desktop\\png\\fathead");

//        InsilicoModel m = new ismCarcinogenicitySFOClassification();
//        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "C:\\Users\\Alberto\\Desktop\\carc\\sfo_cl");
//        
//        m = new ismCarcinogenicitySFORegression();
//        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "C:\\Users\\Alberto\\Desktop\\carc\\sfo_re");
//        
//        m = new ismCarcinogenicitySFIClassification();
//        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "C:\\Users\\Alberto\\Desktop\\carc\\sfi_cl");
//        
//        m = new ismCarcinogenicitySFIRegression();
//        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "C:\\Users\\Alberto\\Desktop\\carc\\sfi_re");
        
//        InsilicoModel m = new ismDevToxPG();
//        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "./png");
        
//        InsilicoModel m = new ismKmArnot();
//        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "./png");
        
//        InsilicoModel m = new ismHepatotoxicityIrfmn();
//        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "./png");
        
//        InsilicoModel m = new ismBeeKnn();
//        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "./png");
        
//        InsilicoModel m = new ismFishNic();
//        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "./png");
        
//        InsilicoModel m = new ismEstrogenBindingCerapp();
//        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "H:\\Alberto\\Work\\InSilico Repository\\Projects\\insilicoModel\\cerapp");
        
//        InsilicoModel m = new ismCarcinogenicityAntares();
//        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "H:\\Alberto\\Work\\Negri\\Java code\\insilico\\trunk\\insilicoModel\\PNG\\antares");
//        
//        m = new ismCarcinogenicityIsscanCgx();
//        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "H:\\Alberto\\Work\\Negri\\Java code\\insilico\\trunk\\insilicoModel\\PNG\\isscancgx");
        
//        InsilicoModel m = new ismPersistenceSedimentIrfmn();
//        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "H:\\Alberto\\Work\\Negri\\Java code\\insilico\\trunk\\insilicoModel\\PNG\\Sediment");
//        
//        m = new ismPersistenceWaterIrfmn();
//        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "H:\\Alberto\\Work\\Negri\\Java code\\insilico\\trunk\\insilicoModel\\PNG\\Water");
//
//        m = new ismPersistenceSoilIrfmn();
//        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "H:\\Alberto\\Work\\Negri\\Java code\\insilico\\trunk\\insilicoModel\\PNG\\Soil");
//
//        InsilicoModel m = new ismLoelIrfmn();
//        Depiction.SaveTSMoleculesAsPNG(m.GetTrainingSet(), "H:\\Alberto\\Work\\Negri\\Java code\\insilico\\trunk\\insilicoModel\\PNG\\loel");
    }
    
    public static void BuildDataset() throws Exception {

        TrainingSet TS;
        InsilicoModel m;
        
        m = new ismKPistKnn();
        m.setSkipADandTSLoading(true);
        TS = new TrainingSet();
        TS.Build("/insilico/model/kp/istknn/data/ts_kp_istknn.txt", m);
        TS.SerializeToFile("ts_kp_istknn.dat");
        
//        m = new ismkpkknn();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/kp/kknn/data/ts_kp_kknn.txt", m);
//        TS.SerializeToFile("ts_kp_kknn.dat");
//        
//        m = new ismPPBkknn();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/ppb/kknn/data/ts_ppb_kknn.txt", m);
//        TS.SerializeToFile("ts_ppb_kknn.dat");

//        m = new ismFishCombase();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/fish/combase/data/ts_fish_combase.txt", m);
//        TS.SerializeToFile("ts_fish_combase.dat");
        
//        m = new ismDaphniaCombase();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/daphnia/combase/data/ts_daphnia_combase.txt", m);
//        TS.SerializeToFile("ts_daphnia_combase.dat");
        
//        m = new ismSludgeCombaseEC50();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/sludge/combaseEC50/data/ts_sludge_combaseEC50.txt", m);
//        TS.SerializeToFile("ts_sludge_combaseEC50.dat");
//
//        m = new ismSludgeCombaseClass();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/sludge/combaseClass/data/ts_sludge_combaseClass.txt", m);
//        TS.SerializeToFile("ts_sludge_combaseClass.dat");

//        m = new ismAlgaeCombaseEC50();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/algae/combaseEC50/data/ts_algae_combaseEC50.txt", m);
//        TS.SerializeToFile("ts_algae_combaseEC50.dat");

//        m = new ismAlgaeCombaseClass();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/algae/combaseClass/data/ts_algae_combaseClass.txt", m);
//        TS.SerializeToFile("ts_algae_combaseClass.dat");

//        m = new ismSkinIRFMN();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/skin/irfmn/data/ts_skin_irfmn.txt", m);
//        TS.SerializeToFile("ts_skin_irfmn.dat");
        
//        m = new ismDaphniaEC50();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/daphnia/ec50/data/ts_daphnia_ec50.txt", m);
//        TS.SerializeToFile("ts_daphnia_ec50.dat");
        
//        m = new ismDaphniaNOEC();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/daphnia/noec/data/ts_daphnia_noec.txt", m);
//        TS.SerializeToFile("ts_daphnia_noec.dat");
        
//        m = new ismZebrafishCoral();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/zebrafish/coral/data/ts_zebrafish_coral.txt", m);
//        TS.SerializeToFile("ts_zebrafish_coral.dat");
        
//        m = new ismAndrogenBindingComparaIRFMN();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/rba/comparairfmn/data/ts_rba_comparairfmn.txt", m);
//        TS.SerializeToFile("ts_rba_comparairfmn.dat");
        
//        m = new ismAlgaeEC50();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/algae/ec50/data/ts_algae_ec50.txt", m);
//        TS.SerializeToFile("ts_algae_ec50.dat");
        
//        m = new ismAlgaeNOEC();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/algae/noec/data/ts_algae_noec.txt", m);
//        TS.SerializeToFile("ts_algae_noec.dat");

//        m = new ismFishLC50();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/fish/lc50/data/ts_fish_lc50.txt", m);
//        TS.SerializeToFile("ts_fish_lc50.dat");

//        m = new ismFishNOEC();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/fish/noec/data/ts_fish_noec.txt", m);
//        TS.SerializeToFile("ts_fish_noec.dat");

//        m = new ismBCFArnotGobas();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/bcf/arnotgobas/data/ts_bcf_arnotgobas.txt", m);
//        TS.SerializeToFile("ts_bcf_arnotgobas.dat");

//        m = new ismPersistenceWaterQuantitativeIrfmn();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/persistence/water/quantitative_irfmn/data/ts_water_quantitative_irfmn.txt", m);
//        TS.SerializeToFile("ts_water_quantitative_irfmn.dat");
//
//        m = new ismPersistenceSedimentQuantitativeIrfmn();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/persistence/sediment/quantitative_irfmn/data/ts_sediment_quantitative_irfmn.txt", m);
//        TS.SerializeToFile("ts_sediment_quantitative_irfmn.dat");

//        m = new ismVaporPressureIRFMN();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/vaporpressure/irfmn/data/ts_vaporpressure_irfmn.txt", m);
//        TS.SerializeToFile("ts_vaporpressure_irfmn.dat");

//        m = new ismWaterSolubilityIRFMN();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/watersolubility/irfmn/data/ts_watersolubility_irfmn.txt", m);
//        TS.SerializeToFile("ts_watersolubility_irfmn.dat");

//        m = new ismPersistenceSoilQuantitativeIrfmn();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/persistence/soil/quantitative_irfmn/data/ts_soil_quantitative_irfmn.txt", m);
//        TS.SerializeToFile("ts_soil_quantitative_irfmn.dat");

        
//        TrainingSetForKNNModels TSK = new TrainingSetForKNNModels();
//        TSK.Build("/insilico/model/fathead/knn/data/ts_fathead_knn.txt", new ismFatheadKnn(), null);
//        TSK.SerializeToFile("ts_fathead_knn.dat");
        
//        TrainingSetForKNNModels TSK = new TrainingSetForKNNModels();
//        TSK.Build("/insilico/model/guppy/knn/data/ts_guppy_knn.txt", new ismGuppyKnn(), null);
//        TSK.SerializeToFile("ts_guppy_knn.dat");
        
//        m = new ismCarcinogenicitySFIRegression();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/carcinogenicity/sfiregression/data/ts_carc_sfiregression.txt", m);
//        TS.SerializeToFile("ts_carc_sfiregression.dat");

//        m = new ismCarcinogenicitySFIClassification();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/carcinogenicity/sficlassification/data/ts_carc_sficlassification.txt", m);
//        TS.SerializeToFile("ts_carc_sficlassification.dat");
        
//        m = new ismDevToxPG();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/devtox/pg/data/ts_devtox_pg.txt", m);
//        TS.SerializeToFile("ts_devtox_pg.dat");
        
//        m = new ismCarcinogenicitySFORegression();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/carcinogenicity/sforegression/data/ts_carc_sforegression.txt", m);
//        TS.SerializeToFile("ts_carc_sforegression.dat");
        
//        m = new ismCarcinogenicitySFOClassification();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/carcinogenicity/sfoclassification/data/ts_carc_sfoclassification.txt", m);
//        TS.SerializeToFile("ts_carc_sfoclassification.dat");
//        
//        m = new ismKmArnot();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/km/arnot/data/ts_km_arnot.txt", m);
//        TS.SerializeToFile("ts_km_arnot.dat");
        
//        m = new ismHepatotoxicityIrfmn();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/hepatotoxicity/irfmn/data/ts_hepa_irfmn.txt", m);
//        TS.SerializeToFile("ts_hepa_irfmn.dat");
        
//        TrainingSetForKNNModels TSK = new TrainingSetForKNNModels();
//        TSK.Build("/insilico/model/bee/knn/data/ts_bee_knn.txt", new ismBeeKnn(), null);
//        TSK.SerializeToFile("ts_bee_knn.dat");
        
//        
//        m = new ismEstrogenBindingCerapp();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/rba/cerapp/data/ts_estrogen_cerapp.txt", m);
//        TS.SerializeToFile("ts_estrogen_cerapp.dat");

//        m = new ismFishNic();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/fish/nic/data/ts_fish_nic.txt", m);
//        TS.SerializeToFile("ts_fish_nic.dat");

        
//        m = new ismCarcinogenicityIsscanCgx();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/carcinogenicity/isscancgx/data/ts_carc_isscancgx.txt", m);
//        TS.SerializeToFile("ts_carc_isscancgx.dat");
        
//        m = new ismCarcinogenicityAntares();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/carcinogenicity/antares/data/ts_carc_antares.txt", m);
//        TS.SerializeToFile("ts_carc_antares.dat");
//        
//        
//        m = new ismMutagenicityCaesar();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/mutagenicity/caesar/data/ts_muta_caesar.txt", m);
//        TS.SerializeToFile("ts_muta_caesar.dat");
//        
//        m = new ismMutagenicityBB();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/mutagenicity/bb/data/ts_muta_bb.txt", m);
//        TS.SerializeToFile("ts_muta_bb.dat");

//        m = new ismMutagenicitySarpy();
//        m.setSkipADandTSLoading(true);
//        TS = new TrainingSet();
//        TS.Build("/insilico/model/mutagenicity/caesar/data/ts_muta_caesar.txt", m);
//        TS.SerializeToFile("ts_muta_sarpy.dat");

//        TrainingSetForKNNModels TSK = new TrainingSetForKNNModels();
//        TSK.Build("/insilico/model/mutagenicity/knn/data/ts_muta_knn.txt", new ismMutagenicityKnn(), null);
//        TSK.SerializeToFile("ts_muta_knn.dat");

        
//        TrainingSetForLOEL TSK = new TrainingSetForLOEL();
//        TSK.Build("/insilico/model/loel/irfmn/data/ts_loel_irfmn.txt");
//        TSK.SerializeToFile("ts_loel_irfmn.dat");
//
//        TSK = new TrainingSetForKNNModels();
//        TSK.Build("/insilico/model/persistence/sediment/irfmn/data/ts_pers_sed_irfmn.txt", new ismPersistenceSedimentIrfmn(), new SAAndrogenBindComparaIRFMN());
//        TSK.SerializeToFile("ts_pers_sed_irfmn.dat");
//
//        TSK = new TrainingSetForKNNModels();
//        TSK.Build("/insilico/model/persistence/water/irfmn/data/ts_pers_water_irfmn.txt", new ismPersistenceWaterIrfmn(), new SAPersistenceWater());
//        TSK.SerializeToFile("ts_pers_water_irfmn.dat");
//        
//        TSK = new TrainingSetForKNNModels();
//        TSK.Build("/insilico/model/persistence/soil/irfmn/data/ts_pers_soil_irfmn.txt", new ismPersistenceSoilIrfmn(), new SAPersistenceSoil());
//        TSK.SerializeToFile("ts_pers_soil_irfmn.dat");

        
//        TSK = new TrainingSetForKNNModels();
//        TSK.Build("/insilico/model/bcf/knn/data/ts_bcf_knn.txt", new ismBCFKnn(), null);
//        TSK.SerializeToFile("ts_bcf_knn.dat");
//        
//        TSK = new TrainingSetForKNNModels();
//        TSK.Build("/insilico/model/fish/knn/data/ts_fish_knn.txt", new ismFishKnn(), null);
//        TSK.SerializeToFile("ts_fish_knn.dat");
//        
//        TSK = new TrainingSetForKNNModels();
//        TSK.Build("/insilico/model/mutagenicity/knn/data/ts_muta_knn.txt", new ismMutagenicityKnn(), null);
//        TSK.SerializeToFile("ts_muta_knn.dat");
        
        
//        TSK.PrintToStdOut(true);
    }
    
    public static void main(String[] args) throws Exception {
        

//        BuildDataset();
//        if (1==1) return;
//        
//        BuildPNG();
//        if (1==1) return;  

//    
//                ismkpkknn kpmodel = new ismkpkknn();
//        
//        MoleculeFileSmiles ifile = new MoleculeFileSmiles();
//        ifile.OpenFile("kp smiles.txt");
//        
//        ArrayList<InsilicoMolecule> kp = ifile.ReadAll();
//        PrintWriter w = new PrintWriter(new File("Testout.txt"));
//        for (InsilicoMolecule mm : kp) {
//            InsilicoModelOutput oo = kpmodel.Execute(mm);
//            w.println(oo.getMainResultValue());
//        }
//        w.close();
//        if (1==1) return;
//        
        
//        
//        ismPPBkknn ppbmodel = new ismPPBkknn();
//        
//        MoleculeFileSmiles ifile = new MoleculeFileSmiles();
//        ifile.OpenFile("kp smiles.txt");
//        
//        ArrayList<InsilicoMolecule> ppb = ifile.ReadAll();
//        PrintWriter w = new PrintWriter(new File("Testout.txt"));
//        for (InsilicoMolecule mm : ppb) {
//            InsilicoModelOutput oo = ppbmodel.Execute(mm);
//            w.println(oo.getMainResultValue());
//        }
//        w.close();
//        if (1==1) return;
//        
        

        InsilicoMolecule mmmm = SmilesMolecule.Convert("CS(=O)(=O)NC1=C(OC2=CC=CC=C2)C=C(C=C1)[N+]([O-])=O");  
        ismAromataseIRFMN  model = new   ismAromataseIRFMN();     
//        ismPPBkknn model = new ismPPBkknn();
        
        InsilicoModelOutput oo = model.Execute(mmmm);
        ArrayList<SimilarMolecule> similarmolecules = oo.getSimilarMolecules(); 
        for (SimilarMolecule mol:similarmolecules){
                   System.out.println(mol.getIndex());
                   }
        System.out.println(model.getInfo().getDescriptionLong());
        System.out.println(oo.getAssessment());
         System.out.println(oo.getErrMessage());
////
//        System.out.println(oo.getMainResultValue());
//        if (1==1) return;  
//        //////
//            
//        ismDaphniaCombase noec = new ismDaphniaCombase();
////        noec.setSkipADandTSLoading(true);
//        InsilicoMolecule mmmm = SmilesMolecule.Convert("CCCCCCCCCCC");   
//        InsilicoModelOutput oo = noec.Execute(mmmm);
//        System.out.println(oo.getMainResultValue());
//        if (1==1) return;  
//        
////        MoleculeFileSmiles ifile = new MoleculeFileSmiles();
////        ifile.OpenFile("noec.txt");
////        ArrayList<InsilicoMolecule> ina = ifile.ReadAll();
////        
////        for (InsilicoMolecule mm : ina) {
////            InsilicoModelOutput oo = noec.Execute(mm);
////            System.out.println(oo.getMainResultValue());
////        }        
//        if(1==1) return;
//        
        
//        InsilicoMolecule mmmm = SmilesMolecule.Convert("c1ccc2c(c1)cc3ccc4cccc5ccc2c3c45");        
//        ismVaporPressureIRFMN vp = new ismVaporPressureIRFMN();
////        iTrainingSet ts = vp.GetTrainingSet();
////        for (int i=0; i<ts.getMoleculesSize(); i++)
////            System.out.println(ts.getMoleculeSet(i) + "\t" + ts.getExperimentalValue(i) + "\t" + ts.getPredictedValue(i));
////        if(1==1) return;
//        InsilicoModelOutput ows = vp.Execute(mmmm);
//        System.out.println(ows.getMainResultValue());
//        if(1==1) return;
        
        
//        InsilicoLogger.InitLoggerOff();
        
//        ismCarcinogenicitySFIClassification carc = new ismCarcinogenicitySFIClassification();
//        
//        MoleculeFileSmiles ifile = new MoleculeFileSmiles();
//        ifile.OpenFile("ina.txt");
//        ArrayList<InsilicoMolecule> ina = ifile.ReadAll();
//        
//        for (InsilicoMolecule mm : ina) {
//            InsilicoModelOutput oo = carc.Execute(mm);
//            System.out.println(oo.getMainResultValue());
//        }
//        if (1==1) return;
        
//        DARTLibrary dart = new DARTLibrary();
//
//        InsilicoMolecule mm = SmilesMolecule.Convert("O=C(CCl)C(Cl)(Cl)Cl");
//        DARTLibrary.DARTResult rr = (dart.Check(mm));
//        System.out.println(rr.Index + rr.SubIndex);
//        mm = SmilesMolecule.Convert("O=C(CCCl)C(Cl)(Cl)Cl");
//        rr = (dart.Check(mm));
//        System.out.println(rr.Index + rr.SubIndex);
//        
//        if (1==1) return;


//        DARTLibrary dart = new DARTLibrary();
//        dart.CheckVirtualCompoundsSMARTS();
//        if (1==1) return;
       
//        ismBCFArnotGobas argo = new ismBCFArnotGobas();
//        argo.setSkipADandTSLoading(true);
//        MoleculeFileSmiles sfo = new MoleculeFileSmiles();
//        sfo.OpenFile("bcf.txt");
//        ArrayList<InsilicoMolecule> m_sfo = sfo.ReadAll();
//        
//        int idx = 0;
//        for (InsilicoMolecule mmm : m_sfo) {
//            InsilicoModelOutput oo = argo.Execute(mmm);
//            System.out.println(oo.getMainResultValue());
//        }
        
        
        

//        Path sddir = Paths.get("C:\\Users\\Alberto\\Desktop\\group SD files");
//        VirtualCompoundLibrary.ProcessDirectorySDF(sddir,"C:\\Users\\Alberto\\Desktop\\pg_smi");
//
//        if (1==1) return;       

        
//        Path dir = Paths.get("C:\\Users\\Alberto\\Desktop\\PG nuovo\\smiles elaborate e corrette");
//        VirtualCompoundLibrary.ProcessDirectorySMI(dir);
//
//        if (1==1) return;

        

//
//        MoleculeFileSmiles sfile = new MoleculeFileSmiles();
//        sfile.OpenFile("C:\\Users\\Alberto\\Desktop\\pg.txt");
//        ArrayList<InsilicoMolecule> procter = sfile.ReadAll();
//
////        ArrayList<InsilicoMolecule> procter = new ArrayList<>();
////        procter.add(SmilesMolecule.Convert("CCCCC(CCC)C1=CC(=C(C(=C1)[N+](=O)[O-])OC(=O)/C=C/C)[N+](=O)[O-]"));
//        
//        DARTLibrary PG = new DARTLibrary();
//        System.out.println("no\tsmiles\tcategory\tindex\tstructure");
//        int pg_idx = 0;
//        for (InsilicoMolecule mmm : procter) {
//            pg_idx++;
//            if (!mmm.IsValid()) {
//                System.out.println("ERR");
//                continue;
//            }
//            DARTLibrary.DARTResult res = (PG.Check(mmm));
//            if (res == null)
//                System.out.println(pg_idx + "\t" + mmm.GetSMILES() + "\tN/A\tN/A\t-\t");
//            else 
//                System.out.println(pg_idx + "\t" + mmm.GetSMILES() + "\t" + res.Index + "\t" + res.Index + res.SubIndex + "\t" + res.Structure);
//        }         
//        
// 
//        if (1==1) return;
//        
//        
  
//        ismKmArnot km = new ismKmArnot();
//        ismLogPMeylan logP = new ismLogPMeylan();
//        
//        MoleculeFileSmiles sfo = new MoleculeFileSmiles();
//        sfo.OpenFile("bcf.txt");
//        ArrayList<InsilicoMolecule> m_sfo = sfo.ReadAll();
//        
//        int idx = 0;
//        for (InsilicoMolecule mmm : m_sfo) {
//            InsilicoModelOutput mo = km.Execute(mmm);
//            double km_da_usare = Math.pow(10, mo.getMainResultValue());
//
//            mo = logP.Execute(mmm);
//            double logp_da_usare = mo.getMainResultValue();
//
//            ArnotGobasBCFBAF ag = new ArnotGobasBCFBAF(logp_da_usare, km_da_usare); 
//            System.out.println(idx + "\t" + mmm.GetSMILES() + "\t" + km_da_usare + "\t" + logp_da_usare + "\t" + ag.getArnotLogBCFlow()+ "\t" + ag.getArnotLogBCFmid()+ "\t" + ag.getArnotLogBCFup() + "\t" + ag.getArnotLogBAFlow()+ "\t" + ag.getArnotLogBAFmid()+ "\t" + ag.getArnotLogBAFup());
//        
//            idx++;
//        }
//
//        if (1==1) return;
        
        
//        InsilicoModel m;
//        m = new ismCarcinogenicitySFORegression();
//        String InputSMILES = "O=C6OC(C)C(NC(=O)c2ccc(c1OC=5C(=Nc12)C(C(=O)NC3C(=O)NC(C(=O)N4CCCC4(C(=O)N(C)CC(=O)N(C)C(C(=O)OC3(C))C(C)C))C(C)C)=C(N)C(=O)C=5C)C)C(=O)NC(C(=O)N7CCCC7(C(=O)N(C)CC(=O)N(C)C6C(C)C))C(C)C"; 
//        InsilicoMolecule mm = SmilesMolecule.Convert(InputSMILES);
//        InsilicoModelOutput o = m.Execute(mm);
//        System.out.println(o.getMainResultValue());
//        
//        if (1==1) return;
        
//        BuildPNG();
//        if (1==1) return;        
//        
//        
//        InsilicoModelRunnerByMolecule models = new InsilicoModelRunnerByMolecule();
//        models.AddModel(new ismBeeKnn());
//  
//        ArrayList<InsilicoMolecule> ds = new ArrayList<>();
//        ds.add(SmilesMolecule.Convert("c1ccc(Br)cc1Cl"));
//        ds.add(SmilesMolecule.Convert("c1ccc(Br)cc1N"));
//        ds.add(SmilesMolecule.Convert("O=[N+]([O-])c1oc(cc1)c2cnc(NN=C(C)C)s2"));
//        
//        models.Run(ds);
//        ReportTXTSingle.PrintReport(ds, models.GetModelWrappers().get(0));
//        
//        if (1==1) return;
        
        
        

        
        
//        ismCarcinogenicitySFOClassification loel = new ismCarcinogenicitySFOClassification();
//        iTrainingSet ts = loel.GetTrainingSet();
//        for (int i=0; i<ts.getMoleculesSize(); i++) {
//            System.out.println(ts.getSMILES(i) + "\t" + ts.getExperimentalValueFormatted(i) +
//                    "\t" + ts.getPredictedValueFormatted(i));
//        }
//        
//        if (1==1) return;
        
        
//        InsilicoModel mdl2 = new ismHepatotoxicityIrfmn();
//        InsilicoModelOutput oooo = mdl2.Execute(SmilesMolecule.Convert("N#CC(c1cc(cc(c1)C(C#N)(C)C)Cn2ncnc2)(C)CCCC=O"));
//        System.out.println(oooo.getAssessmentVerbose());
//        if (1==1) return;
        
        
        
//        // Alert Muta-ISS (regole Benigni-Bossa, SOLO mutagenicity)
//        SABenigniBossa BBAlerts = new SABenigniBossa();
//        SABenigniBossaAdditional BBAlertsAdd = new SABenigniBossaAdditional();
//        
//        AlertList BBList = new AlertList();
//        for (Alert a : BBAlerts.getAlerts().getSAList()) 
//            if (a.getBoolProperty(SABenigniBossa.KEY_BBSA_IS_MUTAGEN))
//                BBList.add(a);
//        for (Alert a : BBAlertsAdd.getAlerts().getSAList()) 
//            if (a.getBoolProperty(SABenigniBossa.KEY_BBSA_IS_MUTAGEN))
//                BBList.add(a);
//        
//        // Se hai bisogno degli header, con il codice seguente
//        // estrai i nomi degli alert
//        String alertNames = "";
//        for (Alert a : BBList.getSAList()) 
//            alertNames += a.getName() + "\t";
//        System.out.println("Input mol\t" + alertNames);
//        
//        InsilicoMolecule m;
//        
//        // Se l'input e' una SMILES usa la seguente
//        // altrimenti hai gia' un oggetto InsilicoMolecule e usi direttamente
//        // quello come input
//        String InputSMILES = "O=[N+]([O-])c1oc(cc1)c2cnc(NN=C(C)C)s2"; 
//        m = SmilesMolecule.Convert(InputSMILES);
//
//        
//        // Calcola gli alert per la molecola m - ovviamente puoi prendere il 
//        // codice seguente e applicarlo dentro un ciclo a tutte le tue mol
//        boolean[] Results = new boolean[BBList.size()];
//        for (int i=0; i<BBList.size(); i++) 
//            Results[i] = false;
//        
//        AlertList ResBase = BBAlerts.Calculate(m);
//        AlertList ResAdd = BBAlertsAdd.Calculate(m);
//        
//        for (int i=0; i<BBList.size(); i++) {
//            Alert curAlert = BBList.getSAList().get(i);
//            for (Alert a : ResBase.getSAList())
//                if (a.getId().compareTo(curAlert.getId())==0)
//                    Results[i] = true;
//            for (Alert a : ResAdd.getSAList())
//                if (a.getId().compareTo(curAlert.getId())==0)
//                    Results[i] = true;
//        }
//        
//        // Output dei risultati, se ti serve
//        String s = m.GetSMILES();
//        for (int i=0; i<BBList.size(); i++)
//            s += "\t" + Results[i];
//        System.out.println(s);
//        
//        
//        
//        
//        if(1==1) return;
//        
//        
//        InsilicoModel mdl = new ismCarcinogenicitySlopeFactor();
//        mdl.setSkipADandTSLoading(true);
//        InsilicoModelOutput ooo = mdl.Execute(SmilesMolecule.Convert("O=C(Oc2cccc1c2(OC(C)(C)C1))N(C)SN(CCCC)CCCC"));
//        
//        for (int i=0; i<mdl.getDescriptorsSize(); i++)
//            System.out.print(mdl.getDescriptorsNames()[i] + "\t");
//        System.out.println();
//        for (int i=0; i<mdl.getDescriptorsSize(); i++)
//            System.out.print(mdl.GetDescriptor(i) + "\t");
//        System.out.println();
//        
//        if (1==1) return;

        
        
//        InsilicoModelRunnerByMolecule models = new InsilicoModelRunnerByMolecule();
//        models.AddModel(new ismEstrogenBindingCerapp());
//  
//        MoleculeFileSmiles SMI = new MoleculeFileSmiles();
//        SMI.OpenFile("giap.txt");
//        ArrayList<InsilicoMolecule> ds = SMI.ReadAll();
//
//        models.Run(ds);
//        ReportTXTSingle.PrintReport(ds, models.GetModelWrappers().get(0));
//        
//        if (1==1) return;
        
        
        
        
        
        
//        BuildDataset();
//        if (1==1) return;

//        BuildPNG();
//        if (1==1) return;

//        MoleculeFileSmiles SMI = new MoleculeFileSmiles();
//        SMI.OpenFile("marjana_fish.smi");
//        ArrayList<InsilicoMolecule> Mols = SMI.ReadAll();
//        SMI.CloseFile();

/*        
        InsilicoModel mm = new ismFishNic();
//        mm.setSkipADandTSLoading(true);
//        for (int i=0; i<mm.getDescriptorsSize(); i++)
//            System.out.print(mm.getDescriptorsNames()[i] + "\t");
//        System.out.println();
        
        for (InsilicoMolecule m : Mols) {
            InsilicoModelOutput mo = mm.Execute(m);
//            for (int i=0; i<mm.getDescriptorsSize(); i++)
//                System.out.print(mm.GetDescriptor(i) + "\t");
            System.out.println(mo.getMainResultValue());
        }
        
        if (1==1) return;
  */      
        
        
        if (1==1) return;
        
        
        
//        InsilicoModel bcf2 = new ismBCFMeylan();
//        TrainingSet TSS = (TrainingSet)bcf2.GetTrainingSet();
//        TSS.PrintToStdOut(true);
//        
//        if (1==1) return;
        
//        MoleculeFileSDF sdf = new MoleculeFileSDF();
//        sdf.OpenFile("BCF860.sdf");
//        ArrayList<InsilicoMolecule> ds = sdf.ReadAll();;
//        
//        InsilicoModel bcf = new ismBCFCaesar();
//        
//        for (int i=0; i<bcf.getDescriptorsSize(); i++)
//            System.out.print(bcf.getDescriptorsNames()[i] + "\t");
//        System.out.println();
//        
//        for (InsilicoMolecule m : ds) {
//            bcf.Execute(m);
//            for (int i=0; i<bcf.getDescriptorsSize(); i++)
//                System.out.print(bcf.GetDescriptor(i) + "\t");
//            System.out.println();
//        }
//        
//        if (1==1) return;
        
       
    }
}
