/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package insilico.model.ppb.kknn;

import insilico.core.ad.ADCheckACF;
import insilico.core.ad.ADCheckIndicesQuantitative;
import insilico.core.ad.item.ADIndexACF;
import insilico.core.ad.item.ADIndexADIAggregate;
import insilico.core.ad.item.ADIndexAccuracy;
import insilico.core.ad.item.ADIndexConcordance;
import insilico.core.ad.item.ADIndexMaxError;
import insilico.core.ad.item.ADIndexSimilarity;
import insilico.core.ad.item.iADIndex;
import insilico.core.constant.MessagesAD;
import insilico.core.descriptor.DescriptorBlock;
import insilico.core.descriptor.DescriptorsEngine;
import insilico.core.exception.GenericFailureException;
import insilico.core.exception.InitFailureException;
import insilico.core.model.InsilicoModel;
import insilico.core.model.iInsilicoModel;
import insilico.core.model.trainingset.TrainingSetForKNNModels;
import insilico.core.kknn.KKNNModel;
import insilico.core.tools.ModelUtilities;
import java.util.ArrayList;
import insilico.core.descriptor.blocks.AtomCenteredFragments;
import insilico.core.descriptor.blocks.AutoCorrelation;
import insilico.core.descriptor.blocks.Constitutional;
import insilico.core.descriptor.blocks.FunctionalGroups;
import insilico.core.descriptor.blocks.PVSA;



/**
 *
 * @author kosmu
 */
public class ismPPBkknn extends InsilicoModel{
    

  private static final long serialVersionUID = 1L;
  
  private static final String ModelData = "/insilico/model/ppb/kknn/data/model_ppb_kknn.xml";
  
  
  private final double[] Scaling_a = new double[] { 3.5370843989769822D, 3.856560783960346D, -0.03387469721111648D, -0.10508931674940925D, 0.6170238277597968D, 2.657289002557545D, 0.22506393861892582D, 0.21994884910485935D, 0.14066496163682865D, 78.51069875307833D, 87.46735867136897D, 39.22878914743523D };
  
  private final double[] Scaling_s = new double[] { 5.96984135772872D, 0.659460276333299D, 0.3613066018922694D, 0.11762769273291303D, 0.038593831950676234D, 2.0594228257542744D, 0.6038219583948833D, 0.5040461898273225D, 0.5667347480444093D, 66.91139520241951D, 51.531270372701464D, 40.59997969146719D };
  
  public ismPPBkknn() throws InitFailureException {
    super("/insilico/model/ppb/kknn/data/model_ppb_kknn.xml");
    
    //add descriptors
    this.DescriptorsSize = 12;
    this.DescriptorsNames = new String[this.DescriptorsSize];
    this.DescriptorsNames[0] = "H-046";
    this.DescriptorsNames[1] = "ATS4m";
    this.DescriptorsNames[2] = "MATS7p";
    this.DescriptorsNames[3] = "MATS1e";
    this.DescriptorsNames[4] = "Mv";
    this.DescriptorsNames[5] = "nN";
    this.DescriptorsNames[6] = "nR=Ct";
    this.DescriptorsNames[7] = "nRCOOH";
    this.DescriptorsNames[8] = "nRNH2";
    this.DescriptorsNames[9] = "P_VSA_m_3";
    this.DescriptorsNames[10] = "P_VSA_i_2";
    this.DescriptorsNames[11] = "P_VSA_i_4";
   
    
    
    this.ResultsSize = 1;
    this.ResultsName = new String[this.ResultsSize];
    this.ResultsName[0] = "Predicted Fraction Unbound [square root]";
    this.ADItemsName = new String[5];
    this.ADItemsName[0] = (new ADIndexSimilarity()).GetIndexName();
    this.ADItemsName[1] = (new ADIndexAccuracy()).GetIndexName();
    this.ADItemsName[2] = (new ADIndexConcordance()).GetIndexName();
    this.ADItemsName[3] = (new ADIndexMaxError()).GetIndexName();
    this.ADItemsName[4] = (new ADIndexACF()).GetIndexName();
  }
  
  public ArrayList<DescriptorBlock> GetRequiredDescriptorBlocks() {
    ArrayList<DescriptorBlock> blocks = new ArrayList<>();
    AtomCenteredFragments atomCenteredFragments = new AtomCenteredFragments();
    blocks.add(atomCenteredFragments);
    AutoCorrelation autoCorrelation = new AutoCorrelation();
    autoCorrelation.setBoolProperty("lag01", true);
    autoCorrelation.setBoolProperty("lag04", true);
    autoCorrelation.setBoolProperty("lag07", true);
    autoCorrelation.setBoolProperty("weightm", true);
    autoCorrelation.setBoolProperty("weightp", true);
    autoCorrelation.setBoolProperty("weighte", true);
    blocks.add(autoCorrelation);
    Constitutional constitutional= new  Constitutional();
    blocks.add(constitutional);
    FunctionalGroups functionalGroups= new  FunctionalGroups();
    blocks.add(functionalGroups);
        PVSA pvsa= new  PVSA();
        pvsa.setBoolProperty("weightm", true);
        pvsa.setBoolProperty("weighti", true);
    blocks.add(pvsa);


    return blocks;

  }
  
  protected short CalculateDescriptors(DescriptorsEngine DescEngine) {
    try {
      this.Descriptors = new double[this.DescriptorsSize];
      this.Descriptors[0] = DescEngine.getDescriptorBlock(AtomCenteredFragments.class).GetByName("H-046").getValue();
      this.Descriptors[1] = DescEngine.getDescriptorBlock(AutoCorrelation.class).GetByName("ATS4m").getValue();
      this.Descriptors[2] = DescEngine.getDescriptorBlock(AutoCorrelation.class).GetByName("MATS7p").getValue();
      this.Descriptors[3] = DescEngine.getDescriptorBlock(AutoCorrelation.class).GetByName("MATS1e").getValue();
      this.Descriptors[4] = DescEngine.getDescriptorBlock(Constitutional.class).GetByName("Mv").getValue();
      this.Descriptors[5] = DescEngine.getDescriptorBlock(Constitutional.class).GetByName("nN").getValue();
      this.Descriptors[6] = DescEngine.getDescriptorBlock(FunctionalGroups.class).GetByName("nR=Ct").getValue();
      this.Descriptors[7] = DescEngine.getDescriptorBlock(FunctionalGroups.class).GetByName("nRCOOH").getValue();
      this.Descriptors[8] = DescEngine.getDescriptorBlock(FunctionalGroups.class).GetByName("nRNH2").getValue();
      this.Descriptors[9] = DescEngine.getDescriptorBlock(PVSA.class).GetByName("P_VSA_m_3").getValue();
      this.Descriptors[10] = DescEngine.getDescriptorBlock(PVSA.class).GetByName("P_VSA_i_2").getValue();
      this.Descriptors[11] = DescEngine.getDescriptorBlock(PVSA.class).GetByName("P_VSA_i_4").getValue();
      for (int i = 0; i < this.DescriptorsSize; i++)
        this.Descriptors[i] = (this.Descriptors[i] - this.Scaling_a[i]) / this.Scaling_s[i]; 
    } catch (Throwable e) {
      return -2;
    } 
    return 1;
  }
  
  protected short CalculateModel() {
    int KKNN_K = 5;
    short KKNN_Distance = 1;
    KKNNModel KKNNKNN = new KKNNModel(this.TS, this.DescriptorsSize, KKNN_K, KKNN_Distance);
    double KKNNResult = 0.0D;
    try {
      KKNNResult = KKNNKNN.Calculate(this.Descriptors);
    } catch (Exception ex) {
      return -1;
    } 
    this.CurOutput.setMainResultValue(KKNNResult);
    String[] Res = new String[this.ResultsSize];
    Res[0] = String.valueOf(this.Format_4D.format(KKNNResult));
    this.CurOutput.setResults(Res);
    return 1;
  }
  
  protected short CalculateAD() {
    ADCheckIndicesQuantitative adq = new ADCheckIndicesQuantitative(this.TS);
    adq.setMoleculesForIndexSize(3);
    double Val = this.CurOutput.HasExperimental() ? this.CurOutput.getExperimental() : this.CurOutput.getMainResultValue();
    if (Val == -999.0D) {
      try {
        adq.SetSimilarMolecules(this.CurMolecule, this.CurOutput);
      } catch (GenericFailureException genericFailureException) {}
      return -1;
    } 
    if (!adq.Calculate(this.CurMolecule, this.CurOutput))
      return -1; 
    try {
      ((ADIndexSimilarity)this.CurOutput.getADIndex(ADIndexSimilarity.class)).SetThresholds(0.75D, 0.7D);
      ((ADIndexAccuracy)this.CurOutput.getADIndex(ADIndexAccuracy.class)).SetThresholds(1.0D, 0.5D);
      ((ADIndexConcordance)this.CurOutput.getADIndex(ADIndexConcordance.class)).SetThresholds(1.0D, 0.5D);
      ((ADIndexMaxError)this.CurOutput.getADIndex(ADIndexMaxError.class)).SetThresholds(1.0D, 0.5D);
    } catch (Throwable e) {
      return -1;
    } 
    ADCheckACF adacf = new ADCheckACF(this.TS);
    if (!adacf.Calculate(this.CurMolecule, this.CurOutput))
      return -1; 
    double acfContribution = this.CurOutput.getADIndex(ADIndexACF.class).GetIndexValue();
    double ADIValue = adq.getIndexADI() * acfContribution;
    ADIndexADIAggregate ADI = new ADIndexADIAggregate(0.75D, 0.7D, 1.0D, 0.85D, 0.7D);
    ADI.SetValue(ADIValue, this.CurOutput.getADIndex(ADIndexAccuracy.class), this.CurOutput
        .getADIndex(ADIndexConcordance.class), this.CurOutput
        .getADIndex(ADIndexMaxError.class));
    this.CurOutput.setADI((iADIndex)ADI);
    return 1;
  }
  
  protected void CalculateAssessment() {
    String ADItemWarnings = ModelUtilities.BuildADItemsWarningMsg(this.CurOutput.getADIndex());
    String Result = this.CurOutput.getResults()[0] + " square root";
    if (this.CurOutput.getMainResultValue() == -999.0D) {
      this.CurOutput.setAssessment("N/A");
      this.CurOutput.setAssessmentVerbose(String.format(MessagesAD.ASSESS_LONG_NA, new Object[] { "N/A" }));
    } else {
      switch (this.CurOutput.getADI().GetAssessmentClass()) {
        case 1:
          this.CurOutput.setAssessment(String.format(MessagesAD.ASSESS_SHORT_LOW, new Object[] { Result }));
          this.CurOutput.setAssessmentVerbose(String.format(MessagesAD.ASSESS_LONG_LOW, new Object[] { Result, ADItemWarnings }));
          break;
        case 2:
          this.CurOutput.setAssessment(String.format(MessagesAD.ASSESS_SHORT_MEDIUM, new Object[] { Result }));
          this.CurOutput.setAssessmentVerbose(String.format(MessagesAD.ASSESS_LONG_MEDIUM, new Object[] { Result, ADItemWarnings }));
          break;
        case 3:
          this.CurOutput.setAssessment(String.format(MessagesAD.ASSESS_SHORT_HIGH, new Object[] { Result }));
          this.CurOutput.setAssessmentVerbose(String.format(MessagesAD.ASSESS_LONG_HIGH, new Object[] { Result }));
          if (!ADItemWarnings.isEmpty())
            this.CurOutput.setAssessmentVerbose(this.CurOutput.getAssessmentVerbose() + 
                String.format(MessagesAD.ASSESS_LONG_ADD_ISSUES, new Object[] { ADItemWarnings })); 
          break;
      } 
    } 
    if (this.CurOutput.HasExperimental()) {
      this.CurOutput.setAssessmentVerbose(String.format(MessagesAD.ASSESS_LONG_EXPERIMENTAL, new Object[] { this.CurOutput.getExperimentalFormatted() + " square root", this.CurOutput.getAssessment() }));
      this.CurOutput.setAssessment(String.format(MessagesAD.ASSESS_SHORT_EXPERIMENTAL, new Object[] { this.CurOutput.getExperimentalFormatted() + " square root" }));
    } 
    this.CurOutput.setAssessmentStatus((short)0);
  }
  
  public void ProcessTrainingSet() throws Exception {
    setSkipADandTSLoading(true);
    TrainingSetForKNNModels TSK = new TrainingSetForKNNModels();
    TSK.SetCalculateDescriptors(true);
    String TSPath = getInfo().getTrainingSetURL();
    String[] buf = TSPath.split("/");
    String DatName = buf[buf.length - 1];
    TSPath = TSPath.substring(0, TSPath.length() - 3) + "txt";
    TSK.Build(TSPath, (iInsilicoModel)this, null);
    TSK.SerializeToFile(DatName);
  }
}
