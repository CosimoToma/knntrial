 package insilico.model.aromatase2.irfmn;
 
 import insilico.core.descriptor.DescriptorBlock;
 import insilico.core.descriptor.weight.GhoseCrippenWeights;
 import insilico.core.descriptor.weight.IonizationPotential;
 import insilico.core.descriptor.weight.Mass;
 import insilico.core.exception.InvalidMoleculeException;
 import insilico.core.molecule.InsilicoMolecule;
 import insilico.core.molecule.tools.Manipulator;
 import insilico.core.tools.MoleculeUtilities;
 import java.util.ArrayList;
 import org.openscience.cdk.Atom;
 import org.openscience.cdk.Molecule;
 import org.openscience.cdk.interfaces.IAtom;
 
 
 
 
 
 
 
 
 
 
 public class AromatasePVSA
   extends DescriptorBlock
 {
   private static final long serialVersionUID = 1L;
   private static final String BlockName = "PVSA Descriptors";
   public static final String PARAMETER_WEIGHT_M = "weightm";
   public static final String PARAMETER_WEIGHT_LOGP = "weightlogp";
   public static final String PARAMETER_WEIGHT_I = "weighti";
   public static final String PARAMETER_WEIGHT_MR = "weightmr";
   private static final short WEIGHT_M_IDX = 0;
   private static final short WEIGHT_LOGP_IDX = 1;
   private static final short WEIGHT_I_IDX = 2;
   private static final short WEIGHT_MR_IDX = 3;
   private static final String[] WEIGHT_SYMBOL = new String[] { "m", "logp", "i", "mr" };
   
   private static final Object[][] RefBondLengths = new Object[][] { { "Br", "Br", 
         Double.valueOf(2.54D) }, { "Br", "C", 
         Double.valueOf(1.97D) }, { "Br", "Cl", 
         Double.valueOf(2.36D) }, { "Br", "F", 
         Double.valueOf(1.85D) }, { "Br", "H", 
         Double.valueOf(1.44D) }, { "Br", "I", 
         Double.valueOf(2.65D) }, { "Br", "N", 
         Double.valueOf(1.84D) }, { "Br", "O", 
         Double.valueOf(1.58D) }, { "Br", "P", 
         Double.valueOf(2.37D) }, { "Br", "S", 
         Double.valueOf(2.21D) }, { "C", "C", 
         Double.valueOf(1.54D) }, { "C", "Cl", 
         Double.valueOf(1.8D) }, { "C", "F", 
         Double.valueOf(1.35D) }, { "C", "H", 
         Double.valueOf(1.06D) }, { "C", "I", 
         Double.valueOf(2.12D) }, { "C", "N", 
         Double.valueOf(1.47D) }, { "C", "O", 
         Double.valueOf(1.43D) }, { "C", "P", 
         Double.valueOf(1.85D) }, { "C", "S", 
         Double.valueOf(1.81D) }, { "Cl", "Cl", 
         Double.valueOf(2.31D) }, { "Cl", "F", 
         Double.valueOf(1.63D) }, { "Cl", "H", 
         Double.valueOf(1.22D) }, { "Cl", "I", 
         Double.valueOf(2.56D) }, { "Cl", "N", 
         Double.valueOf(1.74D) }, { "Cl", "O", 
         Double.valueOf(1.41D) }, { "Cl", "P", 
         Double.valueOf(2.01D) }, { "Cl", "S", 
         Double.valueOf(2.07D) }, { "F", "F", 
         Double.valueOf(1.28D) }, { "F", "H", 
         Double.valueOf(0.87D) }, { "F", "I", 
         Double.valueOf(2.04D) }, { "F", "N", 
         Double.valueOf(1.41D) }, { "F", "O", 
         Double.valueOf(1.32D) }, { "F", "P", 
         Double.valueOf(1.5D) }, { "F", "S", 
         Double.valueOf(1.64D) }, { "H", "I", 
         Double.valueOf(1.63D) }, { "H", "N", 
         Double.valueOf(1.01D) }, { "H", "O", 
         Double.valueOf(0.97D) }, { "H", "P", 
         Double.valueOf(1.41D) }, { "H", "S", 
         Double.valueOf(1.31D) }, { "I", "I", 
         Double.valueOf(2.92D) }, { "I", "N", 
         Double.valueOf(2.26D) }, { "I", "O", 
         Double.valueOf(2.14D) }, { "I", "P", 
         Double.valueOf(2.49D) }, { "I", "S", 
         Double.valueOf(2.69D) }, { "N", "N", 
         Double.valueOf(1.45D) }, { "N", "O", 
         Double.valueOf(1.46D) }, { "N", "P", 
         Double.valueOf(1.6D) }, { "N", "S", 
         Double.valueOf(1.76D) }, { "O", "O", 
         Double.valueOf(1.47D) }, { "O", "P", 
         Double.valueOf(1.57D) }, { "O", "S", 
         Double.valueOf(1.57D) }, { "P", "P", 
         Double.valueOf(2.26D) }, { "P", "S", 
         Double.valueOf(2.07D) }, { "S", "S", 
         Double.valueOf(2.05D) } };
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
   
   protected final void GenerateDescriptors() {
     this.DescList.clear();
     ArrayList<Integer> weightList = BuildWeightList();
     for (Integer curWeight : weightList) {
       int bins = BuildBinSize(curWeight.intValue());
       for (int i = 1; i <= bins; i++)
         Add("P_VSA_" + WEIGHT_SYMBOL[curWeight.intValue()] + "_" + i, ""); 
     } 
     SetAllValues(-999.0D);
   }
 
   
   private ArrayList<Integer> BuildWeightList() {
     ArrayList<Integer> w = new ArrayList<>();
     if (getBoolProperty("weightm"))
       w.add(new Integer(0)); 
     if (getBoolProperty("weightlogp"))
       w.add(new Integer(1)); 
     if (getBoolProperty("weighti"))
       w.add(new Integer(2)); 
     if (getBoolProperty("weightmr"))
       w.add(new Integer(3)); 
     return w;
   }
 
   
   private int BuildBinSize(int curWeight) {
     if (curWeight == 0)
       return 5; 
     if (curWeight == 1)
       return 8; 
     if (curWeight == 2)
       return 4; 
     if (curWeight == 3)
       return 8; 
     return 0;
   }
 
 
 
 
 
 
 
 
   
   public void Calculate(InsilicoMolecule mol) {
     Molecule m;
     GenerateDescriptors();
 
     
     try {
       Molecule orig_m = mol.GetStructure();
       m = Manipulator.AddHydrogens(orig_m);
     } catch (InvalidMoleculeException|insilico.core.exception.GenericFailureException e) {
       SetAllValues(-999.0D);
       
       return;
     } 
     int nSK = m.getAtomCount();
 
 
     
     double[] VSA = new double[nSK];
     
     for (int i = 0; i < nSK; i++) {
       
       IAtom at = m.getAtom(i);
 
       
       double vdwR = GetVdWRadius(m, at);
       if (vdwR != -999.0D) {
         
         double coef = 0.0D;
         for (IAtom connAt : m.getConnectedAtomsList(at)) {
           
           double connAt_vdwR = GetVdWRadius(m, connAt);
           if (connAt_vdwR == -999.0D) {
             continue;
           }
           double refR = GetRefBondLength(at, connAt);
           if (refR == -999.0D) {
             continue;
           }
           double c = 0.0D;
           double bnd = MoleculeUtilities.Bond2Double(m.getBond(at, connAt));
           if (bnd == 1.5D) c = 0.1D; 
           if (bnd == 2.0D) c = 0.2D; 
           if (bnd == 3.0D) c = 0.3D;
           
           double g_1 = Math.max(Math.abs(vdwR - connAt_vdwR), GetRefBondLength(at, connAt) - c);
           double g_2 = vdwR + connAt_vdwR;
           double g = Math.min(g_1, g_2);
           
           coef += (Math.pow(connAt_vdwR, 2.0D) - Math.pow(vdwR - g, 2.0D)) / g;
         } 
         
         VSA[i] = 12.566370614359172D * Math.pow(vdwR, 2.0D) - Math.PI * vdwR * coef;
       } 
     } 
 
 
 
     
     ArrayList<Integer> weightList = BuildWeightList();
     
     for (Integer curWeight : weightList) {
 
       
       double[] w = null;
       if (curWeight.intValue() == 0)
         w = Mass.getWeights(m); 
       if (curWeight.intValue() == 1)
         w = GhoseCrippenWeights.GetHydrophobiticty(m); 
       if (curWeight.intValue() == 2)
         w = IonizationPotential.getWeightsNormalized(m); 
       if (curWeight.intValue() == 3) {
         w = AromataseGCWeights.GetMolarRefractivity(m);
       }
       int bins = BuildBinSize(curWeight.intValue());
       
       for (int b = 1; b <= bins; b++) {
         
         double PVSA = 0.0D;
         for (int j = 0; j < nSK; j++) {
           if (w[j] != -999.0D && 
             b == CalculateBin(curWeight.intValue(), w[j])) {
             PVSA += VSA[j];
           }
         } 
         SetByName("P_VSA_" + WEIGHT_SYMBOL[curWeight.intValue()] + "_" + b, PVSA);
       } 
     } 
   }
 
 
   
   private int CalculateBin(int curWeight, double value) {
     if (curWeight == 0) {
       
       if (value < 1.0D) return 1; 
       if (value < 1.2D) return 2; 
       if (value < 1.6D) return 3; 
       if (value < 3.0D) return 4; 
       return 5;
     } 
     
     if (curWeight == 1) {
       
       if (value < -1.5D) return 1; 
       if (value < -0.5D) return 2; 
       if (value < -0.25D) return 3; 
       if (value < 0.0D) return 4; 
       if (value < 0.25D) return 5; 
       if (value < 0.52D) return 6; 
       if (value < 0.75D) return 7; 
       return 8;
     } 
     
     if (curWeight == 2) {
       
       if (value < 1.0D) return 1; 
       if (value < 1.15D) return 2; 
       if (value < 1.25D) return 3; 
       return 4;
     } 
     
     if (curWeight == 3) {
       
       if (value < 0.9D) return 1; 
       if (value < 1.5D) return 2; 
       if (value < 2.0D) return 3; 
       if (value < 2.5D) return 4; 
       if (value < 3.0D) return 5; 
       if (value < 4.0D) return 6; 
       if (value < 6.0D) return 7; 
       return 8;
     } 
     
     return 0;
   }
 
   
   private boolean AtomCouple(IAtom at1, IAtom at2, String symbol1, String symbol2) {
     if (at1.getSymbol().equalsIgnoreCase(symbol1) && at2.getSymbol().equalsIgnoreCase(symbol2))
       return true; 
     if (at1.getSymbol().equalsIgnoreCase(symbol2) && at2.getSymbol().equalsIgnoreCase(symbol1))
       return true; 
     return false;
   }
 
   
   private double GetRefBondLength(IAtom at1, IAtom at2) {
     double len = -999.0D;
     for (int i = 0; i < RefBondLengths.length; i++) {
       if (AtomCouple(at1, at2, (String)RefBondLengths[i][0], (String)RefBondLengths[i][1])) {
         len = ((Double)RefBondLengths[i][2]).doubleValue();
         break;
       } 
     } 
     return len;
   }
 
 
   
   private double GetVdWRadius(Molecule m, IAtom at) {
     String s = at.getSymbol();
     if (s.equalsIgnoreCase("C"))
       return 1.95D; 
     if (s.equalsIgnoreCase("N"))
       return 1.95D; 
     if (s.equalsIgnoreCase("F"))
       return 1.496D; 
     if (s.equalsIgnoreCase("P"))
       return 2.287D; 
     if (s.equalsIgnoreCase("S"))
       return 2.185D; 
     if (s.equalsIgnoreCase("Cl"))
       return 2.044D; 
     if (s.equalsIgnoreCase("Br"))
       return 2.166D; 
     if (s.equalsIgnoreCase("I")) {
       return 2.358D;
     }
     if (s.equalsIgnoreCase("O")) {
 
       
       if (m.getConnectedAtomsList(at).size() == 1) {
         return 1.81D;
       }
       
       if (Manipulator.CountImplicitHydrogens((Atom)at) > 0) {
         return 2.152D;
       }
       return 1.779D;
     } 
     
     if (s.equalsIgnoreCase("H")) {
       IAtom connAt = m.getConnectedAtomsList(at).get(0);
       if (connAt.getSymbol().equalsIgnoreCase("0"))
         return 0.8D; 
       if (connAt.getSymbol().equalsIgnoreCase("N"))
         return 0.7D; 
       if (connAt.getSymbol().equalsIgnoreCase("P"))
         return 0.7D; 
       return 1.485D;
     } 
     
     return -999.0D;
   }
 
 
 
 
 
 
 
 
   
   public DescriptorBlock CreateClone() throws CloneNotSupportedException {
     AromatasePVSA block = new AromatasePVSA();
     block.CloneDetailsFrom(this);
     return block;
   }
 }


/* Location:              D:\seafile\Seafile\Skin Sens\vega-1.1.5-b39\lib\insilicoModel.jar!\insilico\model\aromatase\irfmn\AromatasePVSA.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */