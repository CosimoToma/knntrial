 package insilico.model.aromatase2.irfmn;
 
 import insilico.core.molecule.matrix.ConnectionAugMatrix;
 import org.openscience.cdk.Molecule;
 import org.openscience.cdk.interfaces.IAtom;
 
 
 
 
 
 
 
 
 
 public class AromataseGCWeights
 {
   static final double[] Hydrophobicity = new double[] { 0.0D, -1.5603D, -1.012D, -0.6681D, -0.3698D, -1.788D, -1.2486D, -1.0305D, -0.6805D, -0.3858D, 0.7555D, -0.2849D, 0.02D, 0.7894D, 1.6422D, -0.7866D, -0.3962D, 0.0383D, -0.8051D, -0.2129D, 0.2432D, 0.4697D, 0.2952D, 0.0D, -0.3251D, 0.1492D, 0.1539D, 5.0E-4D, 0.2361D, 0.3514D, 0.1814D, 0.0901D, 0.5142D, -0.3723D, 0.2813D, 0.1191D, -0.132D, -0.0244D, -0.2405D, -0.0909D, -0.1002D, 0.4182D, -0.2147D, -9.0E-4D, 0.1388D, 0.0D, 0.7341D, 0.6301D, 0.518D, -0.0371D, -0.1036D, 0.5234D, 0.6666D, 0.5372D, 0.6338D, 0.362D, -0.3567D, -0.0127D, -0.0233D, -0.1541D, 0.0324D, 1.052D, -0.7941D, 0.4165D, 0.6601D, 0.0D, -0.5427D, -0.3168D, 0.0132D, -0.3883D, -0.0389D, 0.1087D, -0.5113D, 0.1259D, 0.1349D, -0.1624D, -2.0585D, -1.915D, 0.4208D, -1.4439D, 0.0D, 0.4797D, 0.2358D, 0.1029D, 0.3566D, 0.1988D, 0.7443D, 0.5337D, 0.2996D, 0.8155D, 0.4856D, 0.8888D, 0.7452D, 0.5034D, 0.8995D, 0.5946D, 1.4201D, 1.1472D, 0.0D, 0.7293D, 0.7173D, 0.0D, -2.6737D, -2.4178D, -3.1121D, 0.0D, 0.6146D, 0.5906D, 0.8758D, -0.4979D, -0.3786D, 1.5188D, 1.0255D, 0.0D, 0.0D, 0.0D, -0.9359D, -0.1726D, -0.7966D, 0.6705D, -0.4801D };
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
   
   static final double[] MolarRefractivity = new double[] { 0.0D, 2.968D, 2.9116D, 2.8028D, 2.6205D, 3.015D, 2.9244D, 2.6329D, 2.504D, 2.377D, 2.5559D, 2.303D, 2.3006D, 2.9627D, 2.3038D, 3.2001D, 4.2654D, 3.9392D, 3.6005D, 4.487D, 3.2001D, 3.4825D, 4.2817D, 3.9556D, 3.4491D, 3.8821D, 3.7593D, 2.5009D, 2.5D, 3.0627D, 2.5009D, 0.0D, 2.6632D, 3.4671D, 3.6842D, 2.9372D, 4.019D, 4.777D, 3.9031D, 3.9964D, 3.4986D, 3.4997D, 2.7784D, 2.6267D, 2.5D, 0.0D, 0.8447D, 0.8939D, 0.8005D, 0.832D, 0.8D, 0.8188D, 0.9215D, 0.9769D, 0.7701D, 0.0D, 1.7646D, 1.4778D, 1.4429D, 1.6191D, 1.3502D, 1.945D, 0.0D, 0.0D, 11.1366D, 13.1149D, 2.6221D, 2.5D, 2.898D, 3.6841D, 4.2808D, 3.6189D, 2.5D, 2.7956D, 2.7D, 4.2063D, 4.0184D, 3.0009D, 4.7142D, 0.0D, 0.0D, 0.8725D, 1.1837D, 1.1573D, 0.8001D, 1.5013D, 5.6156D, 6.1022D, 5.9921D, 5.3885D, 6.1363D, 8.5991D, 8.9188D, 8.8006D, 8.2065D, 8.7352D, 13.9462D, 14.0792D, 14.073D, 12.9918D, 13.3408D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 7.8916D, 7.7935D, 9.4338D, 7.7223D, 5.7558D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 5.5306D, 5.5152D, 6.836D, 10.0101D, 5.2806D };
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
   
   public static double GetHydrophobiticty(int FragmentIndex) {
     if (FragmentIndex >= 1 && FragmentIndex <= 120) {
       return Hydrophobicity[FragmentIndex];
     }
     return 0.0D;
   }
 
 
 
 
 
 
 
 
 
   
   public static double GetMolarRefractivity(int FragmentIndex) {
     if (FragmentIndex >= 1 && FragmentIndex <= 120) {
       return MolarRefractivity[FragmentIndex];
     }
     return 0.0D;
   }
 
 
   
   private static boolean IsAtomElectronegative(int AtomicNumber) {
     boolean ans = false;
     if (AtomicNumber == 7 || AtomicNumber == 8 || AtomicNumber == 15 || AtomicNumber == 16 || AtomicNumber == 34 || AtomicNumber == 9 || AtomicNumber == 5 || AtomicNumber == 14 || AtomicNumber == 17 || AtomicNumber == 35 || AtomicNumber == 53)
     {
 
 
       
       ans = true;
     }
     return ans;
   }
   
   public static double[] GetHydrophobiticty(Molecule CurMol) {
     double[][] ConnAugMatrix;
     int[] FragAtomId = new int[CurMol.getAtomCount()];
     double[] w = new double[CurMol.getAtomCount()];
     for (int i = 0; i < CurMol.getAtomCount(); i++) {
       FragAtomId[i] = -999;
       w[i] = -999.0D;
     } 
 
     
     try {
       ConnAugMatrix = ConnectionAugMatrix.getMatrix(CurMol);
     } catch (Exception e) {
       return w;
     } 
     
     for (IAtom at : CurMol.atoms()) {
       GetHydrophobitictyPrivate(at, CurMol, FragAtomId, ConnAugMatrix);
     }
     for (int j = 0; j < CurMol.getAtomCount(); j++) {
       if (FragAtomId[j] != -999) {
         w[j] = Hydrophobicity[FragAtomId[j]];
       }
     } 
     return w;
   }
 
   
   public static double[] GetMolarRefractivity(Molecule CurMol) {
     double[][] ConnAugMatrix;
     int[] FragAtomId = new int[CurMol.getAtomCount()];
     double[] w = new double[CurMol.getAtomCount()];
     for (int i = 0; i < CurMol.getAtomCount(); i++) {
       FragAtomId[i] = -999;
       w[i] = -999.0D;
     } 
 
     
     try {
       ConnAugMatrix = ConnectionAugMatrix.getMatrix(CurMol);
     } catch (Exception e) {
       return w;
     } 
     
     for (IAtom at : CurMol.atoms()) {
       GetHydrophobitictyPrivate(at, CurMol, FragAtomId, ConnAugMatrix);
     }
     for (int j = 0; j < CurMol.getAtomCount(); j++) {
       if (FragAtomId[j] != -999) {
         w[j] = MolarRefractivity[FragAtomId[j]];
       }
     } 
     return w;
   }
 
 
   
   private static void GetHydrophobitictyPrivate(IAtom at, Molecule CurMol, int[] FragAtomId, double[][] ConnAugMatrix) {
     int At = CurMol.getAtomNumber(at);
     int nSK = CurMol.getAtomCount();
     
     boolean[] AtomAromatic = new boolean[nSK];
     for (int i = 0; i < nSK; i++) {
       if (CurMol.getAtom(i).getFlag(5)) {
         AtomAromatic[i] = true;
       } else {
         AtomAromatic[i] = false;
       } 
     } 
 
 
 
 
 
 
     
     int VD = 0, nH = 0, Charge = 0;
     int nSingle = 0, nDouble = 0, nTriple = 0, nArom = 0;
     
     int sX = 0, dX = 0, tX = 0, aX = 0, asX = 0;
     int sR = 0, dR = 0, tR = 0, aR = 0;
     int sAr = 0, dAr = 0, aAr = 0;
     int sAl = 0, dAl = 0, aAl = 0;
 
 
 
     
     int C_OxiNumber = 0, C_Hybridazion = 0, C_CX = 0;
     int j;
     for (j = 0; j < nSK; j++) {
       if (j != At)
       {
         if (!at.getSymbol().equals("H"))
         {
           if (ConnAugMatrix[At][j] > 0.0D) {
             VD++;
             int Z = (int)ConnAugMatrix[j][j];
             double b = ConnAugMatrix[At][j];
             
             if (b == 1.0D) {
               nSingle++;
               if (IsAtomElectronegative(Z))
                 sX++; 
               if (Z == 6)
                 sR++; 
               if (AtomAromatic[j])
               { sAr++; }
               
               else if (Z == 6) { sAl++; }
             
             }  if (b == 2.0D) {
               nDouble++;
               if (IsAtomElectronegative(Z))
                 dX++; 
               if (Z == 6)
                 dR++; 
               if (AtomAromatic[j])
               { dAr++; }
               
               else if (Z == 6) { dAl++; }
             
             }  if (b == 3.0D) {
               nTriple++;
               if (IsAtomElectronegative(Z))
                 tX++; 
               if (Z == 6)
                 tR++; 
             } 
             if (b == 1.5D) {
               nArom++;
               if (IsAtomElectronegative(Z)) {
 
                 
                 int elNegCharge, elNegVD = 0, elNegH = 0;
                 for (int k = 0; k < nSK; k++) {
                   if (ConnAugMatrix[j][k] > 0.0D && 
                     j != k)
                     elNegVD++; 
                 } 
                 try {
                   elNegH = CurMol.getAtom(j).getImplicitHydrogenCount().intValue();
                 } catch (Exception e) {
                   elNegH = 0;
                 } 
                 try {
                   elNegCharge = CurMol.getAtom(j).getFormalCharge().intValue();
                 } catch (Exception e) {
                   elNegCharge = 0;
                 } 
                 elNegVD += elNegH - elNegCharge;
                 
                 boolean IsPyrroleLikeArom = false;
                 
                 if (Z == 7 && elNegVD == 3)
                   IsPyrroleLikeArom = true; 
                 if (Z == 8)
                   IsPyrroleLikeArom = true; 
                 if (Z == 16 && elNegVD == 2) {
                   IsPyrroleLikeArom = true;
                 }
                 if (IsPyrroleLikeArom) {
                   asX++;
                 } else {
                   aX++;
                 } 
               } 
               if (Z == 6)
                 aR++; 
               if (AtomAromatic[j])
               { aAr++; }
               
               else if (Z == 6) { aAl++; }
             
             } 
           } 
         }
       }
     } 
     try {
       nH = 0;
       for (IAtom conn : CurMol.getConnectedAtomsList(at)) {
         if (conn.getSymbol().equals("H"))
           nH++; 
       } 
     } catch (Exception e) {
       nH = 0;
     } 
 
     
     try {
       Charge = CurMol.getAtom(At).getFormalCharge().intValue();
     } catch (Exception e) {
       Charge = 0;
     } 
 
     
     if (ConnAugMatrix[At][At] == 6.0D) {
       
       int c_VD = 0;
       for (int k = 0; k < nSK; k++) {
         if (k != At && 
           ConnAugMatrix[k][At] > 0.0D) {
           c_VD++;
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
           
           if (ConnAugMatrix[k][k] == 6.0D)
           {
             for (int m = 0; m < nSK; m++) {
               if (m != k && 
                 ConnAugMatrix[m][k] > 0.0D && IsAtomElectronegative((int)ConnAugMatrix[m][m])) {
                 C_CX++;
               }
             } 
           }
         } 
       } 
       C_OxiNumber += sX * 1 + dX * 2 + tX * 3;
       C_OxiNumber += asX * 1;
       if (aX > 1) {
         C_OxiNumber = (int)(C_OxiNumber + aX * 1.5D);
       } else {
         C_OxiNumber += aX * 2;
       } 
       C_Hybridazion = c_VD - 1;
       
       if (C_OxiNumber != C_OxiNumber) {
         C_OxiNumber++;
       }
     } 
 
 
 
 
 
     
     if (nH > 0) {
       
       int H_type = 0;
       
       if (ConnAugMatrix[At][At] == 6.0D) {
         
         boolean IsAlphaCarbon = false;
 
 
         
         if (nSingle > 0 && nDouble == 0 && nTriple == 0 && nArom == 0)
         {
           for (int k = 0; k < nSK; k++) {
             if (k != At)
             {
 
               
               if (ConnAugMatrix[At][k] > 0.0D)
               {
                 if (ConnAugMatrix[At][k] == 1.0D && ConnAugMatrix[k][k] == 6.0D) {
                   
                   int nCdX = 0, nCtX = 0, nCaX = 0;
                   
                   for (int m = 0; m < nSK; m++) {
                     if (m != k) {
                       
                       int Z = (int)ConnAugMatrix[m][m];
                       if (ConnAugMatrix[k][m] > 0.0D && IsAtomElectronegative(Z)) {
                         if (ConnAugMatrix[k][m] == 2.0D)
                           nCdX++; 
                         if (ConnAugMatrix[k][m] == 3.0D)
                           nCtX++; 
                         if (ConnAugMatrix[k][m] == 1.5D)
                           nCaX++; 
                       } 
                     } 
                   } 
                   if (nCdX + nCtX + nCaX == 1) {
                     IsAlphaCarbon = true;
                   }
                 } else {
                   
                   IsAlphaCarbon = false;
                   break;
                 } 
               }
             }
           } 
         }
         if (IsAlphaCarbon)
         {
           H_type = 51;
         
         }
         else
         {
           if (C_OxiNumber == 0 && C_Hybridazion == 3 && C_CX == 0) {
             H_type = 46;
           }
           
           if ((C_OxiNumber == 1 && C_Hybridazion == 3) || (C_OxiNumber == 0 && C_Hybridazion == 2))
           {
             H_type = 47;
           }
           
           if ((C_OxiNumber == 2 && C_Hybridazion == 3) || (C_OxiNumber == 1 && C_Hybridazion == 2) || (C_OxiNumber == 0 && C_Hybridazion == 1))
           {
             
             H_type = 48;
           }
           
           if ((C_OxiNumber == 3 && C_Hybridazion == 3) || (C_OxiNumber == 2 && C_Hybridazion == 2) || (C_OxiNumber == 3 && C_Hybridazion == 2) || (C_OxiNumber == 3 && C_Hybridazion == 1))
           {
 
             
             H_type = 49;
           }
           
           if (C_OxiNumber == 0 && C_Hybridazion == 3 && C_CX == 1) {
             H_type = 52;
           }
           
           if (C_OxiNumber == 0 && C_Hybridazion == 3 && C_CX == 2) {
             H_type = 53;
           }
           
           if (C_OxiNumber == 0 && C_Hybridazion == 3 && C_CX == 3) {
             H_type = 54;
           }
           
           if (C_OxiNumber == 0 && C_Hybridazion == 3 && C_CX == 4) {
             H_type = 55;
           }
         }
       
       }
       else {
         
         H_type = 50;
       } 
 
       
       for (IAtom conn : CurMol.getConnectedAtomsList(at)) {
         if (conn.getSymbol().equalsIgnoreCase("H")) {
           FragAtomId[CurMol.getAtomNumber(conn)] = H_type;
         }
       } 
     } 
 
 
 
 
     
     for (j = 0; j < nSK; j++) {
       if (j != At && 
         ConnAugMatrix[j][At] > 0.0D)
       {
         
         if (ConnAugMatrix[j][j] == 9.0D) {
           
           if (ConnAugMatrix[At][At] == 6.0D) {
             
             if (C_OxiNumber == 1 && C_Hybridazion == 3) {
               FragAtomId[j] = 81;
             
             }
             else if (C_OxiNumber == 2 && C_Hybridazion == 3) {
               FragAtomId[j] = 82;
             
             }
             else if (C_OxiNumber == 3 && C_Hybridazion == 3) {
               FragAtomId[j] = 83;
             
             }
             else if (C_OxiNumber == 1 && C_Hybridazion == 2) {
               FragAtomId[j] = 84;
             } else {
               
               FragAtomId[j] = 85;
             }
           
           } else {
             
             FragAtomId[j] = 85;
           
           }
 
         
         }
         else if (ConnAugMatrix[j][j] == 17.0D) {
           
           if (ConnAugMatrix[At][At] == 6.0D) {
             
             if (C_OxiNumber == 1 && C_Hybridazion == 3) {
               FragAtomId[j] = 86;
             
             }
             else if (C_OxiNumber == 2 && C_Hybridazion == 3) {
               FragAtomId[j] = 87;
             
             }
             else if (C_OxiNumber == 3 && C_Hybridazion == 3) {
               FragAtomId[j] = 88;
             
             }
             else if (C_OxiNumber == 1 && C_Hybridazion == 2) {
               FragAtomId[j] = 89;
             } else {
               
               FragAtomId[j] = 90;
             }
           
           } else {
             
             FragAtomId[j] = 90;
           
           }
 
         
         }
         else if (ConnAugMatrix[j][j] == 35.0D) {
           
           if (ConnAugMatrix[At][At] == 6.0D) {
             
             if (C_OxiNumber == 1 && C_Hybridazion == 3) {
               FragAtomId[j] = 91;
             
             }
             else if (C_OxiNumber == 2 && C_Hybridazion == 3) {
               FragAtomId[j] = 92;
             
             }
             else if (C_OxiNumber == 3 && C_Hybridazion == 3) {
               FragAtomId[j] = 93;
             
             }
             else if (C_OxiNumber == 1 && C_Hybridazion == 2) {
               FragAtomId[j] = 94;
             } else {
               
               FragAtomId[j] = 95;
             }
           
           } else {
             
             FragAtomId[j] = 95;
           
           }
 
         
         }
         else if (ConnAugMatrix[j][j] == 53.0D) {
           
           if (ConnAugMatrix[At][At] == 6.0D) {
             
             if (C_OxiNumber == 1 && C_Hybridazion == 3) {
               FragAtomId[j] = 96;
             
             }
             else if (C_OxiNumber == 2 && C_Hybridazion == 3) {
               FragAtomId[j] = 97;
             
             }
             else if (C_OxiNumber == 3 && C_Hybridazion == 3) {
               FragAtomId[j] = 98;
             
             }
             else if (C_OxiNumber == 1 && C_Hybridazion == 2) {
               FragAtomId[j] = 99;
             } else {
               
               FragAtomId[j] = 100;
             }
           
           } else {
             
             FragAtomId[j] = 100;
           } 
         } 
       }
     } 
 
 
 
 
 
 
     
     if (ConnAugMatrix[At][At] == 6.0D) {
       
       if ((nH == 3 && sR == 1) || nH == 4) {
         FragAtomId[At] = 1; return;
       } 
       if (nH == 2 && sR == 2) {
         FragAtomId[At] = 2; return;
       } 
       if (nH == 1 && sR == 3) {
         FragAtomId[At] = 3; return;
       } 
       if (nH == 0 && sR == 4) {
         FragAtomId[At] = 4; return;
       } 
       if (nH == 3 && sX == 1) {
         FragAtomId[At] = 5; return;
       } 
       if (nH == 2 && sR == 1 && sX == 1) {
         FragAtomId[At] = 6; return;
       } 
       if (nH == 2 && sX == 2) {
         FragAtomId[At] = 7; return;
       } 
       if (nH == 1 && sR == 2 && sX == 1) {
         FragAtomId[At] = 8; return;
       } 
       if (nH == 1 && sR == 1 && sX == 2) {
         FragAtomId[At] = 9; return;
       } 
       if (nH == 1 && sX == 3) {
         FragAtomId[At] = 10; return;
       } 
       if (nH == 0 && sR == 3 && sX == 1) {
         FragAtomId[At] = 11; return;
       } 
       if (nH == 0 && sR == 2 && sX == 2) {
         FragAtomId[At] = 12; return;
       } 
       if (nH == 0 && sR == 1 && sX == 3) {
         FragAtomId[At] = 13; return;
       } 
       if (nH == 0 && sX == 4) {
         FragAtomId[At] = 14; return;
       } 
       if (nH == 2 && dR == 1) {
         FragAtomId[At] = 15; return;
       } 
       if (nH == 1 && dR == 1 && sR == 1) {
         FragAtomId[At] = 16; return;
       } 
       if (nH == 0 && dR == 1 && sR == 2) {
         FragAtomId[At] = 17; return;
       } 
       if (nH == 1 && dR == 1 && sX == 1) {
         FragAtomId[At] = 18; return;
       } 
       if (nH == 0 && dR == 1 && sX == 1 && sR == 1) {
         FragAtomId[At] = 19; return;
       } 
       if (nH == 0 && dR == 1 && sX == 2) {
         FragAtomId[At] = 20; return;
       } 
       if (nH == 1 && tR == 1) {
         FragAtomId[At] = 21; return;
       } 
       if ((nH == 0 && tR == 1 && sR == 1) || (nH == 0 && dR == 2)) {
         
         FragAtomId[At] = 22; return;
       } 
       if (nH == 0 && tR == 1 && sX == 1) {
         FragAtomId[At] = 23; return;
       } 
       if (nH == 1 && VD == 2 && aR == 2) {
         FragAtomId[At] = 24; return;
       } 
       if (nH == 0 && VD == 3 && aR >= 2 && (sR == 1 || aR == 3)) {
         FragAtomId[At] = 25; return;
       } 
       if (nH == 0 && VD == 3 && aR == 2 && sX == 1) {
         FragAtomId[At] = 26; return;
       } 
       if (nH == 1 && VD == 2 && aR == 1 && aX == 1) {
         FragAtomId[At] = 27; return;
       } 
       if (nH == 0 && VD == 3 && aR >= 1 && aX == 1 && (sR == 1 || aR == 2)) {
         FragAtomId[At] = 28; return;
       } 
       if (nH == 0 && VD == 3 && aR == 1 && aX == 1 && sX == 1) {
         FragAtomId[At] = 29; return;
       } 
       if (nH == 1 && VD == 2 && aX == 2) {
         FragAtomId[At] = 30; return;
       } 
       if (nH == 0 && VD == 3 && aX == 2 && (sR == 1 || aR == 1)) {
         FragAtomId[At] = 31; return;
       } 
       if (nH == 0 && VD == 3 && aX == 2 && sX == 1) {
         FragAtomId[At] = 32;
         
         return;
       } 
       
       if (nH == 1 && VD == 2 && aR == 1 && asX == 1) {
         FragAtomId[At] = 33; return;
       } 
       if (nH == 0 && VD == 3 && ((sR == 1 && aR == 1) || aR == 2) && asX == 1) {
         FragAtomId[At] = 34; return;
       } 
       if (nH == 0 && VD == 3 && sX == 1 && aR == 1 && asX == 1) {
         FragAtomId[At] = 35;
         
         return;
       } 
       
       if (nH == 1 && dX == 1 && sAl == 1) {
         FragAtomId[At] = 36; return;
       } 
       if (nH == 1 && dX == 1 && sAr == 1) {
         FragAtomId[At] = 37; return;
       } 
       if (nH == 0 && dX == 1 && sAl == 2) {
         FragAtomId[At] = 38; return;
       } 
       if (nH == 0 && dX == 1 && sAr == 1 && sR >= 1 && sX == 0) {
         FragAtomId[At] = 39; return;
       } 
       if ((nH == 0 && dX == 1 && sR == 1 && sX == 1) || (nH == 0 && tX == 1 && sR == 1) || (nH == 0 && dX == 2)) {
 
         
         FragAtomId[At] = 40; return;
       } 
       if (nH == 0 && dX == 1 && sX == 2) {
         FragAtomId[At] = 41;
         
         return;
       } 
       
       if (nH == 1 && VD == 2 && aX == 1 && asX == 1) {
         FragAtomId[At] = 42; return;
       } 
       if (nH == 0 && VD == 3 && (sR == 1 || aR == 1) && aX == 1 && asX == 1) {
         FragAtomId[At] = 43; return;
       } 
       if (nH == 0 && VD == 3 && sX == 1 && aX == 1 && asX == 1) {
         FragAtomId[At] = 44;
 
 
         
         return;
       } 
 
       
       return;
     } 
 
     
     if (ConnAugMatrix[At][At] == 8.0D) {
 
 
       
       for (j = 0; j < nSK; j++) {
         if (j != At) {
 
 
 
           
           if (ConnAugMatrix[At][j] > 0.0D && ConnAugMatrix[j][j] == 6.0D) {
             boolean c_Arom = AtomAromatic[j];
             int c_VD = 0, c_dR = 0, c_dO = 0, c_dX = 0;
             for (int k = 0; k < nSK; k++) {
               if (k != j)
               {
                 if (ConnAugMatrix[j][k] > 0.0D) {
                   c_VD++;
                   if (ConnAugMatrix[j][k] == 2.0D) {
                     if (IsAtomElectronegative((int)ConnAugMatrix[k][k]))
                       c_dX++; 
                     if (ConnAugMatrix[k][k] == 8.0D) {
                       c_dO++;
                     } else {
                       c_dR++;
                     } 
                   } 
                 } 
               }
             } 
 
             
             if (VD + nH == 2 && nSingle == 2 && c_dX > 0) {
               FragAtomId[At] = 60;
               
               return;
             } 
             
             if (nH == 1) {
               
               if (c_VD == 3 && !c_Arom && c_dR == 1) {
                 FragAtomId[At] = 57;
                 return;
               } 
               if (c_Arom) {
                 FragAtomId[At] = 57;
                 return;
               } 
               if (!c_Arom && c_VD == 3 && c_dO == 1) {
                 FragAtomId[At] = 57;
 
                 
                 return;
               } 
             } 
           } 
 
           
           if (ConnAugMatrix[At][j] > 0.0D && ConnAugMatrix[j][j] == 7.0D) {
             int n_VD = 0, n_dO = 0, n_sOminus = 0;
             for (int k = 0; k < nSK; k++) {
               if (k != j)
               {
                 if (ConnAugMatrix[j][k] > 0.0D) {
                   n_VD++;
                   if (ConnAugMatrix[j][k] == 2.0D && 
                     ConnAugMatrix[k][k] == 8.0D) {
                     n_dO++;
                   }
                   if (ConnAugMatrix[j][k] == 1.0D && 
                     ConnAugMatrix[k][k] == 8.0D) {
                     int o_VD = 0;
                     for (int z = 0; z < nSK; z++) {
                       if (z != k && 
                         ConnAugMatrix[k][z] > 0.0D) o_VD++; 
                     } 
                     if (o_VD == 1) {
                       int nH_O = 0;
                       try {
                         nH_O = CurMol.getAtom(k).getImplicitHydrogenCount().intValue();
                       } catch (Exception e) {
                         nH_O = 0;
                       } 
                       if (nH == 0) {
                         n_sOminus++;
                       }
                     } 
                   } 
                 } 
               }
             } 
 
 
 
             
             if (VD == 1 && nH == 0 && nDouble == 1 && n_VD == 3 && n_sOminus == 1) {
               FragAtomId[At] = 61; return;
             } 
             if (VD == 1 && nH == 0 && n_VD == 3 && n_dO == 1) {
               FragAtomId[At] = 61;
               
               return;
             } 
           } 
         } 
       } 
       if (nSingle == 1 && nH == 1) {
         FragAtomId[At] = 56;
         return;
       } 
       if (VD == 1 && nDouble == 1) {
         FragAtomId[At] = 58; return;
       } 
       if (VD == 2 && nSingle == 2) {
         for (j = 0; j < nSK; j++) {
           if (j != At)
           {
             if (ConnAugMatrix[At][j] == 1.0D && ConnAugMatrix[j][j] == 8.0D) {
               
               FragAtomId[At] = 63;
               return;
             } 
           }
         } 
       }
       if ((VD == 2 && sAl == 1 && sAr == 1) || (VD == 2 && sAr == 2) || (VD == 2 && aR == 2)) {
 
         
         FragAtomId[At] = 60; return;
       } 
       if (VD == 2 && nSingle == 2 && sAr == 0) {
         FragAtomId[At] = 59; return;
       } 
       if (Charge == -2) {
         FragAtomId[At] = 61; return;
       } 
       if (Charge == -1) {
         FragAtomId[At] = 62;
 
         
         return;
       } 
       
       return;
     } 
     
     if (ConnAugMatrix[At][At] == 34.0D) {
       
       if (VD == 2 && nSingle == 2) {
         FragAtomId[At] = 64; return;
       } 
       if (VD == 1 && nDouble == 1) {
         FragAtomId[At] = 65;
 
         
         return;
       } 
       
       return;
     } 
     
     if (ConnAugMatrix[At][At] == 7.0D) {
 
       
       boolean AromPyridineLike = false, AromPyrroleLike = false;
       if (nArom >= 2) {
         if (VD + nH - Charge == 2)
           AromPyridineLike = true; 
         if (VD + nH - Charge == 3) {
           AromPyrroleLike = true;
         }
       } 
 
       
       int nO = 0, sOCR = 0, sXdX = 0;
       
       for (int k = 0; k < nSK; k++) {
         if (k != At)
         {
           if (ConnAugMatrix[At][k] > 0.0D) {
             int Z = (int)ConnAugMatrix[k][k];
 
             
             if (Z == 8) {
               nO++;
               for (int m = 0; m < nSK; m++) {
                 if (m != k && m != At && 
                   ConnAugMatrix[m][k] > 0.0D && ConnAugMatrix[m][m] == 6.0D) {
                   int c_VD = 0;
                   for (int z = 0; z < nSK; z++) {
                     if (z != m && 
                       ConnAugMatrix[z][m] > 0.0D) c_VD++; 
                   } 
                   if (c_VD == 2) {
                     sOCR++;
                   }
                 } 
               } 
             } 
 
 
             
             if (IsAtomElectronegative(Z) || Z == 6) {
               int x_VD = 0, x_dX = 0;
               for (int m = 0; m < nSK; m++) {
                 if (m != k && m != At && 
                   ConnAugMatrix[m][k] > 0.0D) {
                   x_VD++;
                   if (IsAtomElectronegative((int)ConnAugMatrix[m][m]) && 
                     ConnAugMatrix[m][k] == 2.0D) {
                     x_dX++;
                   }
                 } 
               } 
               if (x_dX == 1) {
                 sXdX++;
               }
             } 
           } 
         }
       } 
       
       if ((VD == 3 && nH == 0 && sAr == 1 && nO == 2) || (VD == 3 && nH == 0 && aR == 2 && aX == 1) || (VD == 2 && nH == 0 && nO == 2)) {
 
 
         
         FragAtomId[At] = 76; return;
       } 
       if (VD == 3 && nH == 0 && sAl == 1 && nO == 2) {
         FragAtomId[At] = 77;
         return;
       } 
       if (!AromPyrroleLike && !AromPyridineLike && 
         Charge == 1) {
         FragAtomId[At] = 79;
         return;
       } 
       if (!AromPyrroleLike && ((
         VD == 3 && sOCR == 1) || (VD + nH == 3 && sXdX > 0))) {
         
         FragAtomId[At] = 72;
         return;
       } 
       if (VD == 1 && nH == 2 && sAl == 1) {
         FragAtomId[At] = 66; return;
       } 
       if (VD == 2 && nH == 1 && sAl == 2) {
         FragAtomId[At] = 67; return;
       } 
       if (VD == 3 && nH == 0 && sAl == 3) {
         FragAtomId[At] = 68; return;
       } 
       if (VD == 1 && nH == 2 && (sAr == 1 || sX == 1)) {
         FragAtomId[At] = 69; return;
       } 
       if (VD == 2 && nH == 1 && sAl == 2) {
         FragAtomId[At] = 70; return;
       } 
       if (VD == 3 && nH == 0 && sAl == 2 && sAr == 1) {
         FragAtomId[At] = 71; return;
       } 
       if ((VD == 2 && nH == 1 && sAr == 2) || (VD == 3 && nH == 0 && sAr == 3) || (VD == 3 && nH == 0 && sAr == 2 && sAl == 1) || AromPyrroleLike) {
 
 
         
         FragAtomId[At] = 73; return;
       } 
       if ((VD == 1 && nH == 0 && tR == 1) || (VD == 2 && nH == 0 && dR == 1 && sR == 1)) {
         
         FragAtomId[At] = 74; return;
       } 
       if (AromPyridineLike) {
         
         FragAtomId[At] = 75; return;
       } 
       if ((VD == 2 && nH == 0 && sAr == 1 && dX == 1) || (VD == 2 && nH == 0 && sX == 1 && dX == 1)) {
         
         FragAtomId[At] = 78;
 
         
         return;
       } 
 
       
       return;
     } 
     
     if (ConnAugMatrix[At][At] == 9.0D && VD == 0) {
       FragAtomId[At] = 101;
       return;
     } 
     if (ConnAugMatrix[At][At] == 17.0D && VD == 0) {
       FragAtomId[At] = 102;
       return;
     } 
     if (ConnAugMatrix[At][At] == 35.0D && VD == 0) {
       FragAtomId[At] = 103;
       return;
     } 
     if (ConnAugMatrix[At][At] == 53.0D && VD == 0) {
       FragAtomId[At] = 104;
 
       
       return;
     } 
     
     if (ConnAugMatrix[At][At] == 16.0D) {
       
       int nO = 0;
       
       for (int k = 0; k < nSK; k++) {
         if (k != At && 
           ConnAugMatrix[At][k] > 0.0D && 
           ConnAugMatrix[k][k] == 8.0D) {
           nO++;
         }
       } 
       
       if (nH == 1 && sR == 1) {
         FragAtomId[At] = 106; return;
       } 
       if (VD == 2 && nH == 0) {
         FragAtomId[At] = 107; return;
       } 
       if (VD == 1 && nDouble == 1) {
         FragAtomId[At] = 108; return;
       } 
       if (VD == 3 && nSingle == 2 && nO == 1) {
         FragAtomId[At] = 109; return;
       } 
       if (VD == 4 && nSingle == 2 && nO >= 2) {
         FragAtomId[At] = 110;
 
         
         return;
       } 
       
       return;
     } 
     
     if (ConnAugMatrix[At][At] == 14.0D) {
       
       if (VD == 4 && nSingle == 4) {
         FragAtomId[At] = 111;
 
         
         return;
       } 
 
       
       return;
     } 
     
     if (ConnAugMatrix[At][At] == 5.0D) {
       
       if (VD == 3 && nSingle == 3) {
         FragAtomId[At] = 112;
 
         
         return;
       } 
 
       
       return;
     } 
     
     if (ConnAugMatrix[At][At] == 15.0D) {
       
       if (VD == 4 && Charge == 1) {
         FragAtomId[At] = 115; return;
       } 
       if (VD == 4 && dX == 1 && sR == 3) {
         FragAtomId[At] = 116; return;
       } 
       if (VD == 4 && dX == 1 && sX == 3) {
         FragAtomId[At] = 117;
         return;
       } 
       if (VD == 3 && sX == 3) {
         FragAtomId[At] = 118; return;
       } 
       if (VD == 3 && sR == 3) {
         FragAtomId[At] = 119; return;
       } 
       if (VD == 4 && dX == 1 && sR == 1 && sX == 2) {
         FragAtomId[At] = 120;
         return;
       } 
     } 
   }
 }


/* Location:              D:\seafile\Seafile\Skin Sens\vega-1.1.5-b39\lib\insilicoModel.jar!\insilico\model\aromatase\irfmn\AromataseGCWeights.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */