 package insilico.model.aromatase2.irfmn;
 
 import Jama.EigenvalueDecomposition;
 import Jama.Matrix;
 import insilico.core.descriptor.weight.Mass;
 import insilico.core.exception.GenericFailureException;
 import insilico.core.exception.InvalidMoleculeException;
 import insilico.core.molecule.InsilicoMolecule;
 import insilico.core.tools.logger.InsilicoLogger;
 import java.util.Arrays;
 import java.util.List;
 import org.openscience.cdk.Atom;
 import org.openscience.cdk.Molecule;
 import org.openscience.cdk.interfaces.IAtom;
 import org.openscience.cdk.interfaces.IBond;
 
 
 
 
 
 
 
 
 
 
 public class AromataseDescriptors
 {
   public static double Calculate_SM6_Bm(InsilicoMolecule mol) {
     Molecule m;
     double[][] BurdenMat;
     try {
       m = mol.GetStructure();
     } catch (InvalidMoleculeException e) {
       return -999.0D;
     } 
     
     int nSK = m.getAtomCount();
 
 
     
     try {
       BurdenMat = mol.GetMatrixBurden();
     } catch (GenericFailureException e) {
       InsilicoLogger.getLogger().warn(e);
       return -999.0D;
     } 
     
     double[] w = Mass.getWeights(m);
 
     
     boolean MissingWeight = false; int i;
     for (i = 0; i < nSK; i++) {
       if (w[i] == -999.0D)
         MissingWeight = true; 
     }  if (MissingWeight) {
       return -999.0D;
     }
     
     for (i = 0; i < nSK; i++) {
       BurdenMat[i][i] = w[i];
     }
 
     
     Matrix DataMatrix = new Matrix(BurdenMat);
     
     EigenvalueDecomposition ed = new EigenvalueDecomposition(DataMatrix);
     double[] eigenvalues = ed.getRealEigenvalues();
     Arrays.sort(eigenvalues);
     
     double sum = 0.0D;
     for (double val : eigenvalues)
       sum += Math.pow(val, 6.0D); 
     double SM6 = Math.log(1.0D + sum);
     
     return SM6;
   }
 
 
   
   public static double Calculate_spMaxAEAdm(InsilicoMolecule mol) {
     Molecule m;
     try {
       m = mol.GetStructure();
     } catch (InvalidMoleculeException e) {
       return -999.0D;
     } 
     
     int nBO = m.getBondCount();
 
     
     if (m.getAtomCount() < 2) {
       return -999.0D;
     }
 
     
     double[][][] EdgeAdjMat = (double[][][])null;
     try {
       EdgeAdjMat = mol.GetMatrixEdgeAdjacency();
     } catch (GenericFailureException e) {
       InsilicoLogger.getLogger().warn(e);
       return -999.0D;
     } 
 
 
     
     double[][] EdgeDipoleMat = new double[EdgeAdjMat.length][(EdgeAdjMat[0]).length]; int i;
     for (i = 0; i < EdgeAdjMat.length; i++) {
       for (int j = 0; j < (EdgeAdjMat[0]).length; j++) {
         EdgeDipoleMat[i][j] = EdgeAdjMat[i][j][0];
       }
     } 
     
     for (i = 0; i < m.getBondCount(); i++) {
       Atom a = (Atom)m.getBond(i).getAtom(0);
       Atom b = (Atom)m.getBond(i).getAtom(1);
       double CurVal = GetDipoleMoment(m, a, b);
       if (CurVal == 0.0D)
         CurVal = GetDipoleMoment(m, b, a); 
       EdgeDipoleMat[i][i] = CurVal;
     } 
     
     Matrix DataMatrix = new Matrix(EdgeDipoleMat);
 
 
     
     EigenvalueDecomposition ed = new EigenvalueDecomposition(DataMatrix);
     double[] eigenvalues = ed.getRealEigenvalues();
     
     Arrays.sort(eigenvalues);
 
     
     double spMax = eigenvalues[eigenvalues.length - 1];
     double spMaxA = spMax / nBO;
     
     return spMaxA;
   }
 
 
   
   public static double Calculate_SM15_EAed(InsilicoMolecule mol) {
     Molecule m;
     try {
       m = mol.GetStructure();
     } catch (InvalidMoleculeException e) {
       return -999.0D;
     } 
     
     int nBO = m.getBondCount();
 
     
     if (m.getAtomCount() < 2) {
       return -999.0D;
     }
 
     
     double[][] EdgeAdjMat = new double[nBO][nBO];
     for (int i = 0; i < nBO; i++) {
       IBond b = m.getBond(i);
       for (int n = 0; n < b.getAtomCount(); n++) {
         IAtom at = b.getAtom(n);
         List<IBond> connBonds = m.getConnectedBondsList(at);
         for (int z = 0; z < m.getConnectedBondsCount(at); z++) {
           IBond connBond = connBonds.get(z);
           int BondNum = m.getBondNumber(connBond);
           if (BondNum != i) {
             EdgeAdjMat[i][BondNum] = 1.0D;
             EdgeAdjMat[BondNum][i] = 1.0D;
           } 
         } 
       } 
     } 
     double[] wEdgeDegree = new double[nBO];
     for (int j = 0; j < nBO; j++) {
       int n = 0;
       for (int i1 = 0; i1 < nBO; i1++)
         n = (int)(n + EdgeAdjMat[j][i1]); 
       wEdgeDegree[j] = n;
     } 
     double[][] EdgeAdjMatEd = new double[nBO][nBO];
     for (int k = 0; k < nBO; k++) {
       for (int n = 0; n < nBO; n++) {
         if (EdgeAdjMat[k][n] == 1.0D) {
           EdgeAdjMatEd[k][n] = wEdgeDegree[n];
           EdgeAdjMatEd[n][k] = wEdgeDegree[k];
         } else {
           EdgeAdjMatEd[k][n] = 0.0D;
           EdgeAdjMatEd[n][k] = 0.0D;
         } 
       } 
     } 
     
     Matrix DataMatrix = new Matrix(EdgeAdjMatEd);
 
 
     
     EigenvalueDecomposition ed = new EigenvalueDecomposition(DataMatrix);
     double[] eigenvalues = ed.getRealEigenvalues();
     
     Arrays.sort(eigenvalues);
     
     double sum = 0.0D;
     for (double val : eigenvalues)
       sum += Math.pow(val, 15.0D); 
     double SM15 = Math.log(1.0D + sum);
     
     return SM15;
   }
 
 
   
   private static double GetDipoleMoment(Molecule CurMol, Atom at1, Atom at2) {
     String a = at1.getSymbol();
     String b = at2.getSymbol();
 
     
     if (a.equalsIgnoreCase("C")) {
 
       
       if (b.equalsIgnoreCase("F")) {
         return 1.51D;
       }
 
       
       if (b.equalsIgnoreCase("Cl")) {
         int nCl = 0;
         for (IAtom at : CurMol.getConnectedAtomsList((IAtom)at1)) {
           if (at.getSymbol().equalsIgnoreCase("Cl"))
             nCl++; 
         } 
         if (nCl == 1)
           return 1.56D; 
         if (nCl == 2)
           return 1.2D; 
         if (nCl == 3) {
           return 0.83D;
         }
       } 
       
       if (b.equalsIgnoreCase("Br")) {
         return 1.48D;
       }
 
       
       if (b.equalsIgnoreCase("I")) {
         return 1.29D;
       }
 
       
       if (b.equalsIgnoreCase("N")) {
         if (CurMol.getBond((IAtom)at1, (IAtom)at2).getFlag(5))
           return 0.0D; 
         IBond.Order ord = CurMol.getBond((IAtom)at1, (IAtom)at2).getOrder();
         if (ord == IBond.Order.SINGLE)
           return 0.4D; 
         if (ord == IBond.Order.DOUBLE)
           return 0.9D; 
         if (ord == IBond.Order.TRIPLE) {
           return 3.6D;
         }
       } 
       
       if (b.equalsIgnoreCase("O")) {
         if (CurMol.getBond((IAtom)at1, (IAtom)at2).getFlag(5))
           return 0.0D; 
         IBond.Order ord = CurMol.getBond((IAtom)at1, (IAtom)at2).getOrder();
         if (ord == IBond.Order.SINGLE)
           return 0.86D; 
         if (ord == IBond.Order.DOUBLE) {
           return 2.4D;
         }
       } 
       
       if (b.equalsIgnoreCase("S")) {
         if (CurMol.getBond((IAtom)at1, (IAtom)at2).getFlag(5))
           return 0.0D; 
         IBond.Order ord = CurMol.getBond((IAtom)at1, (IAtom)at2).getOrder();
         if (ord == IBond.Order.SINGLE)
           return 2.95D; 
         if (ord == IBond.Order.DOUBLE) {
           return 2.8D;
         }
       } 
     } 
 
 
     
     if (a.equalsIgnoreCase("N") && b.equalsIgnoreCase("O")) {
       if (CurMol.getBond((IAtom)at1, (IAtom)at2).getFlag(5))
         return 0.0D; 
       IBond.Order ord = CurMol.getBond((IAtom)at1, (IAtom)at2).getOrder();
       int nH = 0; 
       try { nH = at2.getImplicitHydrogenCount().intValue(); } catch (Exception exception) {}
       int nConn = CurMol.getConnectedAtomsCount((IAtom)at2) + nH;
       if (ord == IBond.Order.SINGLE && nConn == 2)
         return 3.2D; 
       if (ord == IBond.Order.SINGLE && nConn == 1) {
         return 2.0D;
       }
       if (ord == IBond.Order.DOUBLE && nConn == 1) {
         return 2.0D;
       }
     } 
 
     
     if (a.equalsIgnoreCase("S") && b.equalsIgnoreCase("O")) {
       if (CurMol.getBond((IAtom)at1, (IAtom)at2).getFlag(5))
         return 0.0D; 
       IBond.Order ord = CurMol.getBond((IAtom)at1, (IAtom)at2).getOrder();
       int nConn = CurMol.getConnectedAtomsCount((IAtom)at2);
       if (ord == IBond.Order.SINGLE && nConn == 2) {
         return 2.9D;
       }
     } 
 
     
     if (a.equalsIgnoreCase("C") && b.equalsIgnoreCase("C")) {
       
       if (CurMol.getBond((IAtom)at1, (IAtom)at2).getFlag(5)) {
         return 0.0D;
       }
       int nH1 = 0, nH2 = 0;
       try {
         nH1 = at1.getImplicitHydrogenCount().intValue();
       } catch (Exception exception) {}
       try {
         nH2 = at2.getImplicitHydrogenCount().intValue();
       } catch (Exception exception) {}
       
       int nConn1 = CurMol.getConnectedAtomsCount((IAtom)at1) + nH1;
       int nConn2 = CurMol.getConnectedAtomsCount((IAtom)at2) + nH2;
       
       if (nConn1 == 3 && nConn2 == 4)
         return 0.68D; 
       if (nConn1 == 3 && nConn2 == 2)
         return 1.15D; 
       if (nConn1 == 2 && nConn2 == 4) {
         return 1.48D;
       }
     } 
     return 0.0D;
   }
 }


/* Location:              D:\seafile\Seafile\Skin Sens\vega-1.1.5-b39\lib\insilicoModel.jar!\insilico\model\aromatase\irfmn\AromataseDescriptors.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */