 package insilico.model.aromatase2.irfmn;
 
 import insilico.core.ad.ADCheckACF;
 import insilico.core.ad.ADCheckIndicesQualitative;
 import insilico.core.ad.item.ADIndexACF;
 import insilico.core.ad.item.ADIndexADI;
 import insilico.core.ad.item.ADIndexAccuracy;
 import insilico.core.ad.item.ADIndexConcordance;
 import insilico.core.ad.item.ADIndexSimilarity;
 import insilico.core.ad.item.iADIndex;
 import insilico.core.descriptor.DescriptorBlock;
 import insilico.core.descriptor.DescriptorsEngine;
 import insilico.core.descriptor.blocks.AtomCenteredFragments;
 import insilico.core.descriptor.blocks.AutoCorrelationHFilledWithCorrectIState;
 import insilico.core.descriptor.blocks.BurdenEigenvalueHFilled;
 import insilico.core.descriptor.blocks.ConnectivityIndices;
 import insilico.core.descriptor.blocks.EStates;
 import insilico.core.descriptor.blocks.EdgeAdjacencyCorrected;
 import insilico.core.descriptor.blocks.FunctionalGroups;
 import insilico.core.descriptor.blocks.TopologicalDistances;
 import insilico.core.descriptor.blocks.WalkAndPath;
 import insilico.core.descriptor.blocks.logp.MLogP;
 import insilico.core.exception.InitFailureException;
 import insilico.core.model.InsilicoModel;
 import insilico.core.pmml.ModelGenericFromPMML;
 import insilico.core.tools.ModelUtilities;
 import insilico.core.tools.logger.InsilicoLogger;
import insilico.model.test.PMMLTEST;
 import java.io.IOException;
 import java.net.URL;
 import java.util.ArrayList;
 import java.util.LinkedHashMap;
 import java.util.Map;
 import org.dmg.pmml.FieldName;
 
 
 
 
 
 
 public class ismAromataseIRFMN
   extends InsilicoModel
 {
   private static final long serialVersionUID = 1L;
   private static final String ModelData = "/insilico/model/aromatase2/irfmn/data/model_aromatase_irfmn.xml";
   private static final double[] NormalizationStd = new double[] { 0.722707114236381D, 0.310427752760591D, 0.114235214557807D, 2.31908652591402D, 2.66891786902396D, 1.5816081267157D, 17.1633337872584D, 2.80990078171721D, 6.23981935672799D, 0.821738943310684D, 1.3421930548219D, 0.9797670727756D, 0.413338942895699D, 0.940354356942315D, 1.9585517813492D, 0.584725830764994D, 3.54689302386382D, 5.77113497436714D };
   private static final double[] NormalizationMean = new double[] { 3.6784260332613D, 3.47279465655764D, 0.236384422986387D, 4.41629714683753D, 4.17602960925877D, 2.03932277687871D, 17.6678632113703D, 4.08585290910042D, 7.90865944284246D, 2.5557638345508D, 0.721185930851721D, 8.9293139122077D, 0.162629757785467D, 5.28829501450282D, 1.05489634480876D, 0.194156093810073D, 3.35447904652057D, 34.6667993498115D };
   
   private final AromatasePVSA pvsa;
   
   private ModelGenericFromPMML Model;
 
   
   public ismAromataseIRFMN() throws InitFailureException {
     super(ModelData);
 
     
     try {
       URL src = getClass().getResource("/insilico/model/aromatase2/irfmn/data/rangerRF.pmml");
       this.Model = new ModelGenericFromPMML(src.openStream());
     } catch (IOException ex) {
       throw new InitFailureException("Unable to read PMML source from .jar file");
     } 
 
     
     this.pvsa = new AromatasePVSA();
     this.pvsa.setBoolProperty("weightmr", true);
 
     
     this.DescriptorsSize = 18;
     this.DescriptorsNames = new String[this.DescriptorsSize];
     this.DescriptorsNames[0] = "SpMax2_Bh.m.";
     this.DescriptorsNames[1] = "SpMax2_Bh.p.";
     this.DescriptorsNames[2] = "SpMaxA_AEA.dm.";
     this.DescriptorsNames[3] = "piPC08";
     this.DescriptorsNames[4] = "piPC09";
     this.DescriptorsNames[5] = "MLOGP";
     this.DescriptorsNames[6] = "P_VSA_MR_6";
     this.DescriptorsNames[7] = "X5sol";
     this.DescriptorsNames[8] = "ATSC4p";
     this.DescriptorsNames[9] = "Eig03_EA.bo.";
     this.DescriptorsNames[10] = "SsssN";
     this.DescriptorsNames[11] = "SM6_B.m.";
     this.DescriptorsNames[12] = "nRNR2";
     this.DescriptorsNames[13] = "ATS5s";
     this.DescriptorsNames[14] = "SssNH";
     this.DescriptorsNames[15] = "H.049";
     this.DescriptorsNames[16] = "F02.C.N.";
     this.DescriptorsNames[17] = "SM15_EA.ed.";
 
     
     this.ResultsSize = 4;
     this.ResultsName = new String[this.ResultsSize];
     this.ResultsName[0] = "Aromatase activity";
     this.ResultsName[1] = "Probability(Active Agonist)";
     this.ResultsName[2] = "Probability(Active Antagonist)";
     this.ResultsName[3] = "Probability(Inactive)";
 
     
     this.ADItemsName = new String[4];
     this.ADItemsName[0] = (new ADIndexSimilarity()).GetIndexName();
     this.ADItemsName[1] = (new ADIndexAccuracy()).GetIndexName();
     this.ADItemsName[2] = (new ADIndexConcordance()).GetIndexName();
     this.ADItemsName[3] = (new ADIndexACF()).GetIndexName();
   }
 
 
 
 
   
   public ArrayList<DescriptorBlock> GetRequiredDescriptorBlocks() {
     ArrayList<DescriptorBlock> blocks = new ArrayList<>();
 
 
     
     BurdenEigenvalueHFilled burdenEigenvalueHFilled = new BurdenEigenvalueHFilled();
     burdenEigenvalueHFilled.setBoolProperty("weightm", true);
     burdenEigenvalueHFilled.setBoolProperty("weightp", true);
     blocks.add(burdenEigenvalueHFilled);
 
     
     WalkAndPath walkAndPath = new WalkAndPath();
     walkAndPath.setBoolProperty("path08", true);
     walkAndPath.setBoolProperty("path09", true);
     blocks.add(walkAndPath);
 
     
     MLogP mLogP = new MLogP();
     blocks.add(mLogP);
 
     
     ConnectivityIndices connectivityIndices = new ConnectivityIndices();
     connectivityIndices.setBoolProperty("mp05", true);
     blocks.add(connectivityIndices);
 
     
     FunctionalGroups functionalGroups = new FunctionalGroups();
     blocks.add(functionalGroups);
 
     
     AutoCorrelationHFilledWithCorrectIState autoCorrelationHFilledWithCorrectIState = new AutoCorrelationHFilledWithCorrectIState();
     autoCorrelationHFilledWithCorrectIState.setBoolProperty("lag04", true);
     autoCorrelationHFilledWithCorrectIState.setBoolProperty("lag05", true);
     autoCorrelationHFilledWithCorrectIState.setBoolProperty("weightp", true);
     autoCorrelationHFilledWithCorrectIState.setBoolProperty("weights", true);
     blocks.add(autoCorrelationHFilledWithCorrectIState);
 
     
     EStates eStates = new EStates();
     blocks.add(eStates);
 
     
     AtomCenteredFragments atomCenteredFragments = new AtomCenteredFragments();
     blocks.add(atomCenteredFragments);
 
     
     TopologicalDistances topologicalDistances = new TopologicalDistances();
     blocks.add(topologicalDistances);
 
     
     EdgeAdjacencyCorrected edgeAdjacencyCorrected = new EdgeAdjacencyCorrected();
     edgeAdjacencyCorrected.setBoolProperty("weightB", true);
     blocks.add(edgeAdjacencyCorrected);
     
     return blocks;
   }
 
 
 
 
   
   protected short CalculateDescriptors(DescriptorsEngine DescEngine) {
     try {
       this.pvsa.Calculate(this.CurMolecule);
       
       this.Descriptors = new double[this.DescriptorsSize];
       
       this.Descriptors[0] = DescEngine.getDescriptorBlock(BurdenEigenvalueHFilled.class).GetByName("BEH2m").getValue();
       this.Descriptors[1] = DescEngine.getDescriptorBlock(BurdenEigenvalueHFilled.class).GetByName("BEH2p").getValue();
       this.Descriptors[2] = AromataseDescriptors.Calculate_spMaxAEAdm(this.CurMolecule);
       this.Descriptors[3] = DescEngine.getDescriptorBlock(WalkAndPath.class).GetByName("piPC8").getValue();
       this.Descriptors[4] = DescEngine.getDescriptorBlock(WalkAndPath.class).GetByName("piPC9").getValue();
       this.Descriptors[5] = DescEngine.getDescriptorBlock(MLogP.class).GetByName("MLogP").getValue();
       this.Descriptors[6] = this.pvsa.GetByName("P_VSA_mr_5").getValue();
       this.Descriptors[7] = DescEngine.getDescriptorBlock(ConnectivityIndices.class).GetByName("X5sol").getValue();
       this.Descriptors[8] = DescEngine.getDescriptorBlock(AutoCorrelationHFilledWithCorrectIState.class).GetByName("ATSC4p").getValue();
       this.Descriptors[9] = DescEngine.getDescriptorBlock(EdgeAdjacencyCorrected.class).GetByName("EEig3bo").getValue();
       this.Descriptors[10] = DescEngine.getDescriptorBlock(EStates.class).GetByName("SsssN").getValue();
       this.Descriptors[11] = AromataseDescriptors.Calculate_SM6_Bm(this.CurMolecule);
       this.Descriptors[12] = DescEngine.getDescriptorBlock(FunctionalGroups.class).GetByName("nRNR2").getValue();
       this.Descriptors[13] = DescEngine.getDescriptorBlock(AutoCorrelationHFilledWithCorrectIState.class).GetByName("ATS5s").getValue();
       this.Descriptors[14] = DescEngine.getDescriptorBlock(EStates.class).GetByName("SssNH").getValue();
       this.Descriptors[15] = DescEngine.getDescriptorBlock(AtomCenteredFragments.class).GetByName("H-049").getValue();
       this.Descriptors[16] = DescEngine.getDescriptorBlock(TopologicalDistances.class).GetByName("F2(C..N)").getValue();
       this.Descriptors[17] = AromataseDescriptors.Calculate_SM15_EAed(this.CurMolecule);
     }
     catch (Throwable e) {
       return -2;
     } 
     
     return 1;
   }
 
 
 
 
   
   protected short CalculateModel() {
     double pInactive, pAgonist, pAntagonist, Prediction, ScaledDescriptors[] = new double[this.DescriptorsSize];
     for (int i = 0; i < this.DescriptorsSize; i++) {
         //Add Math.round to round up to 5 decimal
       ScaledDescriptors[i] = Math.round((this.Descriptors[i] - NormalizationMean[i])*100000.0 / NormalizationStd[i])/100000.0;
     }
     
     Map<String, Object> argumentsObject = new LinkedHashMap<>();
     for (int j = 0; j < this.DescriptorsSize; j++) {
       argumentsObject.put(this.DescriptorsNames[j], Double.valueOf(ScaledDescriptors[j]));
     }
 
     
     try {
         
       Map<FieldName, ?> oo = this.Model.Evaluate(argumentsObject);
       //       @todo understand why PMML inverts levels 
       pInactive = ((Double)oo.get(new FieldName("probability(active.agonist)"))).doubleValue();
       pAgonist = ((Double)oo.get(new FieldName("probability(inactive)" ))).doubleValue();
       
        
       pAntagonist =((Double)oo.get(new FieldName("probability(active.antagonist)"))).doubleValue();
     } catch (Exception ex) {
       return -1;
     } 
 
 
     Prediction = max(pInactive,pAgonist,pAntagonist);
     
//     if (pInactive > pAgonist && pInactive > pAntagonist) {
//       Prediction = 0.0D;
//     } else if (pAgonist > pInactive && pAgonist > pAntagonist) {
//       Prediction = 1.0D;
//     } else if (pAntagonist > pInactive && pAntagonist > pAgonist) {
//       Prediction = 2.0D;
//     } else {
//       Prediction = -1.0D;
//     } 
     this.CurOutput.setMainResultValue(Prediction);
     
     String[] Res = new String[this.ResultsSize];
     try {
       Res[0] = GetTrainingSet().getClassLabel(Prediction);
     } catch (Throwable ex) {
       InsilicoLogger.getLogger().warn("Unable to find label for aromatase activity value " + Prediction);
       Res[0] = String.valueOf(Prediction);
     } 
     Res[1] = this.Format_3D.format(pAgonist);
     Res[2] = this.Format_3D.format(pAntagonist);
     Res[3] = this.Format_3D.format(pInactive);
     this.CurOutput.setResults(Res);
     
     return 1;
   }
 
 
 
 
   
   protected short CalculateAD() {
     ADCheckIndicesQualitative adq = new ADCheckIndicesQualitative(this.TS);
     adq.AddMappingToPositiveValue(1.0D);
     adq.AddMappingToPositiveValue(2.0D);
     adq.AddMappingToNegativeValue(0.0D);
     adq.AddMappingToNegativeValue(-1.0D);
     adq.setMoleculesForIndexSize(2);
     if (!adq.Calculate(this.CurMolecule, this.CurOutput)) {
       return -1;
     }
     
     try {
       ((ADIndexSimilarity)this.CurOutput.getADIndex(ADIndexSimilarity.class)).SetThresholds(0.8D, 0.6D);
       ((ADIndexAccuracy)this.CurOutput.getADIndex(ADIndexAccuracy.class)).SetThresholds(0.8D, 0.6D);
       ((ADIndexConcordance)this.CurOutput.getADIndex(ADIndexConcordance.class)).SetThresholds(0.8D, 0.6D);
     } catch (Throwable e) {
       return -1;
     } 
 
     
     ADCheckACF adacf = new ADCheckACF(this.TS);
     if (!adacf.Calculate(this.CurMolecule, this.CurOutput)) {
       return -1;
     }
     
     double acfContribution = this.CurOutput.getADIndex(ADIndexACF.class).GetIndexValue();
     double ADIValue = adq.getIndexADI() * acfContribution;
     
     ADIndexADI ADI = new ADIndexADI();
     ADI.SetIndexValue(ADIValue);
     ADI.SetThresholds(0.8D, 0.6D);
     this.CurOutput.setADI((iADIndex)ADI);
     
     return 1;
   }
 
public static double max(double... n) {
    int i = 0;
    double max = n[i];
    int max_idx=0;

    while (++i < n.length)
        if (n[i] > max){
            max_idx=i;
            max = n[i];
}
    return Double.valueOf(max_idx);
}

 
 
   
   protected void CalculateAssessment() {
     ModelUtilities.SetDefaultAssessment(this.CurOutput, this.CurOutput.getResults()[0]);
 
     
     double Val = this.CurOutput.HasExperimental() ? this.CurOutput.getExperimental() : this.CurOutput.getMainResultValue();
     if (Val == 0.0D) {
       this.CurOutput.setAssessmentStatus((short)2);
     } else if (Val == 1.0D || Val == 2.0D) {
       this.CurOutput.setAssessmentStatus((short)1);
     } else {
       this.CurOutput.setAssessmentStatus((short)0);
     } 
   }
 }

